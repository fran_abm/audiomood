import csv
import random
import os

from numpy.core.numeric import argwhere
import stftFeatures
import datetime
import gzip
import numpy as np
import json
import sys
from collections import namedtuple, Counter
from tqdm import tqdm
import argparse


def convert_labels(pos_classes, neg_classes, file):
    """
    Converte as labels do AudioSet, que estão em Unicode, para texto.

    Args:
        pos_classes (List): Lista com as labels da classe positiva
        neg_classes (List): Lista com as labels da classe negativa
        file (Str): Caminho para o ficheiro do Audioset com as labels.

    Returns:
        conv -> Dict: Dicionário com as todas as conversões
        pos_labels -> List: Lista com as labels positivas convertidas
        neg_labels -> List: Lista com as labels negativas convertidas

    """
    conv = {}
    pos_labels = []
    neg_labels = []
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        for i in reader:
            clas = i[2].split(",")[0]
            if clas in pos_classes:
                pos_labels.append(i[1])
                conv[i[1]] = clas
            elif clas in neg_classes:
                neg_labels.append(i[1])
                conv[i[1]] = clas
            elif clas == 'Single-lens reflex camera':
                neg_labels.append(i[1])
                conv[i[1]] = 'Single-lens'
            elif clas == 'Machine gun':
                pos_labels.append(i[1])
                conv[i[1]] = 'Machine-gun'

    return pos_labels, neg_labels, conv

def build_base_dataset(positive_labels, negative_labels):
    """
    Cria uma pré-dataset que será utilizado para o donwload.

    Args:
        positive_labels (List): Lista das labels que constituem a classe positiva
        negative_labels (List): Lista das labels que constintuem a classe negativa

    Returns:
        Dict: Dicionário que será utilizado para o download.
    """         
    dataset = {'pos_videos': {}, 'neg_videos':{}}

    for i in positive_labels:
        if i not in dataset['pos_videos'].keys():
            dataset['pos_videos'][i] = []

    for i in negative_labels:
        if i not in dataset['neg_videos'].keys():
            dataset['neg_videos'][i] = []
    
    return dataset


def write_dataset_json(dataset, filePath):
    """
    Escreve o pré-dataset para um ficheiro json.

    Args:
        dataset (dict): Pré-Dataset
        filePath (str): Caminho onde será guardado este ficheiro.
    """
    with open(filePath, 'w') as outfile:
        json.dump(dataset, outfile)


def read_dataset_json(filePath):
    """Lê o ficheiro JSON com o pré-dataset.

    Args:
        filePath (str): Caminho para o ficheiro dataset.

    Returns:
        Dict: Pré-Dataset
    """
    with open(filePath) as json_file:
        return json.load(json_file)

def cleanLabels(conv, labels):
    a = []
    for i in labels:
        try:
            a.append(conv[i.strip().replace('"', '')])
        except:
            pass
    return a

def create_dataset(data, labels, dataname, labelname, height, width):
    """
    Esta função lê os áudios, converte para espectogramas, e escreve os espectograms e as 
        respectivas labels para os seus ficheiros, assim criando so datasets. 

    Chama:
        - stftFeatures.writeStft8bitsFiles

    Args:
        data (List): Lista com os caminhos para os áudios
        labels (List): Lista com a labels que correspondem aos áudios
        dataname (Str): Caminho para p ficheiro de dados
        labelname (Str): Caminho para o ficheiro de labels
        height (Int): Altura esperada do espectograma.
        width (Int): Largura esperada do espectograma.
    """
    print('Data')
    stftFeatures.writeStft8bitsFiles(data, dataname, height, width)

    # Label Creation
    outputFile = gzip.open(labelname, "wb")

    print('Labels')
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(labels)).byteswap()).tobytes())

    total = len(labels)
    current = 1
    for i in tqdm(range(len(labels)), desc="[Labels]", position=0):
        # print('{} of {}'.format(current, total), end="\r", flush=True)
        outputFile.write(int(labels[i]).to_bytes(1, 'big'))
        current += 1
    outputFile.close()


def readLabels(f):
    """
    Reads the labels file from a dataset
    """

    data = []
    with gzip.open(f, "rb") as o:
        magic = o.read(4)
        print(magic)
        numOfItems = int.from_bytes(o.read(4), 'big')
        print(numOfItems)
        for i in range(numOfItems):
            a = int.from_bytes(o.read(1), "big")
            data.append(a)
    return data


def build_directories(basePath, labels):
    """
    Função que cria as diretorias base, se não existirem.

    Args:
        basePath (str): Caminho para onde serão criadas as diretorias
        labels (List): Lista com as labels das classes.
    """
    for i in labels:
        if i not in os.listdir(basePath):
            os.mkdir(basePath + i)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('path', metavar='N', type=str, nargs='+',
                    help='Path to train file')

    args = parser.parse_args()

    path = args.path[0]

    data = path + 'output.wav'
    stftFeatures.writeStft8bitsFiles([data], "volume-ubyte.gz", 512, 320)   
    # print(readLabels(path))
