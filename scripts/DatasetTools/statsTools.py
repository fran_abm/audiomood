import matplotlib.pyplot as plt
import numpy as np
import datetime

### TODO: documentation is missing !
def labelCombo(k, o):
    a = []
    for i in o:
        if type(i.labels) == type([]):
            for j in i.labels:
                if k[j] != 'Screaming':
                    a.append(k[j])

    c = dict(Counter(a))
    plt.bar(c.keys(), c.values())
    plt.xticks(rotation=90)
    plt.ylabel('Number of Videos')
    plt.xlabel('Labels')
    plt.tight_layout()

    plt.show()
    return a

### TODO: documentation is missing !
def histogramAudioLength(folder):
    length = []
    for i in os.listdir(folder):
        a = audio_conversion.get_audio_duration(folder + i)
        length.append(a)

    print(length)
    plt.hist(length)
    plt.ylabel('Number of Videos')
    plt.xlabel('Length of Video')
    plt.show()


### TODO: documentation is missing !
def histogramLineLength(l):
    length = []
    for i in l:
        a = float(i.endTime) - float(i.srtTime)
        length.append(a)

    print(length)
    plt.hist(length)
    plt.ylabel('Number of Videos')
    plt.xlabel('Length of Video')
    plt.show()
