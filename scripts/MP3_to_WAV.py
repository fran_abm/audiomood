import sys, os
import datetime
import time
import gc

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    base_folder = '/home/quietswami/Documents/EMOMUSIC/clips_45seconds/'
    new_folder = '/home/quietswami/Documents/EMOMUSIC/WAV/'
    tmp_folder = '/home/quietswami/Documents/temp/'
    for i in os.listdir(base_folder):
        print('Starting {}'.format(i))
        name = tmp_folder + i.split('.')[0] + '.wav'
        converted_name = new_folder + i.split('.')[0] + '.wav'
        os.system('mplayer -vo null -ao pcm:file={} {}'.format(name, base_folder + i))
        os.system('sox {} -c 1 -b 16 -r 16000 {}'.format(name, converted_name))
        gc.collect()



