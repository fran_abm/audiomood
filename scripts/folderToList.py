import os
from pathlib import Path

pos = ["Screaming"]
neg = ["Speech", "Singing", "Yell", "Run"]
base_folder = str(Path.home()) + '/Documents/Base/'

all_list = []

name = str(Path.home()) + "/audioset_v3.txt"

for cla in pos:
    for audio in os.listdir(base_folder + cla):
        all_list.append("{}/{}\n".format(cla, audio))

number = int(len(all_list) / len(neg))

for cla in neg:
    for audio in os.listdir(base_folder + cla)[:number]:
        all_list.append("{}/{}\n".format(cla, audio))


with open(name, "w") as re: 
    for audio in all_list:
        re.write(audio)
