from youtube_dl import YoutubeDL


def extractInfo(t, filepath):
    for i in open(filepath, 'r').readlines():
        ydl = YoutubeDL()
        info = ydl.extract_info(i, download=False)
        yield (info["id"], t, info["title"], info["artist"], info["duration"])


def writeFile(path, data):
    with open(path, 'a+') as f:
        f.write(str(data))

if __name__ == '__main__':
    for i in extractInfo('happy-train', "happy_songs_train.txt"):
        writeFile('happy_train_table.txt', i)

    for i in extractInfo('sad-train', "sad_songs_train.txt"):
        writeFile('sad_train_table.txt', i)

    for i in extractInfo('happy-test', "happy_songs_test.txt"):
        writeFile('happy_test_table.txt', i)

    for i in extractInfo('sad-test', "sad_songs_test.txt"):
        writeFile('sad_test_table.txt', i)

    for i in extractInfo('happy-val', "happy_songs_val.txt"):
        writeFile('happy_val_table.txt', i)

    for i in extractInfo('sad-val', "sad_songs_val.txt"):
        writeFile('sad_val_table.txt', i)





