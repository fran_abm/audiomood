#
# This script returns the basic info from a given ubyte, in terms of number of images or items, the width and height of the data.
#


def get_header(input_file, file_type):
    if file_type == 'data':
        in_file = gzip.open(input_file, 'rb')
        
    elif file_type == 'label':
        in_file = gzip.open(input_file, 'rb')

    else:
        print("Error: File type does not exist...")

if __name__ == "__main__":
