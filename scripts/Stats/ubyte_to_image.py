# This script reads an UByte data file and prints out the several images in that file.

import sys, os
sys.path.insert(0, os.path.abspath('..'))
import numpy as np

from Audiomood import Ubyte
from Audiomood import Audio
from Audiomood import Spectogram
from Audiomood import Header
from Audiomood import Conversion
from PIL import Image
from collections import defaultdict

import scipy
import gzip
import wave
import struct
import librosa

def bytes2int(b):
    return int.from_bytes(b, "big")

def open_read_ubyte(file_path):
    with gzip.open(file_path, "rb") as o:
        # The first 4 bytes are for the magic number.
        # Currently we don't know if the magic number has any significance.
        magic = o.read(4)
        magicC =  int.from_bytes(magic, "big")
        print(magicC)
        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Magic Number before conversion: " + str(magic))
        
        # self.header.setMagic(magicC)

        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Magic Number: " + str(magicC))

        # # Beacause the first 8 bytes of the file mean the same thing in both types of files
        # #   we can read and set the headers without knowing the type.
        numOfItems = int.from_bytes(o.read(4), "big")
        print(numOfItems)
        numOfItems = 100
        # self.header.setNumOfItems(numOfItems)

        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfItems))

        # self.header.type = t

        # # This part of the header is only for the data files.
        # if self.type == "data":
        numOfRows = bytes2int(o.read(4))
        print(numOfRows)
        # self.header.setNumOfRows(numOfRows)
        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfRows))

        numOfCols = bytes2int(o.read(4))
        print(numOfCols)
        # self.header.setNumOfCols(numOfCols)
        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfCols))


        # # This routine might be optimized.
        # # We are reading until there is no more to read.
        # # TODO: Check if it can be broken.
        # n = True
        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Starting data collection...")
        

        # # This will be used to limit the iterations. 
        # it = 0

        # if self.header.getType() == "data":
        #     # If the type is data, we need to account for the number of pixels in all the images
        # it = self.header.getNumOfItems() * self.header.getNumOfRows() * self.header.getNumOfCols()
        # else:
        #     # If the type is labels, we only need to know how many items are there.
        #     it = self.header.getNumOfItems()
        data = []
        for i in range(100):
            data_lines = []
            for i in range(512):
                data_columns = []
                for i in range(32):
                    try:
                        a = bytes2int(o.read(1))
                        data_columns.append(a)
                    except:
                        Display.errorMessage("Ubyte", "parseFile", "Unable to read byte.")
                data_lines.append(data_columns)
            data.append(np.array(data_lines))

        for em, i in enumerate(data):
            img = Image.fromarray(data[0], 'L')
            img.save('Images/' + str(em) + '.png')

        # if self.debug:
        #     Display.debugMessage("Ubyte", "parseFile", "Finished data collection.")
        #     Display.debugMessage("Ubyte", "parseFile", "Number of data points: " + str(len(self.data)))


    

if __name__ == "__main__":
    open_read_ubyte('test_first_100-ubyte-idx3.gz')