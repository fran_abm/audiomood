
def extractAudio(filepath):
    os.system('ffmpeg -i {} -b:a 16k /home/francisco/Documents/movie.wav ')


def create1sWAV(filepath):
    time = audio_conversion.get_audio_duration(filepath)
    name = i.split('.')[0]

    for k in range(int(math.floor(time))):
        print("START: {} | END: {}".format(float(k), float(k+1)))
        os.system('sox {input} -t wavpcm {path}/{name}-{start}to{end}.wav trim {start} ={end}'.format(input=filepath, name=name, start=float(k), end=float(k+1), path='./movieSegments/'))


def analyzeMovie(modelPath):

    output = []

    # Add Model Here

    file = open(currentFolder + 'checkpoint', 'r')
    line = file.readline()
    modelfile = (line.split('"')[1] + '.meta')
    file.close()
    sess = tf.Session()
    new_saver = tf.train.import_meta_graph(currentFolder + modelfile)
    new_saver.restore(sess, tf.train.latest_checkpoint(currentFolder))
    graph = tf.get_default_graph()
    # x = graph.get_tensor_by_name('x:0')
    # y_ = graph.get_tensor_by_name('y_:0')
    # git = graph.get_tensor_by_name('git:0')
    # x_image = tf.reshape(x, [-1, 28, 28, 1])
    # w1 = graph.get_tensor_by_name('W1:0')
    # b1 = graph.get_tensor_by_name('B1:0')
    # w2 = graph.get_tensor_by_name('W2:0')
    # b2 = graph.get_tensor_by_name('B2:0')
    # w3 = graph.get_tensor_by_name('W3:0')
    # b3 = graph.get_tensor_by_name('B3:0')
    # w4 = graph.get_tensor_by_name('W4:0')
    # b4 = graph.get_tensor_by_name('B4:0')
    start_time = time.time()
    class_number = graph.get_tensor_by_name('class_number:0')

    for i in sorted(os.listdir('./movieSegments/')):
        data = dBMagnitudeStft8bits(i, 512, 32)
        classNumbers = class_number.eval(feed_dict={x:data}, session=sess)
        yield class_number



if __name__ == "__main__":
    extractAudio('/home/francisco/Downloads/...')
