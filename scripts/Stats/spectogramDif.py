#
# This script creates an image with the same implementation of the spectogram for several different audio tracks
#
import sys, os
sys.path.insert(0, os.path.abspath('..'))



import matplotlib.pyplot as plt
import numpy as np
from sys import argv
import os
from Audiomood import Conversion
import librosa


if __name__ == "__main__":
    folder = argv[1]
    allAudio = os.listdir(folder)
    ver = 1
    hor = 1
    plt.figure(figsize=(12, 8))
    plt.suptitle("Janelas = 1024, N. Mels = 128, Tamanho Hop = 512")
    for i in range(len(allAudio)):
        plt.subplot(len(allAudio), hor, ver)
        spec = Conversion.audioToSpec(folder + allAudio[i], 1024, 128, 512)
        librosa.display.specshow(librosa.power_to_db(spec.spectogram,ref=np.max),y_axis='mel', fmax=8000,x_axis='time')
        plt.colorbar(format='%+2.0f dB')
        ver += 1


    plt.savefig("test1.png")
    plt.show()

    