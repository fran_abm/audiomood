#
# This script will create six ubyte files, three pairs of label and data files.
#  One file with the training data, one with the validation data, and one with the testing data.
# This script is optimized for the TFSRC dataset.
#
import os
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import random
import numpy as np

from Audiomood import Ubyte
from Audiomood import Audio
from Audiomood import Spectogram
from Audiomood import Header
from Audiomood import Conversion


base_folder = "/home/quietswami/Downloads/TFSRC-train/train/"

def getAlternativeDatasetList(filename):
    """
    This functions creates a list of the files that belong in a alternative dataset.

    Args:
        - filename, a string with the filename (or filepath) of the file that contains the alternative dataset data.

    Returns:
        - A list with the alternative dataset sounds.
    """
    return[a.strip("\n") for a in  open(filename, "r").readlines()]


def getListOfFiles(testing, validation, namesOfFolders=None):
    """
    This function will divide by the folders names in the argument namesOfFolders, by three lists,
        one for training, one for validation, and one for testing.

    Args:
        - testing, a list with all the audio files that will be put in the testing file.
        - validation, a list with all the audio files that will be put in the validation file.
        - namesOfFolders, either a list or None. If None it will run all the folders. If a list, it will only run
            in the folders named.
    """
    audioFolder = base_folder + "audio/"
    allFolders = os.listdir(audioFolder)
    testingAudio = []
    trainingAudio = [] 
    validationAudio = []
    runningFolder = []
    
    if namesOfFolders == None:
        runningFolder = allFolders
    else:
        for i in namesOfFolders:
            if i in allFolders:
                runningFolder.append(i)
    
    for j in runningFolder:
        for i in os.listdir(audioFolder + j):
            if str(j + "/" +i) in testing:
                testingAudio.append({"path": audioFolder + j + "/" + i, "folder": j, "name": i})
            elif j + "/" +i in validation:
                validationAudio.append({"path": audioFolder + j + "/" + i, "folder": j, "name": i})
            else:
                trainingAudio.append({"path": audioFolder + j + "/" + i, "folder": j, "name": i})

    return trainingAudio, testingAudio, validationAudio


def convert_labels(label, labels):
    return labels.index(label)



def rectifyValues(spectogram, low, high):
    minimum = spectogram.min()
    maximum = spectogram.max()
    alpha = 0
    
    if minimum != maximum:
        alpha = (low - high)/(minimum - maximum)
    
    beta = low - alpha*minimum
    rectified = spectogram * alpha + beta
    return rectified


def cleanDataset(s, l):
    """
    This functions creates two ubyte objects, one for the data and one for the labels.

    Args:
        - s, is the set that will be used to create the object.

    Returns:
        - An Ubyte object with the data in it.
    """
    labels = []
    data = []
    for i in s:
        labels.append(convert_labels(i["folder"], l))
        spec = Conversion.audioToSpec(i["path"])

        # Value rectification and rounding up.
        rec = np.rint(rectifyValues(spec.spectogram,0,255))
        toAppend = []
        for j in rec:
            for k in j:
                toAppend.append(k)
        diff = 128*44 - len(toAppend)
        if diff > 0:
            for i in range(diff):
                toAppend.append(0)
        for j in toAppend:
            data.append(int(j)) 
    shape = spec.spectogram.shape
    return data, labels, shape


def createUbyte(data, labels, shape):
    d = Ubyte.Ubyte()
    l = Ubyte.Ubyte()
    dH = Header.Header()
    lH = Header.Header()
    dH.setNumOfRows(shape[0])
    dH.setNumOfCols(shape[1])
    dH.setNumOfItems(len(labels))
    lH.setType("labels")
    lH.setMagic(2049)
    lH.setNumOfItems(len(labels))
    d.setHeader(dH)
    l.setHeader(lH)
    d.setData(data)
    l.setData(labels)
    return d, l


if __name__ == "__main__":
    listFolders = ["no", "yes", "zero"]
    testing = getAlternativeDatasetList(base_folder + "testing_list.txt")
    validation = getAlternativeDatasetList(base_folder + "validation_list.txt")
    trainSet,testSet,valSet = getListOfFiles(testing, validation, listFolders)
    trainSet = trainSet[:1000]
    testSet = testSet[:1000]
    valSet = valSet[:1000]
    d, l, s = cleanDataset(trainSet, listFolders)
    d, l = createUbyte(d, l, s)
    if not os.path.exists(base_folder + "ubyte/"):
        os.mkdir(base_folder + "ubyte/")
    d.saveFile(base_folder+ "ubyte/TFSRC-train-data-idx3-ubyte")
    l.saveFile(base_folder+ "ubyte/TFSRC-train-label-idx3-ubyte")
    d, l, s = cleanDataset(testSet, listFolders)
    d, l = createUbyte(d, l, s)
    d.saveFile(base_folder+ "ubyte/TFSRC-testing-idx3-ubyte")
    l.saveFile(base_folder+ "ubyte/TFSRC-testing-label-idx3-ubyte")
    d, l, s = cleanDataset(valSet, listFolders)
    d, l = createUbyte(d, l, s)
    d.saveFile(base_folder+ "ubyte/TFSRC-validation-data-idx3-ubyte")
    l.saveFile(base_folder+ "ubyte/TFSRC-validation-label-idx3-ubyte")



    

