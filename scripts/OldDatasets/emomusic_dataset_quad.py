#! /bin/usr/env python3

#
# EMOMUSIC Dataset ETL Script
#   Given the EMOMUSIC dataset, this script extracts, transforms and creates a Ubyte file, to be used to train, test and validate a
#   machine learning model.
#
#   This particular scripts takes the VA values, and returns the quadrant where the VA for a particular audio track is.
#
#   Overall, the dynamic annotations have 61 VA values for 744 of the 1000 songs. This 61 VA values start beign recorded at the 14 second mark.
#   This values are recorded every 0.5 seconds.
#   Because we are taking a 1 second slice from the music, we are going to give that slice the VA value of the second where the slice begins,
#       so if a slice begins at the 14 second mark and ends at the 15 second mark, that slice gets the VA value of the 14 second mark.
#
# TODO:
#   - There needs to be an adaptation of the system to take in consideration the static value, instead of the dynamic value.
#
#



# Libs
import audio_conversion
import datetime
import os
import random
import csv
import itertools
import numpy as np
import gzip
import sys


# Global Variables
debug_flag = False
rows = 512
columns = 32

def elapsed_time(start, end):
    """
    Returns the elapsed time between two timestamps.

    Args:
        - start: Datetime, a datetime object with the starting timestamp.
        - end: Datetime, a datetime object with the end timestamp.
    """
    return end - start

def timestamp():
    """
    Returns the current time in the correct timestamp format.

    Returns:
        - A datetime object, with the current time in the correct format.
    """
    return datetime.datetime.strptime(datetime.datetime.now().strftime("%d/%m/%y %H:%M"), "%d/%m/%y %H:%M")

def annotation_statistics(arousal_csv, valence_csv):
    if debug_flag:
        print("{} Annotation Statistics Started".format(timestamp()))

    arousal = open(arousal_csv, 'r')
    arousal_reader = csv.reader(arousal)
    next(arousal_reader) # this is used to pass the header.

    valence = open(valence_csv, 'r')
    valence_reader = csv.reader(valence)
    next(valence_reader) # the same as above.

    number_arousal = 0
    ids_arousal = []
    for i in arousal_reader:
        number_arousal += 1
        ids_arousal.append(i[0])

    number_valence = 0
    ids_valence = []
    for i in valence_reader:
        number_valence += 1
        ids_valence.append(i[0])

    print(number_arousal)
    print(number_valence)
    print(ids_valence)
    print(ids_arousal)
    if ids_arousal == ids_valence:
        print("EQUAL")

def create_wav_from_mp3(audio_path, target_folder):
    """
    This functions transforma the audio files in a folder to WAV files, and stores them in a given location.

    Args:
        - audio_path: String, the path to the audio sources to be transform.
        - target_folder: String: the path to where the WAV files will be stored.
    """
    start = timestamp()
    if debug_flag:
        print("{} Starting create_wav_from_mp3".format(timestamp()))

    if not os.path.isdir(target_folder):
        if debug_flag:
            print("{} does not exist. Creating.".format(target_folder))
        os.mkdir(target_folder)

    for i in os.listdir(audio_path):
        if debug_flag:
            print("Starting conversion of: {}".format(i))
        name = i.split(".")[0]
        audio_conversion.audio_to_wav(audio_path + i, target_folder + name + ".wav")

    end = timestamp()
    if debug_flag:
        print("{} Finished create_wav_from_mp3".format(timestamp()))
        print("Elapsed Time: {}".format(elapsed_time(start, end)))

def calculate_quadrant(anno):
    """
    Calculates the axis that divides the 4 quadrants.

    Args:
        - anno, list, a list with all the VA values.

    Returns:
        - tuple, containing 2 values, the first tha average A values and the second the average V value.
    """

    max_a = -1000
    max_v = -1000
    min_a = 1000
    min_v = 1000

    for j in anno:
        for i in anno[j]:
            if int(i[0]) < min_a:
                min_a =int(i[0])
            if int(i[0]) > max_a:
                max_a = int(i[0])

            if int(i[1]) < min_v:
                min_v = int(i[1])
            if int(i[1]) > max_a:
                max_v = int(i[1])

    avg_a = int((max_a + min_a)/2)
    avg_v = int((max_v + min_v)/2)
    return (avg_a, avg_v)

def quadrant(curr_v, curr_a, avg_v, avg_a):
    """
    This function converts the given VA combination to a quadrant of all of the VA values.

    Args:
        - curr_v: integer, a value between 0 and 255.
        - curr_a: integer, a value between 0 and 255.
        - avg_v: integer, a value between 0 and 255.
        - avg_a: integer, a value between 0 and 255.

    Returns:
        - integer, a value betwen 0 and 3, equivalent to the VA.
    """

    if curr_a > avg_a and curr_v > avg_v:
        return 0
    elif curr_a < avg_a and curr_v > avg_v:
        return 1
    elif curr_a < avg_a and curr_v < avg_v:
        return 2
    elif curr_a > avg_a and curr_v < avg_v:
        return 3

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.

    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0

    if mn != mx:
        alpha = (low - high)/(mn - mx)

    beta = low - alpha * mn
    return value * alpha + beta

def create_annotations(a_csv, v_csv):
    """
    Reads, and creates a data structure with the values of the arousal and valence for each second of each song.

    Args:
        - a_csv: String, the path to a csv file, containing the arousal values.
        - v_csv: String, the path to a csv file, containing the valence values

    Returns:
        - A dictionary containing, for each audio track, and for each second of music, the values for
            arousal and valance.
    """
    if debug_flag:
        print("{} Create Annotation Started".format(timestamp()))

    arousal = open(arousal_csv, 'r')
    arousal_reader = csv.reader(arousal)
    next(arousal_reader) # this is used to pass the header.

    valence = open(valence_csv, 'r')
    valence_reader = csv.reader(valence)
    next(valence_reader) # the same as above.


    dic = {}
    for arousal, valence in itertools.zip_longest(arousal_reader, valence_reader):
        dic[arousal[0]] = list(zip(arousal[1:], valence[1:]))

    return dic

def convert_annotations(ano):
    aro_min = 10000
    aro_max = -10000
    val_min = 10000
    val_max = -10000

    for k,v in ano.items():
        l = []
        for i in v:
            j = np.array(i).astype(float)
            if j[0] > aro_max:
                aro_max = j[0]
            if j[0] < aro_min:
                aro_min = j[0]
            if j[1] > val_max:
                val_max = j[1]
            if j[1] < val_min:
                val_min = j[1]
            l.append(j)
        ano[k] = l

    for k,v in ano.items():
        l = []
        for i in v:
            aro = int(rectify_values(0, 255, aro_min, aro_max, i[0]))
            val = int(rectify_values(0, 255, val_min, val_max, i[1]))
            l.append(np.array([aro, val]).astype(int))
        ano[k] = l
    return ano

def convert_data(data):
    aro_min = 1000000
    aro_max = -1000000

    a = []

    for i in data:
        if i > aro_max:
            aro_max = i
        if i < aro_min:
            aro_min = i

    b = []
    for k,v in enumerate(data):
        sys.stdout.write("Done: %f%%   \r" % ((k/len(data))*100))
        sys.stdout.flush()
        a = rectify_values(0, 255, aro_min, aro_max, v)
        b.append(int(a))
    return b

def dataset_division(anno, seed):
    """
    As the dataset creators didn't give a specific division of the dataset between the 3 types, this function\
    will do a 60-20-20 split between training, validation and testing, and shuffles the audio tracks.

    Args:
        - folder_path: String, the path of the files to be divided between the 3 datasets.
        - seed: int, a seed to keep the randomness consistent between instances.
    """
    if debug_flag:
        print("{} Dataset Division Started".format(timestamp()))
    files = [i for i in anno]
    random.seed(seed)
    random.shuffle(files)
    train = files[:int(len(files) * 0.6)]
    val = files[int(len(files)*0.6) : int(len(files)*0.8)]
    test = files[int(len(files)*0.8) :]
    return train, val, test

def build_dataset(annotations, audio_path, avg):

    if debug_flag:
        print("{} Starting built_database".format(timestamp()))

    new_dict = {}

    for k,v in annotations.items():
        current_second = 14
        audio_location = audio_path + k + '.wav'
        for i in v:
            if debug_flag:
                sys.stdout.write("Starting second {} for audio {}\r".format(current_second, k))
                sys.stdout.flush()
            data, samp_rate = audio_conversion.read_audio_to_wav(audio_location, samp=16000, offset=current_second, duration=1)
            data = audio_conversion.db_magnitude_stft_8bits(data, rows, columns, samp_rate)
            if k not in new_dict.keys():
                new_dict[k] = {current_second: [data, quadrant(i[0], i[1], avg[0], avg[1])]}
            elif current_second not in new_dict[k].keys():
                new_dict[k][current_second] = [data, quadrant(i[0], i[1], avg[0], avg[1])]
            else:
                new_dict[k][current_second] = [data, quadrant(i[0], i[1], avg[0], avg[1])]
            current_second += 0.5

    return new_dict

def create_files(data_name, label_name, audio_list, annotations):

    if debug_flag:
        print("{} Starting create_files".format(timestamp()))

    label_sequence = []
    # To limit simultaneos IO ops, we will put all the ids by sequence here
    #   and later we will insert them to the label file.

    # Data Insertion
    send_to_file = []
    for i in audio_list:
        for k, j in annotations[i].items():
            sys.stdout.write("Done: %f%%   \r" % ((k/len(audio_list))*100))
            sys.stdout.flush()
            if j[1] != None:
                send_to_file.extend(audio_conversion.flatten_list(j[0]))
                label_sequence.append(j[1])

    print("Starting Convert Data")
    a = convert_data(send_to_file)

    print("Writing Data to File")
    audio_conversion.write_to_file(data_name, a, 3, (len(audio_list) * 61), 512, 32)

    # Label Insertion
    print("Writing Labels to File")
    audio_conversion.write_to_file(label_name, label_sequence, 2, (len(audio_list) *61), 1)


if __name__ == "__main__":

    debug_flag = True

    start_time = datetime.datetime.now()

    #
    # Folder and dataset naming
    #

    base_folder = '/home/quietswami/Documents/WAV/'
    annotation_folder = base_folder + "annotations/"
    static = annotation_folder + 'static_annotations.csv'
    arousal_csv = base_folder + 'arousal_cont_average.csv'
    valence_csv = base_folder + 'valence_cont_average.csv'
    audio_path = base_folder + 'clips_45seconds/'
    wav_folder = base_folder + "emo_wav/"

    training_data_name = base_folder + "emomusic_train_quad_data-ubyte.gz"
    training_labels_name = base_folder + "emomusic_train_quad_labels-ubyte.gz"

    testing_data_name = base_folder + "emomusic_test_quad_data-ubyte.gz"
    testing_labels_name = base_folder + "emomusic_test_quad_labels-ubyte.gz"

    val_data_name = base_folder + "emomusic_val_quad_data-ubyte.gz"
    val_labels_name = base_folder + "emomusic_val_quad_labels-ubyte.gz"

    # If Needed!
    # create_wav_from_mp3(audio_path, wav_folder)

    # Annotation Creation
    anno = convert_annotations(create_annotations(arousal_csv, valence_csv))

    quad_coords = calculate_quadrant(anno)

    # Dataset split
    seed = 11
    train_list, val_list, test_list = dataset_division(anno, seed)

    a = build_dataset(anno, wav_folder, quad_coords)


    # Creation of dataset


    print("Starting TRAINING")
    create_files(training_data_name, training_labels_name, train_list, a)

    print("Starting VALIDATION")
    create_files(val_data_name, val_labels_name, val_list, a)

    print("Starting TESTING")
    create_files(testing_data_name, testing_labels_name, test_list, a)


    end_time = datetime.datetime.now()
    if debug_flag:
        print("Elapsed Time: {}".format(elapsed_time(start_time, end_time)))