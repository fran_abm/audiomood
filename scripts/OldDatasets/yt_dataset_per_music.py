import audio_conversion
import datetime
import os
import random
import csv
import itertools
import numpy as np
import gzip
import sys
import yt_downloader
from pathlib import Path
import stftFeatures
from shutil import copy2, rmtree
import librosa
import ffmpeg as ff
import sox
import math




def download_songs(d):
    """This function downloads and creates the folder structure for the dataset.
    Checks if the folder exists, if not creates it.

    Arguments:
        d -- A dict with very specific rules.
            {"folder_path": "file_path"} - where folder path is where the audio will be stored, and file path, the path of
                the text file with the links for the songs.
    """
    # for k,v in d.items():
    #     if not os.path.isdir(k):
    #         print("{} does not exist. Creating...".format(k))
    #         os.mkdir(k)
    #     name = k.split("/")[-2]
    #     for i in open('YT_Datasets/' + v, 'r').readlines():
    os.system('youtube-dl -x --audio-format "wav" --audio-quality 0 --output "./MusicDownloads/%(id)s.%(ext)s" {}'.format(d))


def downsample(folderPath):
    for i in os.listdir(folderPath):
        if os.path.isfile(folderPath + i):
            print(folderPath + i)
            os.system('sox {} -c 1 -b 16 -r 16000 ./MusicDownloads/A1AJEv50Ld4_16000.wav'.format(folderPath+i))
            os.unlink(folderPath + i)


def createFolderByMusic(baseFolder):
    for i in os.listdir(baseFolder):
        folderName = i.split(".")[0]
        os.mkdir(baseFolder + folderName)
        copy2(baseFolder + i, baseFolder + folderName)
        os.unlink(baseFolder + i)

def create1sWAV(folderpath):
    for i in os.listdir(folderpath):
        # os.mkdir('{}/samples'.format(folderpath))
        if i.endswith('.wav'):
            time = audio_conversion.get_audio_duration(folderpath + i)
            for k in range(int(math.floor(time))):
                print("START: {} | END: {}".format(float(k), float(k+1)))
                os.system('sox {input} -t wavpcm {path}/A1AJEv50Ld4-{start}to{end}.wav trim {start} ={end}'.format(input=folderpath+i, start=float(k), end=float(k+1), path=folderpath+'samples'))
            # name = j.split(".")[0]
            # path = folderpath + i + "/"  + j
            # newPath = folderpath + i + "/" + name + "-"
            # time = float(audio_conversion.get_audio_duration(path))
            # for j in np.arange(0.0, time - 0.5, 0.5):
            #     data, samp = audio_conversion.read_audio_to_wav(path, samp=16000, offset=j, duration=1)
            #     audio_conversion.save_wav(newPath + str(j) + ".wav", data, samp)
            # os.unlink(path)

def create_dataset(data, labels, dataname, labelname, height, width):
    stftFeatures.writeStft8bitsFiles(data, dataname, height, width)

    # Label Creation
    outputFile = gzip.open(labelname,"wb")
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(labels)).byteswap()).tobytes())
    for i in labels:
        print(i)
        outputFile.write(int(i).to_bytes(1, "big"))
    outputFile.close()

def createPreDataset(folderPath, label):
    labels = []
    data = []
    for i in os.listdir(folderPath):
        if i.endswith('.wav'):
            data.append(folderPath + "/"+ i)
            labels.append(label)

    return data, labels


def deleteFiles(folder_path):
    for i in os.listdir(folder_path):
        if os.path.isfile(folder_path + i):
            os.unlink(folder_path + i)

def deleteFolders(folder_path):
    rmtree(folder_path)

if __name__ == "__main__":

    debug_flag = True
    path = str(Path.home())
    start_time = datetime.datetime.now()

    #
    # Folder and dataset naming
    #

    base_folder = '{}/'.format(path)

    wav_folder = base_folder + "Desktop/"

    # dataset_folder = wav_folder + "Datasets/"


    if not os.path.isdir(wav_folder):
        os.mkdir(wav_folder)

    # if not os.path.isdir(dataset_folder):
    #     os.mkdir(dataset_folder)

    happy_train = wav_folder + "happy_train/"
    sad_train = wav_folder + "Desktop/"
    # scream_train = wav_folder + "scream_train/"

    happy_test = wav_folder + "happy_test/"
    sad_test = wav_folder + "sad_test/"
    # scream_train = wav_folder + "scream_test/"

    happy_val = wav_folder + "happy_val/"
    sad_val = wav_folder + "sad_val/"
    # scream_train = wav_folder + "scream_test/"

    d = {
        # happy_train: 'happy_songs_train.txt',
        sad_train: 'sad_songs_train.txt',
        # scream_train: 'scream_train.txt',
        # happy_test: 'happy_songs_test.txt',
        # sad_test: 'sad_songs_test.txt',
        # # scream_test: 'scream_test.txt',
        # happy_val: 'happy_songs_val.txt',
        # sad_val: 'sad_songs_val.txt',
        # scream_val: 'scream_val.txt'
    }



    # if len(os.listdir(happy_train)) > 0:
    #     deleteFiles(happy_train)
    #     deleteFolders(happy_train)
    # if len(os.listdir(sad_train)) > 0:
    #     deleteFiles(sad_train)
    #     deleteFolders(sad_train)


    # if len(os.listdir(happy_test)) > 0:
    #     deleteFiles(happy_test)
    #     deleteFolders(happy_test)
    # if len(os.Podes correr isso e fazer um ubyte com os wavs? correr isso e fazer um ubyte com os wavs?r(sad_test)) > 0:
    #     deleteFiles(sad_test)
    #     deleteFolders(sad_test)

    # if len(os.listdir(happy_val)) > 0:
    #     deleteFiles(happy_val)
    #     deleteFolders(happy_val)
    # if len(os.listdir(sad_val)) > 0:
    #     deleteFiles(sad_val)
    #     deleteFolders(sad_val)

    download_songs('https://www.youtube.com/watch?v=A1AJEv50Ld4')
    downsample('./MusicDownloads/')

    # createFolderByMusic(sad_train)
    create1sWAV('./MusicDownloads/')

    # createFolderByMusic(happy_test)
    # create1sWAV(happy_train)

    # createFolderByMusic(sad_test)
    # create1sWAV(sad_test)

    # createFolderByMusic(happy_val)
    # create1sWAV(happy_val)

    # createFolderByMusic(sad_val)
    # create1sWAV(sad_val)

    # for i in os.listdir('/home/francisco/Desktop'):
    training_data_name = "sox_test-data-ubyte.gz"
    training_labels_name = "sox_test-labels-ubyte.gz"
    train_data, train_label = createPreDataset('/home/francisco/audiomood/scripts/MusicDownloads/samples', 1)
    create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)


    # for i in os.listdir(happy_test):
    #     training_data_name = dataset_folder +  i + "-data-ubyte.gz"
    #     training_labels_name = dataset_folder + i + "-labels-ubyte.gz"
    #     train_data, train_label = createPreDataset(happy_test + i, 0)
    #     create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)


    # for i in os.listdir(sad_test):
    #     training_data_name = dataset_folder +  i + "-data-ubyte.gz"
    #     training_labels_name = dataset_folder + i + "-labels-ubyte.gz"
    #     train_data, train_label = createPreDataset(sad_test + i, 1)
    #     create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)


    # for i in os.listdir(happy_val):
    #     training_data_name = dataset_folder +  i + "-data-ubyte.gz"
    #     training_labels_name = dataset_folder + i + "-labels-ubyte.gz"
    #     train_data, train_label = createPreDataset(happy_val + i, 0)
    #     create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)


    # for i in os.listdir(sad_val):
    #     training_data_name = dataset_folder +  i + "-data-ubyte.gz"
    #     training_labels_name = dataset_folder + i + "-labels-ubyte.gz"
    #     train_data, train_label = createPreDataset(sad_val + i, 1)
    #     create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)



    end_time = datetime.datetime.now()
    elapsed_time = end_time - start_time
    print("Elapsed Time: {}".format(elapsed_time))