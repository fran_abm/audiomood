
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import random
import numpy as np
import datetime

from collections import defaultdict
import itertools
import csv
import time

import scipy
from scipy import io
from scipy.io import wavfile
from scipy import signal
import gzip
import wave
import struct
import librosa

def create_annotations(arousal_csv, valence_csv):
    print("Starting Annoation Creation")

    arousal = open(arousal_csv, 'r')
    arousal_reader = csv.reader(arousal)
    next(arousal_reader)

    valence = open(valence_csv, 'r')
    valence_reader = csv.reader(valence)
    next(valence_reader)
    
    dic = {}
    for arousal, valence in itertools.zip_longest(arousal_reader, valence_reader):
        dic[arousal[0]] = list(zip(arousal[1:], valence[1:]))

    return dic


def read_test_segments(test_path):
    with open(test_path, 'r') as f:
        a = {}
        for i in f.readlines():
            line = i.strip().split('/')[-1]
            name = line.split('-')[0]
            start = line.split('-')[-1].split('to')[0]
            stop = line.split('-')[-1].split('to')[-1][:-4]
            if name not in a.keys():
                a[name] = []
            a[name].append({'start': start, 'stop': stop})
        return a

def read_def_files(file_path):
    with open(file_path, 'r') as f:
        a = []
        for i in f.readlines():
            a.append(i.strip().split('/')[-1].split('.')[0])
        return a

def create_labels(l, an, num_items, label_file_path):
    output_file = gzip.open(label_file_path,"wb")
    output_file.write(b'\x00')
    output_file.write(b'\x00')
    output_file.write(b'\x0D') # This identifies the type of data that will be put in the file. In this case a float.
    output_file.write(b'\x02') # And this the dimension of the labels. In this case a matrice nX2
    output_file.write((np.int32(num_items).byteswap()).tobytes())
    output_file.write((np.int32(2).byteswap()).tobytes())
    output_file.close()
    for i in l:
        if i in an.keys():
            line = an[i]
            for j in line:
                j = np.array(j).astype(float)
                f = gzip.open(label_file_path, 'wb')
                f.write(j[0])
                f.write(j[1])
                f.close()

def create_test_labels(file, an, label_file_path):
    lines = open(file, 'r').readlines()

    output_file = gzip.open(label_file_path,"wb")
    output_file.write(b'\x00')
    output_file.write(b'\x00')
    output_file.write(b'\x0D') # This identifies the type of data that will be put in the file. In this case a float.
    output_file.write(b'\x02') # And this the dimension of the labels. In this case a matrice nX2
    output_file.write((np.int32(len(lines)).byteswap()).tobytes())
    output_file.write((np.int32(2).byteswap()).tobytes())
    output_file.close()
    for i in lines:
        r = enumerate(np.arange(14, 44.5, 0.5))
        line = i.strip().split('/')[-1]
        name = line.split('-')[0]
        start = line.split('-')[-1].split('to')[0]
        stop = line.split('-')[-1].split('to')[-1][:-4]
        if name in an.keys():
            for k, j in r:
                print('{} at {}'.format(j, start))
                if float(j) == float(start):
                    l = np.array(an[name][k]).astype(float)
                    o = gzip.open(label_file_path, 'wb')
                    o.write(l[0])
                    o.write(l[1])
                    o.close()

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    base_folder = '/home/quietswami/Documents/EMOMUSIC/'
    train_files = base_folder + 'training-files.txt'
    val_files =  base_folder + 'validation-files.txt'
    test_files = base_folder + 'test-segments.txt'
    arousal_csv = base_folder + 'arousal_cont_average.csv'
    valence_csv = base_folder + 'valence_cont_average.csv'


    a = create_annotations(arousal_csv, valence_csv)


    train = read_def_files(train_files)
    val = read_def_files(val_files)
    test = read_test_segments(test_files)

    create_labels(train, a, len(train),'train-labels-ubyte.gz')
    create_labels(val, a, len(val),'val-labels-ubyte.gz')
    create_test_labels(test_files, a, 'test-labels-ubyte.gz')