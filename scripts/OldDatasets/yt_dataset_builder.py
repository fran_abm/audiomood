#! /bin/usr/env python3

# Libs
import audio_conversion
import datetime
import os
import random
import csv
import itertools
import numpy as np
import gzip
import sys
import yt_downloader
from pathlib import Path
import math
import stftFeatures


# Global Variables
debug_flag = False
rows = 512
columns = 32


#
# Youtube Dataset Builder
#
#   This script creates a ubyte files when given a list of folder containing WAV that have been downloaded from youtube, using the
#   yt_downloader script.
#
#   This script divides the dataset into the 3 ubyte files (train, test and validation) by a percentage - shuffles the audio files before
#   it cuts them into 1 second pieces.
#

def analyse_folder(folderpath):
    """Checks if the given folder exists, and if there are any wav files in it.

    Arguments:
        folderpath {str} -- The path for the folder to be analysed.

    Returns:
        Boolean - if True, the folder exists and contains wav files.
    """
    if os.path.isdir(folderpath):
        if ".wav" in [os.path.splitext(i)[1] for i in os.listdir(folderpath)]:
            return True
        else:
            return False
    else:
        return False

def get_wav_stats(filepath):
    """Returns a Dictionary with information of the wav file.

    Arguments:
        filepath {str} -- The filepath to the wav file to be analysed.

    Returns:
        dict -- contains the information.
    """
    return {}

def elapsed_time(start, end):
    """
    Returns the elapsed time between two timestamps.

    Args:
        - start: Datetime, a datetime object with the starting timestamp.
        - end: Datetime, a datetime object with the end timestamp.
    """
    return end - start

def timestamp():
    """
    Returns the current time in the correct timestamp format.

    Returns:
        - A datetime object, with the current time in the correct format.
    """
    return datetime.datetime.strptime(datetime.datetime.now().strftime("%d/%m/%y %H:%M"), "%d/%m/%y %H:%M")

def check_folder(folder):
    """Checks if folder exists and if it has any WAV files in it.

    Arguments:
        folder {str} -- Folder path.

    Returns:
        bool -- True if the folder exists, and if it has any WAV files.
    """
    if os.path.isdir(folder):
        paths = [os.path.splitext(folder + i)[1] for i in os.listdir(folder)]
        if ".wav" in paths:
            return True
        else:
            return False
    else:
        return False

def get_classification(name):
    """Returns a value between 0 and n - where n is the number of classification available.
    There is a global dictionary with all the possible value pair.

    Arguments:
        name {str} -- The classification name to be converted to a integer.

    Returns:
        int -- The converted classification.
    """
    return enum[name]


def convert_data(data):
    aro_min = 1000000
    aro_max = -1000000

    a = []

    for i in data:
        if i > aro_max:
            aro_max = i
        if i < aro_min:
            aro_min = i

    b = []
    for k,v in enumerate(data):
        sys.stdout.write("Done: %f%%   \r" % ((k/len(data))*100))
        sys.stdout.flush()
        a = rectify_values(0, 255, aro_min, aro_max, v)
        b.append(int(a))
    return b

def create_wav_from_mp3(audio_path, target_folder):
    """
    This functions transforma the audio files in a folder to WAV files, and stores them in a given location.

    Args:
        - audio_path: String, the path to the audio sources to be transform.
        - target_folder: String: the path to where the WAV files will be stored.
    """
    start = timestamp()
    if debug_flag:
        print("{} Starting create_wav_from_mp3".format(timestamp()))

    if not os.path.isdir(target_folder):
        if debug_flag:
            print("{} does not exist. Creating.".format(target_folder))
        os.mkdir(target_folder)

    for i in os.listdir(audio_path):
        if debug_flag:
            print("Starting conversion of: {}".format(i))
        name = i.split(".")[0]
        audio_conversion.audio_to_wav(audio_path + i, target_folder + name + ".wav")

    end = timestamp()
    if debug_flag:
        print("{} Finished create_wav_from_mp3".format(timestamp()))
        print("Elapsed Time: {}".format(elapsed_time(start, end)))

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.

    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0

    if mn != mx:
        alpha = (low - high)/(mn - mx)

    beta = low - alpha * mn
    return value * alpha + beta

def convert_data(data):
    aro_min = 1000000
    aro_max = -1000000

    a = []

    for i in data:
        if i > aro_max:
            aro_max = i
        if i < aro_min:
            aro_min = i

    b = []
    for k,v in enumerate(data):
        sys.stdout.write("Done: %f%%   \r" % ((k/len(data))*100))
        sys.stdout.flush()
        a = rectify_values(0, 255, aro_min, aro_max, v)
        b.append(int(a))
    return b

def dataset_division(files, seed):
    """
    As the dataset creators didn't give a specific division of the dataset between the 3 types, this function\
    will do a 60-20-20 split between training, validation and testing, and shuffles the audio tracks.

    Args:
        - files: List, a list with the dataset fully loaded.
        - seed: int, a seed to keep the randomness consistent between instances.
    """
    if debug_flag:
        print("{} Dataset Division Started".format(timestamp()))
    random.seed(seed)
    random.shuffle(files)
    train = files[:int(len(files) * 0.6)]
    val = files[int(len(files)*0.6) : int(len(files)*0.8)]
    test = files[int(len(files)*0.8) :]
    return train, val, test

def download_songs(d):
    """This function downloads and creates the folder structure for the dataset.
    Checks if the folder exists, if not creates it.

    Arguments:
        d -- A dict with very specific rules.
            {"folder_path": "file_path"} - where folder path is where the audio will be stored, and file path, the path of
                the text file with the links for the songs.
    """
    for k,v in d.items():
        if not os.path.isdir(k):
            print("{} does not exist. Creating...".format(k))
            os.mkdir(k)

        for i in open('YT_Datasets/' + v, 'r').readlines():
            os.system('youtube-dl -x --audio-format "wav" --audio-quality 0 --output "{}%(id)s.%(ext)s" {}'.format(k, i))

            # yt_downloader.download(i.strip(), audio_output = True,  output_folder = k, output_name = name, output_filetype = "wav")


def downsample(folderPath):
    for i in os.listdir(folderPath):
        if os.path.isfile(folderPath + i):
            print(folderPath + i)
            name = i.split('.')[0]
            os.system('sox {} -c 1 -b 16 -r 16000 {}{}_16000.wav'.format(folderPath+i, folderPath, name))
            os.unlink(folderPath + i)

def create1sWAV(folderpath):
    for i in os.listdir(folderpath):
        # os.mkdir('{}/samples'.format(folderpath))
        if i.endswith('.wav'):
            time = audio_conversion.get_audio_duration(folderpath + i)
            name = i.split('.')[0]

            for k in range(int(math.floor(time))):
                print("START: {} | END: {}".format(float(k), float(k+1)))
                os.system('sox {input} -t wavpcm {path}/{name}-{start}to{end}.wav trim {start} ={end}'.format(input=folderpath+i, name=name, start=float(k), end=float(k+1), path=folderpath))
            os.unlink(folderpath+i)

def create_preset(p):
    d = []
    for i in os.listdir(p):
        path = p + i
        time = int(audio_conversion.get_audio_duration(path))
        for j in range(time):
            data, samp = audio_conversion.read_audio_to_wav(path, samp=16000, offset=j, duration=1)
            data = audio_conversion.db_magnitude_stft_8bits(data, 512, 32, samp)
            d.append(data)
    return d

def createPreDataset(happyFolderPath, sadFolderPath):
    labels = []
    data = []
    for i in os.listdir(happyFolderPath):
        if i.endswith('.wav'):
            data.append(happyFolderPath + i)
            labels.append(0)


    for i in os.listdir(sadFolderPath):
        if i.endswith('.wav'):
            data.append(sadFolderPath + i)
            labels.append(1)

    return data, labels

def create_dataset(data, labels, dataname, labelname, height, width):
    stftFeatures.writeStft8bitsFiles(data, dataname, height, width)

    # Label Creation
    outputFile = gzip.open(labelname,"wb")
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(labels)).byteswap()).tobytes())
    for i in labels:
        print(i)
        outputFile.write(int(i).to_bytes(1, "big"))
    outputFile.close()


if __name__ == "__main__":

    debug_flag = True
    path = str(Path.home())
    print(path)
    start_time = datetime.datetime.now()

    #
    # Folder and dataset naming
    #

    base_folder = '{}/Documents/'.format(path)


    wav_folder = base_folder + "WAV/"
    if not os.path.isdir(wav_folder):
        os.mkdir(wav_folder)

    happy_train = wav_folder + "happy_train/"
    sad_train = wav_folder + "sad_train/"
    # scream_train = wav_folder + "scream_train/"

    happy_test = wav_folder + "happy_test/"
    sad_test = wav_folder + "sad_test/"
    # scream_train = wav_folder + "scream_test/"

    happy_val = wav_folder + "happy_val/"
    sad_val = wav_folder + "sad_val/"
    scream_train = wav_folder + "scream_test/"

    training_data_name = base_folder + "yt_happy_sad_train_data-ubyte.gz"
    training_labels_name = base_folder + "yt_happy_sad_train_labels-ubyte.gz"

    testing_data_name = base_folder + "yt_happy_sad_test_data-ubyte.gz"
    testing_labels_name = base_folder + "yt_happy_sad_test_labels-ubyte.gz"

    val_data_name = base_folder + "yt_happy_sad_val_data-ubyte.gz"
    val_labels_name = base_folder + "yt_happy_sad_val_labels-ubyte.gz"

    # If Needed!

    song = {
        happy_train: 'happy_songs_train.txt',
        sad_train: 'sad_songs_train.txt',
        # scream_train: 'scream_train.txt',
        happy_test: 'happy_songs_test.txt',
        sad_test: 'sad_songs_test.txt',
        # scream_test: 'scream_test.txt',
        happy_val: 'happy_songs_val.txt',
        sad_val: 'sad_songs_val.txt',
        # scream_val: 'scream_val.txt'
    }

    download_songs(song)

    for i in song:
        downsample(i)

    for i in song:
        create1sWAV(i)

    train_data, train_label = createPreDataset(happy_train, sad_train)
    create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)

    test_data, test_label = createPreDataset(happy_test, sad_test)
    create_dataset(test_data, test_label, testing_data_name, testing_labels_name, 512, 32)

    val_data, val_label = createPreDataset(happy_val, sad_val)
    create_dataset(val_data, val_label, val_data_name, val_labels_name, 512, 32)

    end_time = datetime.datetime.now()
    if debug_flag:
        print("Elapsed Time: {}".format(elapsed_time(start_time, end_time)))