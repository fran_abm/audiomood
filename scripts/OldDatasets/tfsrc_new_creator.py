import os
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import random
import numpy as np
from collections import OrderedDict
import time
import datetime
import gzip
import librosa
import scipy

from Audiomood import Ubyte
from Audiomood import Audio
from Audiomood import Spectogram
from Audiomood import Header
from Audiomood import Conversion



def db_magnitude_stft (data, sampling_freq=16000, samples_per_frame=1024):
    """
    PRE: 'wavefile' is a string that correspond to a wav file.
    POS: returns the dB magnitude of the STFT spectrogram.
    """

    data = data / 32768.0
    #-------------------
    f,t,stft = scipy.signal.stft(data,
                                 fs=sampling_freq,
                                 nperseg=samples_per_frame,
                                 noverlap=samples_per_frame // 2,
                                 return_onesided=True)
    return 20 * np.log10(0.00001 + np.sqrt(np.square(np.real(stft)) +
                                           np.square(np.imag(stft))))

def db_magnitude_stft_8bits (audio_file, rows, columns, sampling_freq=16000,
                          samples_per_frame=1024, reduceMatrix=True):
    """
    PRE: 'audio_file' is a string that correspond to an mp3 file.
         'reduceMatrix' when True the resulting matrix is reduced 
         to the size (rows,columns) 
    POS: computes dB magnitude of the STFT spectrogram. The resulting 
         matrix is converted into 8 bits integers and if the number of 
         frames is less than 'columns' then grey columns of pixels are 
         contanated on the left and on the right in order to get a rows 
         x columns matrix.    
    """
    stft = db_magnitude_stft(audio_file, sampling_freq, samples_per_frame)
    highestInt=255
    min = np.min(stft)
    max = np.max(stft)
    low = 0
    high = highestInt
    alpha = (low - high) / (min - max)
    beta = low - alpha * min
    stftint = np.int8(np.round(alpha * stft + beta))
    stftint = -(stftint - 255)
    if reduceMatrix:
        h = stftint.shape[0]
        w = stftint.shape[1]
        if stftint.shape[0] > rows:
            h = rows
        if stftint.shape[1] > columns:
            w = columns
        stftint = stftint[0:h,0:w]
    if stftint.shape[1] < columns:
        colu = np.zeros((512,1), dtype=np.int8) + 127
        delta = 32 - stftint.shape[1]
        for j in range(int(np.floor(delta/2))):
            stftint = np.concatenate((colu,stftint),axis=1)
        for j in  range(int(np.ceil(delta/2))):
            stftint = np.concatenate((stftint,colu),axis=1)
    return np.int8(stftint)

def create_header(file_name, t="data", num_of_items=0, num_of_cols=0, num_of_lines=0):
    """
    Given the type of files, creates the header for the file
    """
    if t ==  "data":
        output_file = gzip.open(file_name,"wb")
        output_file.write(b'\x00')
        output_file.write(b'\x00')
        output_file.write(b'\x08')
        output_file.write(b'\x03')
        output_file.write((np.int32(num_of_items).byteswap()).tobytes())
        output_file.write((np.int32(num_of_lines).byteswap()).tobytes())
        output_file.write((np.int32(num_of_cols).byteswap()).tobytes())
        output_file.close()

    elif t ==  "labels":
        output_file = gzip.open(file_name,"wb")
        output_file.write(b'\x00')
        output_file.write(b'\x00')
        output_file.write(b'\x0D') # This identifies the type of data that will be put in the file. In this case a float.
        output_file.write(b'\x01') # And this the dimension of the labels. In this case a matrice nX2
        output_file.write((np.int32(num_of_items).byteswap()).tobytes())
        output_file.write((np.int32(2).byteswap()).tobytes())
        output_file.close()

def write_to_file(file_name, data):
    with gzip.open(file_name, 'wb') as f:
        f.write(data)

def getAllFoldersFromAudio(folderpath):
    return [i for i in os.listdir(folderpath) if os.path.isdir(folderpath + i)]

def getAlternativeList(filename):
    with open(filename, "r") as f:
        return f.read().splitlines()

def getLabelConversion(fullList, l):
    return fullList.index(l)

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.
    
    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0
    
    if mn != mx:
        alpha = (low - high)/(mn - mx)
    
    beta = low - alpha*mn
    return value * alpha + beta

def getLabel(filepath):
    """
    As the labels are in the file path, we need to retrieve the labels, so that we can 
        create a dataset with labels. 
    """
    return filepath.split("/")[-2]


def create_files(data_file_path, label_file_path, file_list, num_of_cols, num_of_lines, folders, clip_duration=1, sampling_freq=16000, sample_per_frame=1024):
    
    # Data File

    create_header(data_file_path,  t="data", num_of_items=len(file_list), num_of_cols=num_of_cols, num_of_lines=num_of_lines)
    create_header(label_file_path, t="labels", num_of_items=len(file_list))
    track_num = 0
    tracks = len(file_list)
    total = len(file_list):
    for n,i in enumerate(file_list):
        perc = (n/total)*100
        print("{}% Done.".format(str(perc)))
        audio, samp_rate = librosa.core.load(i, sr=16000, duration=clip_duration) 

        stft = db_magnitude_stft_8bits(audio, num_of_lines, num_of_cols, sampling_freq, sample_per_frame)
        d1 = stft.shape[0] # 512
        d2 = stft.shape[1] # 32
        for k in range(d1-1,-1,-1):
            for j in range(d2):
                f = gzip.open(data_file_path, 'wb')
                f.write(stft[k][j].tobytes())
                f.close()

        f = gzip.open(label_file_path, 'wb')
        f.write((np.int32(getLabelConversion(folders, getLabel(i))).byteswap()).tobytes())    
        f.close()
                

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    base_folder ="/Users/itsector/Documents/tfsrc/"

    training_data_name = base_folder + "tfsrc_train_data-idx3.ubyte.gz"
    training_labels_name = base_folder + "tfsrc_train_labels-idx3.ubyte.gz"

    testing_data_name = base_folder + "tfsrc_test_data-idx3.ubyte.gz"
    testing_labels_name = base_folder + "tfsrc_test_labels-idx3.ubyte.gz"

    val_data_name = base_folder + "tfsrc_val_data-idx3.ubyte.gz"
    val_labels_name = base_folder + "tfsrc_val_labels-idx3.ubyte.gz"

    folders = getAllFoldersFromAudio(base_folder)

    usedFolders = [base_folder + i+"/" for i in folders if os.path.isdir(base_folder + i)]
    testingList = [i.split("/") for i in getAlternativeList(base_folder + "testing_list.txt")]

    valList = [i.split("/") for i in getAlternativeList(base_folder + "validation_list.txt")]
    
    testingList = [base_folder + i[0] + "/" + i[1] for i in testingList if i[0] in folders]

    valList = [base_folder + i[0] + "/" + i[1] for i in valList if i[0] in folders]


    full = [os.listdir(i) for i in usedFolders if os.path.isdir(i)] # For each folder, will create a list, with all of the audio tracks in it.
    result = OrderedDict(zip(usedFolders, full))

    train = []
    for k,v in result.items():
        for i in v:
            if k+i not in testingList and  k+i not in valList:
                train.append(k+i)
    random.shuffle(train)
    create_files(training_data_name, training_labels_name, train, 32, 512, folders)

    random.shuffle(valList)
    create_files(val_data_name, val_labels_name, valList, 44, 128, folders)

    random.shuffle(testingList)
    create_files(testing_data_name, testing_labels_name, testingList, 44, 128, folders)

    print("Finished.")
    end_time = datetime.datetime.now()
    delta = end_time - start_time
    print("Used time: {}".format(delta))