import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np

#
# Script to join the annotation from arousal and valence
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_per_each_rater/dynamic/'
    arousal = base_folder + 'arousal/'
    valence = base_folder + 'valence/'
    full_csv = 'full.csv'

    write = []

    songs = list(os.listdir(arousal))
    with open(full_csv, mode='w') as employee_file:
        writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        header = ['Song', 'Person']
        for i in range(15000, 45000, 500):
            header.append(str(i) +'_A')
            header.append(str(i) +'_V')
        writer.writerow(header)

        for i in songs:
            insert = []
            name = i.split('.')[0]
            arousal_reader = csv.reader(open(arousal + i, 'r'), delimiter=',')
            valence_reader = csv.reader(open(valence + i, 'r'), delimiter=',')
            header_a = next(arousal_reader)
            header_v = next(valence_reader)
            a = [j for j in arousal_reader]
            v = [j for j in valence_reader]
            join = list(zip(a, v))



            # for i in range(len(arousal_reader)):
            #     a = zip(i, valence_reader[i])
            #     line.append(a)
            for i, line in enumerate(join):
                o = [name,i]
                if header_a[0] == "WorkerId":
                    for i in list(zip(line[0][1:], line[1][1:])):
                        o.append(i[0])
                        o.append(i[1])
                    writer.writerow(o)
                else:
                    for i in list(zip(line[0], line[1])):
                        o.append(i[0])
                        o.append(i[1])
                    writer.writerow(o)
            

    # with open(full_csv, w) as writer:
