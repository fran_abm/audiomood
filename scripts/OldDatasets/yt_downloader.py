# !/bin/usr/env python3

import subprocess
import os
import youtube_dl

# This script downloads a youtube video, and transforms it to audio.
# Uses youtube-dl to download, and ffmpeg to extract the audio.


def create_output_string(folder, name):
    """[summary]

    Arguments:
        folder_name {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    return "{folder}{name}_%(id)s.%(ext)s".format(folder = folder, name=name)

def my_hook(d):
    """[summary]

    Arguments:
        d {[type]} -- [description]
    """
    if d['status'] == 'finished':
        print('Done {} downloading, now converting ...'.format(d['filename']))

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def download(video_link, audio_output = True, output_folder = None, output_name = None, output_filetype = None):
    """Downloads a video given a link.

    Arguments:
        video_link {str} -- The link for a video, or playlist, on youtube.

    Keyword Arguments:
        audio_output {bool} -- If true, downloads the video as an audio file. (default: {True})
        output_folder {str} -- Sets the output folder for the downloaded video. (default: {None})
        output_filetype {str} -- Sets the output filetype for the downloaded video. (default: {None})
    """

    ydl_opts = {
        'outtmpl':create_output_string(output_folder, output_name),
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        'ignoreerrors': True,
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([video_link])
        result = ydl.extract_info("{}".format(video_link))
        return ydl.prepare_filename(result)

if __name__ == "__main__":
    link = 'https://www.youtube.com/watch?v=A1AJEv50Ld4'
    print(download(link, output_folder = './', output_filetype = "wav"))




