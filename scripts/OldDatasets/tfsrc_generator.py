import numpy as np
import scipy
from scipy import io
from scipy.io import wavfile
from scipy import signal
import gzip
import wave
import struct
import os
import sys

def getLabelConversion(fullList, l):
    return fullList.index(l)

def getAllFoldersFromAudio(folderpath):
    return [i for i in os.listdir(folderpath) if os.path.isdir(folderpath + i)]

def dirInventory(base_folder):
    ret = []
    for i in os.listdir(base_folder):
        if os.path.isdir(base_folder + i):
            ret.extend(dirInventory(base_folder + i + "/"))
        else:
            ret.append(base_folder + i)
    return ret

def readListOfFiles(filename, flag,folder=None):
    """
    PRE: 'filename' is a file that contains a sequence of file names (one per line).
    POS: Returns a list of strings that correspond to the file names contained in 'filename'.
    """
    if flag and folder != None:
        return [folder + i.strip() for i in open(filename, "r").readlines()]
    else:
        return [i.strip() for i in open(filename, "r").readlines()]

def difDirs(all, *args):
    left = []
    n = []
    for i in args:
        n.extend(i)
    for i in all:
        if i not in n:
            left.append(i)
    return left

#======================================================================

def dBMagnitudeStft (wavfile,samplingFreq=16000,samplePerFrame=1024):
    """
    PRE: 'wavefile' is a string that correspond to a wav file.
 POS: returns the dB magnitude of the STFT spectrogram.
    """
    rate, data = scipy.io.wavfile.read(wavfile)
    data = data / 32768.0
    #-------------------
    f,t,stft = scipy.signal.stft(data,
                                 fs=samplingFreq,
                                 nperseg=samplePerFrame,
                                 noverlap=samplePerFrame // 2,
                                 return_onesided=True)
    return 20 * np.log10(0.00001 + np.sqrt(np.square(np.real(stft)) +
                                           np.square(np.imag(stft))))

def dBMagnitudeStft8bits (wavfile, rows, columns, samplingFreq=16000,
                          samplePerFrame=1024, reduceMatrix=True):
    """
    PRE: 'wavefile' is a string that correspond to a wav file.
         'reduceMatrix' when True the resulting matrix is reduced 
         to the size (rows,columns) 
    POS: computes dB magnitude of the STFT spectrogram. The resulting 
         matrix is converted into 8 bits integers and if the number of 
         frames is less than 'columns' then grey columns of pixels are 
         contanated on the left and on the right in order to get a rows 
         x columns matrix.    
    """
    stft = dBMagnitudeStft(wavfile, samplingFreq, samplePerFrame)
    highestInt=255
    min = np.min(stft)
    max = np.max(stft)
    low = 0
    high = highestInt
    alpha = (low - high) / (min - max)
    beta = low - alpha * min
    stftint = np.int8(np.round(alpha * stft + beta))
    stftint = -(stftint - 255)
    if reduceMatrix:
        h = stftint.shape[0]
        w = stftint.shape[1]
        if stftint.shape[0] > rows:
            h = rows
        if stftint.shape[1] > columns:
            w = columns
        stftint = stftint[0:h,0:w]
    if stftint.shape[1] < columns:
        colu = np.zeros((512,1),dtype=np.int8) + 127
        delta = columns - stftint.shape[1]
        for j in range(int(np.floor(delta/2))):
            stftint = np.concatenate((colu,stftint),axis=1)
        for j in  range(int(np.ceil(delta/2))):
            stftint = np.concatenate((stftint,colu),axis=1)
    return np.int8(stftint)

def writedBMagnitudeStft8bits(outputFile, wavfile, 
                              rows, columns,
                              samplingFreq=16000,
                              samplePerFrame=1024):
    """
    Function used by 'writeStft8bitsFiles'
    PRE: 'outputFile' is an open stream to a gziped ubyte file. 
    POS: the sequence of bytes that correspond to 'wavfile' STFT 
         spectrogram is written to the ubyte file.
    """
    stft = dBMagnitudeStft8bits(wavfile, rows, columns, samplingFreq, samplePerFrame)
    d1 = stft.shape[0] # 512
    d2 = stft.shape[1] # 32
    for i in range(d1-1,-1,-1):
        for j in range(d2):
            outputFile.write(stft[i][j].tobytes())

def writeStft8bitsFiles(wavfiles, outputDataName, outputLabelName, folders,height, width, samplingFreq=16000, samplePerFrame=1024):
    """
    PRE: 'wavfiles' is a list of file names (string) corresponding to wav files.
         'outputFilename' is the name of the ubyte file to be created. 
         It must end with '.ubyte.gz'
         'height' correspond to the number of rows of the spectrogram 
         (should be samplePerFrame/2)
         'width' is the max number of frames (× 2 because we use 50% overlap) 
         among wav files. Files that are shorter will be centered on a grey background.  
    POS: A new ubyte file is created !
    """
    # outputFile = gzip.open(outputDataName,"wb")
    # outputFile.write(b'\x00')
    # outputFile.write(b'\x00')
    # outputFile.write(b'\x08')
    # outputFile.write(b'\x03')
    # outputFile.write((np.int32(len(wavfiles)).byteswap()).tobytes())
    # outputFile.write((np.int32(height).byteswap()).tobytes())
    # outputFile.write((np.int32(width).byteswap()).tobytes())

    # for k, wavfile in enumerate(wavfiles):
    #     sys.stdout.write("Done: %f%%   \r" % ((k/len(wavfiles))*100))
    #     sys.stdout.flush()
    #     writedBMagnitudeStft8bits(outputFile,wavfile,
    #                               height, width,
    #                               samplingFreq,
    #                               samplePerFrame)
    # outputFile.close()

    outputFile = gzip.open(outputLabelName,"wb")
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(wavfiles)).byteswap()).tobytes())


    print(len(wavfiles))
    rest = 0

    for k, i in enumerate(wavfiles):
        # sys.stdout.write("Done: %f%%   \r" % ((k/len(wavfiles))*100))
        # sys.stdout.flush()
        folder = i.split("/")[-2]
        i = getLabelConversion(folders, folder)
        # print(i)
        # print(bytes([i]))
        # print(int.from_bytes(bytes([i]), "big"))
        # print("\n")
        outputFile.write(bytes([i]))
        rest += 1

    print(rest)
    
    outputFile.close()


if __name__ == '__main__':
    base_folder = "/Users/itsector/Documents/tfsrc/"
    folders = getAllFoldersFromAudio(base_folder)
    val_list = readListOfFiles('/Users/itsector/Documents/tfsrc/validation_list.txt', True, folder='/Users/itsector/Documents/tfsrc/')
    test_list = readListOfFiles('/Users/itsector/Documents/tfsrc/testing_list.txt', True, folder='/Users/itsector/Documents/tfsrc/')
    train_list = readListOfFiles('/Users/itsector/Documents/tfsrc/training_list.txt', False)
    
    writeStft8bitsFiles(train_list, "tfsrc-train-data-ubyte.gz", "tfsrc-train-label-ubyte.gz", folders, 512, 32)
    # writeStft8bitsFiles(val_list, "tfsrc-val-data-ubyte.gz", "tfsrc-val-label-ubyte.gz", folders, 512, 32)
    # writeStft8bitsFiles(test_list, "tfsrc-test-data-ubyte.gz", "tfsrc-test-label-ubyte.gz", folders, 512, 32)