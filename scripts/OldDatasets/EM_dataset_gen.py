
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import random
import numpy as np
import datetime

from collections import defaultdict
import itertools
import csv
import time

import scipy
from scipy import io
from scipy.io import wavfile
from scipy import signal
import gzip
import wave
import struct
import librosa



def db_magnitude_stft (data, sampling_freq=16000, samples_per_frame=1024):
    """
    PRE: 'wavefile' is a string that correspond to a wav file.
    POS: returns the dB magnitude of the STFT spectrogram.
    """

    data = data / 32768.0
    #-------------------
    f,t,stft = scipy.signal.stft(data,
                                 fs=sampling_freq,
                                 nperseg=samples_per_frame,
                                 noverlap=samples_per_frame // 2,
                                 return_onesided=True)
    return 20 * np.log10(0.00001 + np.sqrt(np.square(np.real(stft)) +
                                           np.square(np.imag(stft))))

def db_magnitude_stft_8bits (audio_file, rows, columns, sampling_freq=16000,
                          samples_per_frame=1024, reduceMatrix=True):
    """
    PRE: 'audio_file' is a string that correspond to an mp3 file.
         'reduceMatrix' when True the resulting matrix is reduced
         to the size (rows,columns)
    POS: computes dB magnitude of the STFT spectrogram. The resulting
         matrix is converted into 8 bits integers and if the number of
         frames is less than 'columns' then grey columns of pixels are
         contanated on the left and on the right in order to get a rows
         x columns matrix.
    """
    stft = db_magnitude_stft(audio_file, sampling_freq, samples_per_frame)
    # print('Shape: ', str(stft.shape))
    highestInt=255
    min = np.min(stft)
    max = np.max(stft)
    low = 0
    high = highestInt
    alpha = (low - high) / (min - max)
    beta = low - alpha * min
    stftint = np.int8(np.round(alpha * stft + beta))
    stftint = -(stftint - 255)
    if reduceMatrix:
        h = stftint.shape[0]
        w = stftint.shape[1]
        if stftint.shape[0] > rows:
            h = rows
        if stftint.shape[1] > columns:
            w = columns
        stftint = stftint[0:h,0:w]
    if stftint.shape[1] < columns:
        colu = np.zeros((512,1),dtype=np.int8) + 127
        delta = 32 - stftint.shape[1]
        for j in range(int(np.floor(delta/2))):
            stftint = np.concatenate((colu,stftint),axis=1)
        for j in  range(int(np.ceil(delta/2))):
            stftint = np.concatenate((stftint,colu),axis=1)
    return np.int8(stftint)

def create_annotations(arousal_csv, valence_csv):
    print("Starting Annoation Creation")

    arousal = open(arousal_csv, 'r')
    arousal_reader = csv.reader(arousal)
    next(arousal_reader)

    valence = open(valence_csv, 'r')
    valence_reader = csv.reader(valence)
    next(valence_reader)

    dic = {}
    for arousal, valence in itertools.zip_longest(arousal_reader, valence_reader):
        dic[arousal[0]] = list(zip(arousal[1:], valence[1:]))

    return dic

def progressive_data_save(file_name, folder_path, audio_height=512, audio_width=32, sampling_freq=16000, sample_per_frame=1024, first_sample=15, last_sample=45, clip_duration=1):
    print('Folder Path: ', folder_path)
    output_file = gzip.open(file_name,"wb")
    for (i, v) in enumerate(list(os.listdir(folder_path))[:100]):
        print('File: ', v)
        print('Number: ', i)
        for j in np.arange(15, 45, 0.5):
            print('Starting Position: ', j)
            audio, samp_rate = librosa.core.load(folder_path + v, offset=j, sr=16000,duration=clip_duration)
            write_db_magnitude_srft_8bits(output_file, audio, audio_height, audio_width, sampling_freq, sample_per_frame)

    output_file.close()

def create_header(file_name, t="data", num_of_items=0, num_of_cols=0, num_of_lines=0):
    """
    Given the type of files, creates the header for the file
    """
    if t ==  "data":
        output_file = gzip.open(file_name,"wb")
        output_file.write(b'\x00')
        output_file.write(b'\x00')
        output_file.write(b'\x08')
        output_file.write(b'\x03')
        output_file.write((np.int32(num_of_items).byteswap()).tobytes())
        output_file.write((np.int32(num_of_lines).byteswap()).tobytes())
        output_file.write((np.int32(num_of_cols).byteswap()).tobytes())
        output_file.close()

    elif t ==  "labels":
        output_file = gzip.open(file_name,"wb")
        output_file.write(b'\x00')
        output_file.write(b'\x00')
        output_file.write(b'\x08') # This identifies the type of data that will be put in the file. In this case a float.
        output_file.write(b'\x01') # And this the dimension of the labels. In this case a matrice nX2
        output_file.write((np.int32(num_of_items).byteswap()).tobytes())
        output_file.write((np.int32(2).byteswap()).tobytes())
        output_file.close()

def write_to_file(file_name, data):
    with gzip.open(file_name, 'wb') as f:
        f.write(data)

def dataset_division(folder_path, seed):
    """
    As the dataset creators didn't give a specific division of the dataset between the 3 types, this function
        will do a 60-20-20 split between training, validation and testing, and shuffles the audio tracks.
    """
    files = [i for i in os.listdir(folder_path)]
    random.seed(seed)
    random.shuffle(files)
    train = files[:int(len(files) * 0.6)]
    val = files[int(len(files)*0.6) : int(len(files)*0.8)]
    test = files[int(len(files)*0.8) :]
    return train, val, test

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.

    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0

    if mn != mx:
        alpha = (low - high)/(mn - mx)

    beta = low - alpha*mn
    return value * alpha + beta


def convert_anotations(ano):
    aro_min = 10000
    aro_max = -10000
    val_min = 10000
    val_max = -10000

    for k,v in ano.items():
        l = []
        for i in v:
            j = np.array(i).astype(float)
            if j[0] > aro_max:
                aro_max = j[0]
            if j[0] < aro_min:
                aro_min = j[0]
            if j[1] > val_max:
                val_max = j[1]
            if j[1] < val_min:
                val_min = j[1]
            l.append(j)
        ano[k] = l

    for k,v in ano.items():
        l = []
        for i in v:
            aro = int(rectify_values(0, 255, aro_min, aro_max, i[0]))
            val = int(rectify_values(0, 255, val_min, val_max, i[1]))
            l.append(np.array([aro, val]).astype(int))
        ano[k] = l
    return ano

def create_files(data_file_path, label_file_path, audio_path, file_list, anotation_dic, num_of_cols, num_of_lines, clip_duration=1, sampling_freq=16000, sample_per_frame=1024):

    num_of_tracks = 0
    for i in file_list:
        if i.split(".")[0] in anotation_dic.keys():
            num_of_tracks += 1

    print("Number of tracks: {}".format(num_of_tracks))

    # Data File

    create_header(data_file_path,  t="data", num_of_items=num_of_tracks, num_of_cols=num_of_cols, num_of_lines=num_of_lines)
    create_header(label_file_path, t="labels", num_of_items=num_of_tracks)
    track_num = 0
    tracks = len(file_list)
    for j in file_list:
        wav = j
        track_num += 1
        percentage = (track_num * 100) / tracks
        sys.stdout.write("Done: %f%%   \r" % (percentage))
        sys.stdout.flush()
        name = j.split(".")[0]
        if name in anotation_dic.keys():

            for k,v in enumerate(np.arange(14, 44, 0.5)):
                av_values = np.array(anotation_dic[name][k]).astype(float)

                # audio, samp_rate = librosa.core.load(audio_path + wav, offset=v, sr=16000,duration=clip_duration)
                # stft = db_magnitude_stft_8bits(audio, num_of_lines, num_of_cols, sampling_freq, sample_per_frame)
                # d1 = stft.shape[0] # 512
                # d2 = stft.shape[1] # 32
                # for i in range(d1-1,-1,-1):
                #     for j in range(d2):
                #         f = gzip.open(data_file_path, 'wb')
                #         f.write(stft[i][j].tobytes())
                #         f.close()

                f = gzip.open(label_file_path, 'wb')
                f.write(av_values[0])
                f.write(av_values[1])
                f.close()




if __name__ == "__main__":
    start_time = datetime.datetime.now()
    base_folder = '/home/quietswami/Documents/EMOMUSIC/'
    static = base_folder + 'static_annotations.csv'
    arousal_csv = base_folder + 'arousal_cont_average.csv'
    valence_csv = base_folder + 'valence_cont_average.csv'
    audio_path = base_folder + 'WAV/'

    training_data_name = base_folder + "emomusic_train_data-idx3-ubyte.gz"
    training_labels_name = base_folder + "emomusic_train_labels-idx1-ubyte.gz"

    testing_data_name = base_folder + "emomusic_test_data-idx3-ubyte.gz"
    testing_labels_name = base_folder + "emomusic_test_labels-idx1-ubyte.gz"

    val_data_name = base_folder + "emomusic_val_data-idx3-ubyte.gz"
    val_labels_name = base_folder + "emomusic_val_labels-idx1-ubyte.gz"

    seed = 11

    # Anotation Cration
    a = create_annotations(arousal_csv, valence_csv)
    # a = convert_anotations(a)

    # Dataset split
    train_list, val_list, test_list = dataset_division(audio_path, seed)

    #
    # Creation of dataset
    #
    create_files(training_data_name, training_labels_name, audio_path, train_list, a, 33, 513)
    create_files(val_data_name, val_labels_name, audio_path, val_list, a, 33, 513)
    create_files(testing_data_name, testing_labels_name, audio_path, test_list, a, 33, 513)



