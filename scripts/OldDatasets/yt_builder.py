import audio_conversion
import datetime
import os
import random
import csv
import itertools
import numpy as np
import gzip
import sys
import yt_downloader
from pathlib import Path
import stftFeatures




def download_songs(d):
    """This function downloads and creates the folder structure for the dataset.
    Checks if the folder exists, if not creates it.

    Arguments:
        d -- A dict with very specific rules.
            {"folder_path": "file_path"} - where folder path is where the audio will be stored, and file path, the path of
                the text file with the links for the songs.
    """
    for k,v in d.items():
        if not os.path.isdir(k):
            print("{} does not exist. Creating...".format(k))
            os.mkdir(k)
        name = k.split("/")[-2]
        for i in open('YT_Datasets/' + v, 'r').readlines():
            yt_downloader.download(i.strip(), audio_output = True,  output_folder = k, output_name = name, output_filetype = "wav")

def downsampling(d)
    for k,v in d.items():
        for i in os.listdir(k):
            if i.endswith('.wav'):
                downsample, sr = librosa.load(k + i, sr=16000)
                librosa.output.write_wav(k+i, downsample, sr)

def create1sWAV(folderpath):
    for i in os.listdir(folderpath):
        path = folderpath + i
        newPath = path.split(".")[0]
        time = float(audio_conversion.get_audio_duration(path))
        print(time)
        for j in np.arange(0.0, time - 0.5, 0.5):
            print(path + str(j) + ".wav")
            data, samp = audio_conversion.read_audio_to_wav(path, samp=16000, offset=j, duration=1)
            audio_conversion.save_wav(path + str(j) + ".wav", data, samp)
        os.unlink(path)

def create_dataset(data, labels, dataname, labelname, height, width):
    stftFeatures.writeStft8bitsFiles(data, dataname, height, width)

    # Label Creation
    outputFile = gzip.open(labelname,"wb")
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(labels)).byteswap()).tobytes())
    for i in labels:
        print(i)
        outputFile.write(int(i).to_bytes(1, "big"))
    outputFile.close()

def createPreDataset(happy, sad):
    labels = []
    data = []
    for i in os.listdir(happy):
        data.append(happy + i)
        labels.append(0)

    for i in os.listdir(sad):
        data.append(sad + i)
        labels.append(1)

    return data, labels


def deleteFiles(folder_path):
    for i in os.listdir(folder_path):
        os.unlink(folder_path + i)

if __name__ == "__main__":

    debug_flag = True
    path = str(Path.home())
    start_time = datetime.datetime.now()

    #
    # Folder and dataset naming
    #

    base_folder = '{}/Documents/'.format(path)


    wav_folder = base_folder + "WAV/"
    if not os.path.isdir(wav_folder):
        os.mkdir(wav_folder)

    happy_train = wav_folder + "happy_train/"
    sad_train = wav_folder + "sad_train/"
    # scream_train = wav_folder + "scream_train/"

    happy_test = wav_folder + "happy_test/"
    sad_test = wav_folder + "sad_test/"
    # scream_train = wav_folder + "scream_test/"

    happy_val = wav_folder + "happy_val/"
    sad_val = wav_folder + "sad_val/"
    # scream_train = wav_folder + "scream_test/"

    training_data_name = base_folder + "yt_happy_sad_v3_train_data-ubyte.gz"
    training_labels_name = base_folder + "yt_happy_sad_v3_train_labels-ubyte.gz"

    testing_data_name = base_folder + "yt_happy_sad_v3_test_data-ubyte.gz"
    testing_labels_name = base_folder + "yt_happy_sad_v3_test_labels-ubyte.gz"

    val_data_name = base_folder + "yt_happy_sad_v3_val_data-ubyte.gz"
    val_labels_name = base_folder + "yt_happy_sad_v3_val_labels-ubyte.gz"


    d = {
        happy_train: 'happy_songs_train.txt',
        sad_train: 'sad_songs_train.txt',
        # scream_train: 'scream_train.txt',
        happy_test: 'happy_songs_test.txt',
        sad_test: 'sad_songs_test.txt',
        # scream_test: 'scream_test.txt',
        happy_val: 'happy_songs_val.txt',
        sad_val: 'sad_songs_val.txt',
        # scream_val: 'scream_val.txt'
    }

    if len(os.listdir(happy_train)) > 0:
        deleteFiles(happy_train)
    if len(os.listdir(sad_train)) > 0:
        deleteFiles(sad_train)

    if len(os.listdir(happy_test)) > 0:
        deleteFiles(happy_test)
    if len(os.listdir(sad_test)) > 0:
        deleteFiles(sad_test)

    if len(os.listdir(happy_val)) > 0:
        deleteFiles(happy_val)
    if len(os.listdir(sad_val)) > 0:
        deleteFiles(sad_val)

    download_songs(d)
    downsample(d)


    create1sWAV(happy_train)
    create1sWAV(sad_train)
    create1sWAV(happy_test)
    create1sWAV(sad_test)
    create1sWAV(happy_val)
    create1sWAV(sad_val)

    train_data, train_label = createPreDataset(happy_train, sad_train)
    test_data, test_label = createPreDataset(happy_test, sad_test)
    val_data, val_label = createPreDataset(happy_val, sad_val)

    if len(train_data) == len(train_label):
        print('Training data all good')

    if len(test_data) == len(test_label):
        print('Testing data all good')

    if len(val_data) == len(val_label):
        print('Val data all good')


    create_dataset(train_data, train_label, training_data_name, training_labels_name, 512, 32)
    create_dataset(test_data, test_label, testing_data_name, testing_labels_name, 512, 32)
    create_dataset(test_data, test_label, val_data_name, val_labels_name, 512, 32)

    end_time = datetime.datetime.now()
    elapsed_time = end_time - start_time
    print("Elapsed Time: {}".format(elapsed_time))