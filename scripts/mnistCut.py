#
# Script to create custom datasts from the MNIST Dataset.
#

import sys, os
sys.path.insert(0, os.path.abspath('..'))

from ubyte import Ubyte as ub
from ubyte import Header as hd

def loadDataset(data, label):
    """
    Loads the dataset that will be cut.
    """
    d = ub.Ubyte(True, "data")
    d.parseFile(data, "data")
    l = ub.Ubyte(True, "labels")
    l.parseFile(label, "labels")
    return d, l

if __name__ == "__main__":
    d, l = loadDataset('../datasets/train-images-idx3-ubyte', '../datasets/train-labels-idx1-ubyte')
    images = d.getData()
    windowSize = 28*28          
    im = [images[i:i+784] for i in range(0, len(images), 784)]
    labels = l.getData()
    newLabels = []
    newData = []
    cleanedData = []
    for i in range(len(labels)):
        if int(labels[i]) < 5:
            newLabels.append(labels[i])
            newData.append(im[i])

    for i in range(len(newData)):
        for j in range(len(newData[i])):
            cleanedData.append(newData[i][j])
    print(len(cleanedData))
    o = ub.Ubyte(False, "data")
    u = ub.Ubyte(False, "labels")
    o.setData(cleanedData)
    u.setData(newLabels)
    oH = hd.Header(False)
    oH.setNumOfCols(28)
    oH.setNumOfItems(len(newData))
    oH.setNumOfRows(28)
    oH.setMagic(2051)
    o.setHeader(oH)
    uH = hd.Header(False, "labels")
    uH.setNumOfItems(len(newLabels))
    uH.setMagic(2049)
    u.setHeader(uH)

    o.saveFile("SMALLMNIST04-data-idx3-ubyte")
    u.saveFile("SMALLMNIST04-labels-idx1-ubyte")

