import csv
import os
import matplotlib.pyplot as plt
import numpy as np
import random

#
# Script to show the average emotion per music
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/EMOMUSIC/'
    arousal_csv = base_folder + 'arousal_cont_average.csv'
    valence_csv = base_folder + 'valence_cont_average.csv'

    arousal = {}
    valence = {}
    song_dic = {}

    with open(arousal_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            arousal[i[0]] = [j for j in i[1:]]

    with open(valence_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            valence[i[0]] = [j for j in i[1:]]
    
    for i in arousal.keys():
        song_dic[i] = list(zip(arousal[i], valence[i]))

    keys = list(song_dic.keys())
    # random.shuffle(keys)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    fig.suptitle('Musicas 10 a 20')
    for i in keys[10:20]:
        print(i)
        data = np.array(song_dic[i]).astype(float)
        ax.plot(data[:, 0], data[:, 1], label='Musica: ' + str(i))

    ax.set_xlabel('Arousal')
    ax.set_ylabel('Valence')
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    ax.legend()

    plt.show()