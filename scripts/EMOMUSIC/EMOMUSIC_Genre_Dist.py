import csv
import os
import matplotlib.pyplot as plt
import numpy as np
import random


if __name__ == "__main__":
    
    file = '/home/quietswami/Documents/EMOMUSIC/songs_info.csv'

    numb = {}

    with open(file, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)
        for i in reader:
            if i[6] not in numb.keys():
                numb[i[6]] = 1
            else:
                numb[i[6]] += 1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    fig.suptitle('Distribuição dos Géneros')
    numeros = []
    generos = []
    for i in numb:
        numeros.append(numb[i])
        generos.append(i.strip())
        print(i)
    
    y_pos = np.arange(len(generos))
    plt.bar(y_pos, numeros, align='edge', width=0.7)
    plt.xticks(y_pos, generos, rotation=20)
    # plt.set_xlabel('Géneros')
    # plt.set_ylabel('Numero de Músicas')

    plt.show()
