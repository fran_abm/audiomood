import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to display the averaged VA per song.
#

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.
    
    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0
    
    if mn != mx:
        alpha = (low - high)/(mn - mx)
    
    beta = low - alpha*mn
    return value * alpha + beta


if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/EMOMUSIC/'
    static = base_folder + 'static_annotations.csv'

    song_dic = {}

    with open(static,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:

            song_dic[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[3], 'arousal_std': i[4]}

    
    dataset = []
    max_val = 0
    min_val = 1000
    max_ar = 0
    min_ar = 1000
    for k, v in song_dic.items():
        val = float(v['valence_mean'])
        aro = float(v['arousal_mean'])
        if val > max_val:
            max_val = val
        elif val < min_val:
            min_val = val
        
        if aro > max_ar:
            max_ar = aro
        elif aro < min_ar:
            min_ar = aro
        dataset.append([v['valence_mean'], v['arousal_mean']])


    new_y_line = rectify_values(-1,1, min_val, max_val, float((min_val + max_val)/2))
    new_x_line = rectify_values(-1,1, min_ar, max_ar,float((min_ar + max_ar)/2))

    print(new_x_line)
    print(new_y_line)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    fig.suptitle('Valores totais do VA')

    for i in dataset:
        data = np.array(i).astype(float)
        x = rectify_values(-1, 1, min_ar, max_ar, data[0])
        y = rectify_values(-1, 1, min_val, max_val, data[1])
        if x > new_x_line and y > new_y_line:
            ax.scatter(x, y, color='red')
        elif x > new_x_line and y < new_y_line:
            ax.scatter(x, y, color='green')
        elif x < new_x_line and y > new_y_line:
            ax.scatter(x, y, color='blue')
        elif x < new_x_line and y < new_y_line:
            ax.scatter(x, y, color='yellow')
        elif x == new_x_line and y > new_y_line:
            ax.scatter(x, y, color='purple')
        elif x == new_x_line and y < new_y_line:
            ax.scatter(x, y, color='cyan')
        elif x < 4.5 and y == 4.5:
            ax.scatter(x, y, color='limegreen')
        elif x > 4.5 and y == 4.5:
            ax.scatter(x, y, color='chocolate')  
    
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_xlabel('Arousal')
    ax.set_ylabel('Valence')
    ax.axhline(y=new_y_line, color='k')
    ax.axvline(x=new_x_line, color='k')
    ax.legend()
    plt.show()
    

