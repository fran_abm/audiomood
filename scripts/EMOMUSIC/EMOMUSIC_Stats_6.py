import csv
import os
import matplotlib.pyplot as plt
import numpy as np
import random

#
def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.
    
    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0
    
    if mn != mx:
        alpha = (low - high)/(mn - mx)
    
    beta = low - alpha*mn
    return value * alpha + beta


if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/EMOMUSIC/'
    static = base_folder + 'static_annotations.csv'
    arousal_csv = base_folder + 'arousal_cont_average.csv'
    valence_csv = base_folder + 'valence_cont_average.csv'

    arousal = {}
    valence = {}
    song_dic = {}

    with open(arousal_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            arousal[i[0]] = [j for j in i[1:]]

    with open(valence_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            valence[i[0]] = [j for j in i[1:]]
    
    for i in arousal.keys():
        song_dic[i] = list(zip(arousal[i], valence[i]))

    song_dic_1 = {}
    min_ar = 1000
    max_ar = 0
    min_val = 1000
    max_val = 0
    with open(static,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:
            val = float(i[1])
            ar = float(i[3])
            if val > max_val:
                max_val = val
            elif val < min_val:
                min_val = val
            
            if ar > max_ar:
                max_ar = ar
            elif ar < min_ar:
                min_ar = ar
            song_dic_1[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[3], 'arousal_std': i[4]}



    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    fig.suptitle('Valores totais do VA')

    colors = ['red', 'blue', 'green', 'yellow', 'pink', 'orange', 'gray', 'black', 'cyan', 'magenta']

    for i in list(song_dic.keys())[10:20]:
        color = colors.pop()
        print(color)
        j = [song_dic_1[i]['valence_mean'], song_dic_1[i]['arousal_mean']]
        data = np.array(j).astype(float)
        x = rectify_values(-1, 1, min_ar, max_ar, data[0])
        y = rectify_values(-1, 1, min_val, max_val, data[1])
        ax.scatter(x, y, color=color, marker='v')
        data = np.array(song_dic[i]).astype(float)
        ax.scatter(np.mean(data[:, 0]), np.mean(data[:, 1]), color=color, label='Musica: ' + str(i))

    
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_xlabel('Arousal')
    ax.set_ylabel('Valence')
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    ax.legend()
    plt.show()