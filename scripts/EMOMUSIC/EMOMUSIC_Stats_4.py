import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to show the average emotion per person
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_averaged_per_song/dynamic/'
    arousal = base_folder + 'arousal.csv'
    valence = base_folder + 'valence.csv'

    combine = {}
    song = ''
    a = {}
    b={}
    with open(arousal, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        r = next(reader)
        for k in reader:
            a[k[0]]  = [j for j in k[1:]]


    with open(valence, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        r = next(reader)
        for k in reader:
            b[k[0]] = [j for j in k[1:]]


    print(a['2'])
    for j in a:
        combine[j] = list(zip(a[j],b[j]))
    print(combine['2'])
    # for i in combine[list(combine.keys())[0]]:
    song_dic = {}

    # dataset = []
    # for k, v in song_dic.items():
    #     dataset.append([v['valence_mean'], v['arousal_mean']])

    # data = np.array(dataset)
    # plt.scatter(data[:, 0], data[:, 1])
    # plt.show()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    for i in list(combine.keys())[:10]:
        data = np.array(combine[i]).astype(float)
        ax.plot(data[:, 0], data[:, 1])

    # print(list(combine.keys())[0])
    # print(song_dic[list(combine.keys())[0]])
    # ax.scatter(song_dic[list(combine.keys())[0]]['valence_mean'], song_dic[list(combine.keys())[0]]['arousal_mean'])
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    plt.show()