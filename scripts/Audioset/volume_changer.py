import os
from pathlib import Path
from random import sample
import sys, os, shutil
sys.path.insert(0, os.path.abspath('..'))
from DatasetTools import audioTools, datasetTools, audioModule, stftFeatures
import numpy as np
import subprocess

import scipy
from scipy import io
from scipy.io import wavfile
from scipy import signal

volume = str(Path.home()) + '/Documents/Volume-Test/'

volume_stats = {}

samplePerFrame = 1024
samplingFreq = 16000

classes = ["Screaming", "Yell", "Run", "Speech", "Singing"]


def stft_stats(path):
    """
    Gets STFT stats for a music.
    """
    stft = stftFeatures.dBMagnitudeStft(path, samplingFreq, samplePerFrame)
    return(stft.min(), stft.max(), np.median(stft))

mins = []
maxs = []
medians = []
for classe in os.listdir(volume):
    if classe in classes:
        print("Stats: {}".format(classe))
        if classe not in volume_stats.keys():
            volume_stats[classe] = {}
        for music in os.listdir(volume + classe ):
            stats = stft_stats(volume + classe + '/' + music)
            volume_stats[classe][music] = stats
            mins.append(stats[0])
            maxs.append(stats[1])
            medians.append(stats[2])


dataset_min = np.array(mins).min()
dataset_max = np.array(maxs).max()
dataset_median = np.median(np.array(medians))

for classe in volume_stats:
    for music, stats in volume_stats[classe].items():
        if stats[1] < dataset_median:
            volumes = [1.10, 1.20, 1.30, 1.40]
            for vol in volumes:
                output_name = volume + classe + '/' + music.replace('.wav', '-{}.wav'.format(vol))
                
                audioTools.volumeChanger(volume + classe + '/' + music, output_name, vol)
        elif stats[0] > dataset_median:
            volumes = [0.90, 0.80, 0.70, 0.60]
            for vol in volumes:
                output_name = volume + classe + '/' +music.replace('.wav', '-{}.wav'.format(vol))
                audioTools.volumeChanger(volume + classe + '/' + music, output_name, vol)

        elif stats[0] > dataset_min and stats[2] > dataset_median:
            volumes = [1.10, 0.90, 0.80, 0.70]
            for vol in volumes:
                output_name = volume + classe + '/' +music.replace('.wav', '-{}.wav'.format(vol))
                audioTools.volumeChanger(volume + classe + '/' + music, output_name, vol)

        elif stats[1] < dataset_max and stats[2] < dataset_median:
            volumes = [0.90, 1.10, 1.20, 1.30]
            for vol in volumes:
                output_name = volume + classe + '/' + music.replace('.wav', '-{}.wav'.format(vol))
                audioTools.volumeChanger(volume + classe + '/' + music, output_name, vol)