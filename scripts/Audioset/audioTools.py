import sys, os
import numpy as np
import sys, os
from tqdm import tqdm
from . import audioModule

"""
Módulo com funções utilizadas para o tratamento de ficheiros áudio.
"""

#
# Download
#
def full_link(id):
    """
    Retorna o link do youtube utilizado para fazer o download.

    Args:
        id ([str]): ID do video.

    Returns:
        str: Link do youtube.
    """
    return 'https://www.youtube.com/watch?v={}'.format(id)

def download_songs(downloadFolder, d):
    """
    Função responsável pelo download do video.
    Converte automáticamente o vídeo em áudio

    Args:
        downloadFolder (str): Caminho para a diretoria onde o áudio será guardado.
        d (str): link do vídeo do youtube.
    """
    os.system('youtube-dl -x --audio-format "wav" -q --audio-quality 0 --output "{}%(id)s.%(ext)s" {}'.format(downloadFolder,d))

def list_download(idList, downloadFolder):
    """
    Função responsável pelo o download de vários vídeos.
    Recebe uma lista com IDs de vídeos, e chama a função 
        download_song para que seja feito o download.

    Args:
        idList (List): Lista com IDs.
        downloadFolder (str): Caminho da diretoria onde será guardado os áudios da lista.
    """

    for i in idList:
        download_songs(downloadFolder, full_link(i[0]))


def dataRotation(download_folder, classe, dataLength):
    """Função que trata da rotação do áudio numa dada diretoria.
    Roda dataLength vezes os dados.
    Normalmente, dataLength deverá igualar ao tamanho dos troços de áudio - 10 para 10 segundos.

    Args:   
        download_folder (str): Caminho para a diretoria onde se encontram todos as classes que foram feitas download.
        classe (str): Classe desejada para o download
        dataLength (int): Número de rotações feitas com o troço de áudio.
    """
    folder = download_folder + classe + '/'
    folder_contains = os.listdir(folder)
    for i in tqdm(range(len(folder_contains)), desc="[{}]".format(classe)):
        item = folder_contains[i]
        if item.endswith('.wav'):
            if audioModule.audioDuration(folder + item) >= dataLength:
                for j in range(dataLength):
                    audioModule.wavShift(folder + item, j, outputFile=None, destFolder=None)
        os.unlink(folder + item)

def cut_audio(start, end, filepath, length=None, outputFilename=None, destFolder=None):
    """
    Função encarregue de fazer o corte do áudio.

    Args:
        start (int): Segundo de começo do corte
        end (int): Segundo de fim do corte
        filepath (str): Caminho para o áudio que será cortado
        length (int, opcional): Dimensão esperada. Defaulta para None.
        outputFilename (str, opcional): Nome com que será guardado o troço cortado. Se for None, o troço é guardado com o mesmo nome.
        destFolder (str, opcional): Caminho onde será guardado o troço cortado. Se for None, o troço é guardado no mesmo sítio.
    """
    if float(start) > float(end):
        startTemp = end
        end = start
        start = startTemp

    if length != None:
        if float(end) - float(start) < length:
            audioModule.wavCut(filepath, start, str(float(start)+length), outputFilename=outputFilename, destFolder=destFolder)
        elif float(end)-float(start) > length:
            excess = float(end)-float(start)-length
            audioModule.wavCut(filepath, start, str(float(end)-excess), outputFilename=outputFilename, destFolder=destFolder)
        else:
            audioModule.wavCut(filepath, start, end, outputFilename=outputFilename, destFolder=destFolder)
    else:
        audioModule.wavCut(filepath, start, end, outputFilename=outputFilename, destFolder=destFolder)

def remove_uncut(downloadFolder, length):
    """
    Esta função apaga todos os troços que não foram cortados.

    Args:
        downloadFolder (str): Caminho para a diretoria de corte
        length (int): Dimensão esperada após o corte
    """
    print('remove uncut')
    for i in os.listdir(downloadFolder):
        try:
            if audioModule.audioDuration(downloadFolder + i) > length and os.path.isfile(downloadFolder + i):
                print("Deleting {}".format(i))
                os.unlink(downloadFolder + i)
        except:
            os.unlink(downloadFolder + i)

def remove_short(downloadFolder, length):
    """
    Retira qualquer áudio cuja dimensão esteja abaixo do esperado. 

    Args:
        downloadFolder (str): Caminho para a diretoria.
        length (int): Dimensão esperada do áudio.
    """
    print('remove short')
    for i in os.listdir(downloadFolder):
        if audioModule.audioDuration(downloadFolder + i) < float(length):
            os.unlink(downloadFolder + i)

def extend_audio(filepath, length):
    """
    Chamada da função que estica um troço de áudio.
    Chama: 
        - audioModule.extend_audio

    Args:
        filepath (str): Caminho para o troço a ser esticado
        length (int): Dimensão esperada.
    """
    if audioModule.audioDuration(filepath) < float(length):
        try:
            before = filepath.replace('.wav', '') + '_before.wav'
            os.rename(filepath, before)
            audioModule.extend_audio(before, filepath, length)
        except:
            pass

def extend_audioFolder(downloadFolder, length):
    """
    Esta função estica uma diretoria inteira.
    Chama:
        - extend_audio

    Args:
        downloadFolder (str): Caminho para a diretoria.
        length (int): Dimensão esperada
    """
    print('extend audio folder')
    for i in os.listdir(downloadFolder):
        extend_audio(downloadFolder + i, length)


def renamecut_audio(folder):
    """
    Funcão que altera o nome de audios cortados.

    Args:
        folder (str): Caminho para a Diretoria.
    """
    print('rename cut audio')
    for i in os.listdir(folder):
        a = i[:11] + ".wav"
        os.rename(folder + i, folder + a)


#
# Downsample
#

def downsample(inputPath, outputPath, rate):
    """
    Função que faz o downsampling de um áudio.

    Args:
        inputPath (str): Caminho para o áudio
        outputPath (str): Caminho onde o áuido vai ser guardado
        rate (int): Bitrate para o downsampling
    """
    if os.path.isfile(inputPath):
        os.system('sox {} -c 1 -b 16 -r {} {}'.format(inputPath, rate, outputPath))

def downsample_folder(folderPath, rate):
    """Função que faz o donwsampling de uma diretoria.
    Chama:
        - downsampling
        - rename_downsampling

    Args:
        folderPath (str): Caminho para a diretoria.
        rate (int): Bitrate esperado.
    """
    total = len(os.listdir(folderPath))
    current = 1

    print('Downsampling {}'.format(folderPath))
    
    for i in os.listdir(folderPath):
        print('{} of {}'.format(current, total), end='\r')
        sys.stdout.flush()

        if i.endswith('.wav'):
            inputPath = folderPath + i
            name, fileType = os.path.splitext(i)
            outputPath = folderPath + name + '_' + str(rate) + '.wav'
            downsample(inputPath, outputPath, rate)
            os.unlink(inputPath)
            rename_downsample(outputPath, rate)
        else:
            os.unlink(folderPath + i)
        current += 1

def rename_downsample(path, rate):
    """Função que muda o nome de um troço que foi feito o downsampling

    Args:
        path (str): Caminho para a diretoria
        rate (int): Bitrate do downsampling
    """
    replace = '_' + str(rate)
    a = path.replace(replace, '')
    a = a[:-4]
    os.rename(path, a)


def create1sWAV(filepath):
    ### TODO: Fix this.
    if filepath.endswith('.wav'):
        time = audio_conversion.get_audio_duration(filepath)
        name = i.replace('_16000', '')
        for k in range(int(math.floor(time))):
            print("START: {} | END: {}".format(float(k), float(k+1)))
            os.system('sox {input} -t wavpcm {path}/{name}-{start}to{end}.wav trim {start} ={end}'.format(input=filepath, start=float(k), name=name, end=float(k+1), path=folderpath+'samples'))

def volumeChanger(input_file, ouput_name, value):
    os.system('/usr/bin/sox -v ' + str(value) + ' ' +  input_file  + ' ' + ouput_name)

