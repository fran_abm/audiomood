import datetime
import os
import random
import csv
from collections import namedtuple, Counter
from pathlib import Path
import sys, os, shutil
sys.path.insert(0, os.path.abspath('..'))
import json
from DatasetTools import audioTools, datasetTools, audioModule
import math
import gzip
import subprocess
import signal
import numpy as np
import subprocess
from multiprocessing import Pool

DATA_TYPES = {b'\x08': "Unsigned Byte", b'\x09': "Signed Byte", b'\x0B': "2 Byte Short", b'\x0C': "4 Byte Integer", b'\x0D': "4 Byte Float", b"\x0E": "8 Byte Double"}

"""
Script que cria os datasets com base no Audioset.

Utiliza as seguintes funções:
- build_directories
- build_base_dataset
- new_download_procedure
- cut
- data_rotation
- avg_length_by_folder
- get_downloaded_videos
- create_dataset

Este script utiliza duas listas: uma para as classes positivas e outra para as classes negativas,
retira os IDs dos vídeos que correspondem a essas classes, faz o download (se ainda não foi feito),
retira o áudio, corta o troço de áudio indicado pelo dataset, faz o downsampling desse troço,
e cria os datasets - treino e teste.
"""


# v1 - Base Dataset
#   - POS: ['Screaming']
#   - NEG: ['Speech', 'Singing', 'Yell' ,'Run']
#

#
# v8  - Non Rotated
#   - POS: ['Screaming', 'Yell', "Gunshot", "Machine-gun"]
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter']
#

# v9  - Non Rotated
#   - POS: ['Screaming', 'Yell', ]
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter', "Gunshot", "Machine-gun"]
#

# v10  - Non Rotated
#   - POS: ['Screaming']
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter', "Gunshot", 'Yell', "Machine-gun"]
#


# v11  - Rotated
#   - POS: ['Screaming', 'Yell', "Gunshot", "Machine-gun"]
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter']
#

# v12  - Rotated
#   - POS: ['Screaming', 'Yell']
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter', "Gunshot", "Machine-gun"]
#

# v13  - Rotated
#   - POS: ['Screaming']
#   - NEG: ['Music','Speech', 'Singing', 'Single-lens', 'Camera' ,'Run', 'Silence', 'Laughter', "Gunshot", "Machine-gun", 'Yell']
#


# v14  - Volume
#   - POS: ['Screaming']
#   - NEG: ['Speech', 'Singing','Run', Yell]
#



def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    exit()
    sys.exit(0)

strTime = datetime.datetime.now()

basic = str(Path.home()) + '/Documents/Base/'
rotated = str(Path.home()) + '/Documents/Base-Rotated/'
volume = str(Path.home()) + '/Documents/Volume-Test/'
rotated_volume = str(Path.home()) + '/Documents/Rotated_Volume_Test/'

no_split_flag = False
equal_split = True
split = 189
download_folder = rotated_volume
version = '9'
extra = False
extra_pos = ["Screaming-Extra"]

print('Starting V{}'.format(version))

dataset_folder = str(Path.home()) + '/Documents/Datasets/'
features_file = str(Path.home()) + '/Documents/class_labels_indices.csv'
bal_csv = str(Path.home()) + '/Documents/bal_segments.csv'
unbal_csv = str(Path.home()) + '/Documents/unbal_segments.csv'


if not os.path.isdir(download_folder):
    os.mkdir(download_folder)


json_dataset_path = str(Path.home()) + '/Documents/dataset.json' 


desiredAudioLength = 10
minAudioLength = 7

sampRate = 16000


# positive_labels = ['Screaming']
# negative_labels = ['Speech', 'Singing','Run', "Yell"]
positive_labels = ['Screaming']
negative_labels =  ['Speech', 'Singing', 'Yell' ,'Run']

datasetTools.build_directories(download_folder, positive_labels)
datasetTools.build_directories(download_folder, negative_labels)

pos_classes, neg_classes, conv = datasetTools.convert_labels(positive_labels, negative_labels, features_file)


if not os.path.isfile(json_dataset_path):

    dataset = datasetTools.build_base_dataset(positive_labels, negative_labels)
    dataset = datasetTools.extractVideosAudioset(dataset, conv, bal_csv)
    dataset = datasetTools.extractVideosAudioset(dataset, conv, unbal_csv)
    # datasetTools.datasetStats(dataset)
    datasetTools.write_dataset_json(dataset, json_dataset_path)

else:
    dataset = datasetTools.read_dataset_json(json_dataset_path)


downloadList = []
all_ids = []


def cut(start, end, downloaded_tmp, cutNameTmp, tmp):
    audioTools.cut_audio(start, end, downloaded_tmp, desiredAudioLength, cutNameTmp, tmp)

def new_download_procedure(id, start, end, classe):
    """
    This new procedure downloads, cuts, and downsamples the video in one go.
    """

    finished_op = download_folder + classe + '/' + id + '.wav'

    if not os.path.isfile(finished_op):

        tmp = '/tmp/'
        audioTools.download_songs(tmp, audioTools.fullLink(id))
        downloaded_tmp = tmp + '{}.wav'.format(id)
        cutNameTmp = tmp + '{}-cut.wav'.format(id)

        if os.path.isfile(downloaded_tmp):
            cut(start, end, downloaded_tmp, cutNameTmp, tmp)

            cutLength = audioModule.audioDuration(cutNameTmp)

            if cutLength > desiredAudioLength:
                cut(start, end, downloaded_tmp, cutNameTmp, tmp)
            elif cutLength < desiredAudioLength:
                audioTools.extend_audio(cutNameTmp, desiredAudioLength)

            if os.path.isfile(cutNameTmp):
                os.unlink(downloaded_tmp)

            audioTools.downsample(cutNameTmp, downloaded_tmp, sampRate)

            try:
                if int(audioModule.audioDuration(cutNameTmp)) == 10:
                    shutil.copyfile(downloaded_tmp, finished_op)
                    try:
                        print('Removing: {} and {}'.format(cutNameTmp, downloaded_tmp))
                        os.unlink(cutNameTmp)
                        os.unlink(downloaded_tmp)
                    except OSError as e: # name the Exception `e`
                        print("Failed with:", e.strerror) # look what it says
                        print("Error code:", e.code)
            except:
                pass



def extra_download(id, destFolder, cutSize, overlap):
    """
    Downloads and cuts videos that will be used in extra classes - Not in AudioSet
    """
    destFolder = download_folder + destFolder

    if not os.path.isdir(destFolder):
        os.mkdir(destFolder)

    tmp = '/tmp/'
    audioTools.download_songs(tmp, audioTools.full_link(id))
    cutNameTmp = tmp + '{}-{}-cut.wav'
    downloaded_tmp = tmp + '{}.wav'.format(id)

    if os.path.isfile(downloaded_tmp):
        length = audioModule.audioDuration(downloaded_tmp)
        
        for start in np.arange(0.0, length - 10, float(overlap)):
            end = start + cutSize
            samp = tmp + '{}-' + str(int(start))+'.wav'.format(id)

            finished_op = destFolder  +'/' + id + '-' + str(int(start)) + '.wav'
            a = cutNameTmp.format(id, start)
            cut(start, end, downloaded_tmp, a, tmp)
            audioTools.downsample(a, samp, sampRate)
            if int(audioModule.audioDuration(a)) == 10:
                shutil.copyfile(samp, finished_op)

    

def avg_length_by_folder(folder):
    """
    Average length in all folders with music
    """
    totalLenght = 0

    for i in os.listdir(folder):
        totalLenght += audioModule.audioDuration(folder + '/' + i)
    
    avg = totalLenght/len(os.listdir(folder))

    print('AVG Length of folder: {}'.format(avg))


def get_downloaded_videos(download_folder):
    """
    Reads download folder and get all downloaded videos.
    This exists because not every video will be downloaded
    """
    download = {}

    for classe in os.listdir(download_folder):
        download[classe] = []
        if os.path.isdir(download_folder + classe):
            for song in os.listdir(download_folder + classe):
                download[classe].append(song.replace('.wav', ''))
    
    return download



# Volume Change in all folders.
# volume_range = [0.8, 0.9, 1, 1.1, 1.2]

# for classe in os.listdir(download_folder):
#     for audio in os.listdir(download_folder + classe):
#         for vol in volume_range:
#             output_name = audio.replace('.wav', '-{}.wav'.format(vol))
#             audioTools.volumeChanger(download_folder + classe + '/' + audio, download_folder + classe + '/' + output_name, vol)        
#         os.unlink(download_folder + classe + '/' + audio)


downloaded_videos = get_downloaded_videos(download_folder)


# Download Procedure
# for t in dataset:
#     for classe in dataset[t]:

#         if len(downloaded_videos[classe]) < 150:

#             test = dataset[t][classe][:1000]
#             print('DOWNLOADING: {}'.format(classe))
#             count = 1
            
#             for i in test:

#                 if not i['id'] in downloaded_videos[classe] and i['id'] not in all_ids:
#                     print('{} of {}'.format(count, len(test)), end='\r')
#                     sys.stdout.flush()
#                     new_download_procedure(i['id'], i['srtTime'], i['endTime'], classe )
#                     all_ids.append(i['id'])

#                 count += 1
        # avg_length_by_folder(download_folder + classe)


# for t in dataset:
#     for classe in dataset[t]:
#         audioTools.dataRotation(download_folder, classe, 10)


def pre_dataset(download_folder, equal_split, split=None, extra=False, extra_pos=None):
    """
    Create the structure used in Ubyte creation,
    """
    final_dataset = []
    pos_total = 0
    
    print("Negative Labels: {}".format(negative_labels))
    positive_label = 1
    negative_label = 0
    for t in dataset:
        for classe in dataset[t]:
            if classe in positive_labels:
                for song in os.listdir(download_folder + classe):
                    final_dataset.append(( download_folder + classe + '/' + song, positive_label))
                    pos_total += 1
    
            else:
                if equal_split:
                    num_per_neg_class = int(pos_total/len(negative_labels))
                    if classe in negative_labels:
                        print('Song per Neg Class: {}'.format(num_per_neg_class))
                        for song in os.listdir(download_folder + classe)[:num_per_neg_class]:
                            final_dataset.append((download_folder + classe + '/' + song, negative_label))
                else:
                    if classe in negative_labels:
                        for song in os.listdir(download_folder + classe)[:split]:
                            final_dataset.append((download_folder + classe + '/' + song, negative_label))

    if extra:
        extra_ammount = int(((len(final_dataset) - pos_total) - pos_total) / len(extra_pos))
        print("Adding {} Videos from Extra...".format(extra_ammount))
        for classe in extra_pos:
            for song in os.listdir(download_folder + classe)[:extra_ammount]:
                final_dataset.append((download_folder + classe + '/' + song, 1))
                pos_total += 1

    print('POS: {}'.format(pos_total))
    print('NEG: {}'.format(len(final_dataset) - pos_total))
    return final_dataset


def pre_dataset_with_file(path, train, test):
    """
    Creates the structure used in ubyte creation if using split files.
    """
    train = [x[:-4] for x in open(train, "r").read().split("\n")[:-1]]
    test = [x[:-4] for x in open(test, "r").read().split("\n")[:-1]]
    
    positive_label = 1
    negative_label = 0
    pos = 0
    neg = 0
    final_train_dataset = []
    final_test_dataset = []

    for song in train:
        label, id = song.split('/')
        if label in positive_labels:
            lab = positive_label
        else:
            lab = negative_label

        # if label not in training.keys():
        #     training[label] = []
        list_of_files = [(path + label + '/' + s, lab) for s in os.listdir(path + label) if id in s]
        final_train_dataset.extend(list_of_files)
    
    for song in test:
        label, id = song.split('/')
        if label in positive_labels:
            lab = positive_label
            pos+=1
        else:
            lab = negative_label
            neg+=1
        list_of_files = [(path + label + '/' + s, lab) for s in os.listdir(path + label) if id in s]
        final_test_dataset.extend(list_of_files)

    return final_train_dataset, final_test_dataset


# d = pre_dataset(download_folder, equal_split, split, extra=extra, extra_pos=extra_pos)

training_10, testing_10 = pre_dataset_with_file(download_folder, '/home/francisco/Documents/V1-Files/train_file_v1_seed_10.txt', '/home/francisco/Documents/V1-Files/test_file__v1_seed_10.txt')
# print(training_10)
# training_11, testing_11 = pre_dataset_with_file(download_folder, '/home/francisco/Documents/V1-Files/train_file_v1_seed_11.txt', '/home/francisco/Documents/V1-Files/test_file__v1_seed_11.txt')
# training_12, testing_12 = pre_dataset_with_file(download_folder, '/home/francisco/Documents/V1-Files/train_file_v1_seed_12.txt', '/home/francisco/Documents/V1-Files/test_file__v1_seed_12.txt')
# training_13, testing_13 = pre_dataset_with_file(download_folder, '/home/francisco/Documents/V1-Files/train_file_v1_seed_13.txt', '/home/francisco/Documents/V1-Files/test_file__v1_seed_13.txt')
# training_14, testing_14 = pre_dataset_with_file(download_folder, '/home/francisco/Documents/V1-Files/train_file_v1_seed_14.txt', '/home/francisco/Documents/V1-Files/test_file__v1_seed_14.txt')


seeds = [10, 11, 12, 13, 14]

# for seed in seeds:
# print("Starting Seed: {}".format(seed))

seed = 10

if seed == 10:
    train = training_10
    test = testing_10
elif seed == 11:
    train = training_11
    test = testing_11
elif seed == 12:
    train = training_12
    test = testing_12
elif seed == 13:
    train = training_13
    test = testing_13
elif seed == 14:
    train = training_14
    test = testing_14

random.seed(seed)
random.shuffle(train)
random.shuffle(test)

# train = pr[:int(len(d) * 0.67)]
# test =  pr[int(len(d)*0.67):]

train_file = '/home/francisco/Documents/Datasets/train_file_v{}_seed_{}.txt'.format(version, seed)
test_file = '/home/francisco/Documents/Datasets/test_file__v{}_seed_{}.txt'.format(version, seed)
no_split = '/home/francisco/Documents/Datasets/no_split__v{}_seed_{}.txt'.format(version, seed)

training_data_name = dataset_folder +'audioset_train_data_v{}_seed_{}-ubyte.gz'.format(version, seed)
training_label_name = dataset_folder + 'audioset_train_label_v{}_seed_{}-ubyte.gz'.format(version, seed)

testing_data_name = dataset_folder + 'audioset_test_data_v{}_seed_{}-ubyte.gz'.format(version,seed)
testing_label_name = dataset_folder + 'audioset_test_label_v{}_seed_{}-ubyte.gz'.format(version,seed)


no_split_data_name = dataset_folder + 'audioset_no_split_data_v{}-ubyte.gz'.format(version)
no_split_label_name = dataset_folder + 'audioset_no_split_label_v{}-ubyte.gz'.format(version)

with open(train_file, 'w') as file:
    for i in train:
        file.write('/'.join(i[0].split('/')[-2:]) + '\n')

with open(test_file, 'w') as file:
    for i in test:
        file.write('/'.join(i[0].split('/')[-2:])+ '\n')


training_data, training_labels = [i for i,j in train], [j for i,j in train]
testing_data, testing_labels = [i for i,j in test], [j for i,j in test]

# no_split_data, no_split_labels = [i for i,j in pr], [j for i,j in pr]

datasetTools.create_dataset(training_data, training_labels, training_data_name, training_label_name, 512, 320)
datasetTools.create_dataset(testing_data, testing_labels, testing_data_name, testing_label_name, 512, 320)

    # if no_split_flag:
    #     with open(no_split, 'w') as file:
    #         for i in pr:
    #             file.write('/'.join(i[0].split('/')[-2:])+ '\n')

    #     datasetTools.create_dataset(no_split_data, no_split_labels, no_split_data_name, no_split_label_name, 512, 320)



endTime = datetime.datetime.now()
print('Time Elapsed: {}'.format(endTime - strTime))
subprocess.Popen(['notify-send', 'Dataset Build Finished'])
