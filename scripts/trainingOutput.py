
import matplotlib.pyplot as plt
import os
import json

output_folder = '/home/francisco/Documents/outputs/'


def readFolder(folderPath):
    """
    Reads and returns the correct path for all the output json files available in the given folder

    Args:
        - folderPath: path to the folder that has all the output json files.
    """
    return [output_folder + i for i in os.listdir(folderPath)]


def readJson(filePath):
    with open(filePath) as f:
        return json.load(f)


def dataCleaner(data):
    x, y = [], []
    for d in data:
        x.append(d[0])
        y.append(d[1])

    return x, y



def createPlots(files):
    for f in files:
        name = f.split('/')[-1].split('-')[0]
        output_json = readJson(f)
        training_acc = output_json['training-accuracy']
        validation_acc = output_json['validation-accuracy']
        x, y = dataCleaner(validation_acc)
        plt.plot(x, y)
        plt.ylabel(name)
        plt.savefig('/home/francisco/Pictures/val/{}.png'.format(name))
        plt.close()


def getMaxValidation(files):
    for f in files:
        name = f.split('/')[-1].split('-')[0]

        output_json = readJson(f)
        validation_acc = output_json['validation-accuracy']
        max = 0
        for acc in validation_acc:
            if acc[1] > max:
                max = acc[1]
        print('{}: {}'.format(name, max))

if __name__ == "__main__":
    # createPlots(readFolder(output_folder))   
    getMaxValidation(readFolder(output_folder))