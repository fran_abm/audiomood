import json
import argparse
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import seaborn as sns
import csv
from sklearn.metrics import plot_confusion_matrix
from conf_mat import make_confusion_matrix

def color(ps):
    if ps == "positive":
        return "green"
    else:
        return "red"

def normalization(arr):
    return [(x - arr.min()) / (arr.max() - arr.min()) for x in arr]

def gt():
    output = []
    pos = []
    neg = []
    with open('movie-table.csv') as csvFile:
        spamreader = csv.reader(csvFile, delimiter=',', quotechar='|')
        for i in spamreader:
            if i[0] == 'segment':
                header = i[1:]
            else:
                position = [int(pos) for pos, value in enumerate(i[1:]) if int(value) == 1]
                if position[0] == 0:
                    pos.append(int(i[0]))
                    output.append(1)
                else:
                    neg.append(int(i[0]))
                    output.append(0)
                # output.append(header[position[0]])

    return output, pos, neg

def confusion_matrix(pre_pos, pre_neg, pos_pos, pos_neg):
    TP = []
    TN = []
    FP = []
    FN = []
    for i in pos_pos:
        if i in pre_pos:
            TP.append(i)
        else:
            FP.append(i)
    
    for i in pos_neg:
        if i in pre_neg:
            TN.append(i)
        else:
            FN.append(i)
    P_Percentage = len(TP) + len(FN)
    F_Percentage = len(TN) + len(FP)    

    return np.array([[len(TP)/P_Percentage, len(FP)/P_Percentage],[len(FN)/F_Percentage, len(TN)/F_Percentage]])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('path', metavar='N', type=str, nargs='+',
                    help='Path to train file')

    args = parser.parse_args()

    path = args.path[0]
    ground, pos, neg = gt()
    a = json.load(open(path, 'r'))
    results = {}    
    outputs = a['outputs']
    a = []

    pos_output = []
    neg_output = []
    
    for segment, result in enumerate(outputs):
        if result[0] > result[1]:
            results[segment] = ["negative", result[0]]
            a.append(-result[0])
            neg_output.append(segment)
        else:
            results[segment] = ["positive", result[1]]
            a.append(+result[1])
            pos_output.append(segment)



    conf_matrix  = confusion_matrix(pos, neg, pos_output, neg_output)
    print(conf_matrix)
    make_confusion_matrix(conf_matrix, figsize=(6,6), cbar=False, sum_stats=False)
    plt.show()
    # fig = plt.figure()


    plt.figure(figsize=(8,12))
    normlized = normalization(np.array(a))
    normlized.extend([0]*28)
    ground.extend([0]*28)

    Z = np.array(normlized).reshape((21,60))
    ground = np.array(ground).reshape((21,60))

    ax = sns.heatmap(Z, linewidths=.5, cmap="Blues")
    plt.show()

    plt.figure(figsize=(8,12))
    ax = sns.heatmap(ground, linewidths=.5, cmap="Blues")
    plt.show()