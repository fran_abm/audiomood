import os
import re
import sys

import tldatasets as tld
import json as json
import numpy as np
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import tensorflow.compat.v1 as tf

sys.path.insert(0, os.path.abspath('..'))

import audioTools
import audioModule
import stftFeatures
tf.disable_v2_behavior()

BASE_FOLDER = '/home/francisco/Documents/'
MOVIE_PATH = BASE_FOLDER + 'saw.wav'
MOVIE_NAME = 'saw'
DOWNSAMPLE_PATH = BASE_FOLDER + 'saw_downsample.wav'
CUT_FOLDER = BASE_FOLDER + '{}/'.format(MOVIE_NAME)
MODEL_FOLDER = ''
WINDOW_SIZE = 10
DATASET_PATH = BASE_FOLDER + 'saw_data-ubyte.gz'
HOPSIZE = 5

if not os.path.isdir(CUT_FOLDER):
    os.mkdir(CUT_FOLDER)


def cutMovie(moviePath, cutFolder, windowSize ,hopSize):
    """
    Cuts a movie in pieces with the length being setup with windowSize (int).
    hopSize indicates the interval between one piece and the next.
    cutFolder is the path where the cutPieces will be stored.\
    """
    length = int(audioModule.audioDuration(moviePath))

    j = 0
    for i in range(0, length, hopSize):
        if i + 10 <= length:
            name = str(j)+'.wav'
            print('WINDOW: {}-{}'.format(i, i + windowSize))
            audioTools.cut_audio(i, i + windowSize, moviePath, outputFilename=name, destFolder=cutFolder)
            j += 1

    #  Order by cut number
    l = []
    for i in os.listdir(cutFolder):
        l.append(int(i.split('.')[0]))

    l.sort()

    # Create Pre Dataset
    new = []
    for i in l:
        new.append(cutFolder + str(i) + '.wav')

    return new


def precreate_dataset(folder):
    """
    Creates a pre dataset to be used on dataset creation.

    Args:
        folder (str): path to folder.

    Returns:
        list: List with all the paths to every section of the movie
    """
    return ['{}/{}.wav'.format(folder, j) for j in sorted([int(i.split('.')[0]) for i in os.listdir(folder)])]
    


def create_dataset(l, path):
    """
    Receives an ordered list and a path where the file will be saved, and creates the dataset.
    """
    stftFeatures.writeStft8bitsFiles(l, path, 512, 320)



# audioTools.downsample(MOVIE_PATH, DOWNSAMPLE_PATH, 16000)
# create_dataset(precreate_dataset(BASE_FOLDER + MOVIE_NAME), DATASET_PATH)

expname = 'saw'
expstatus = {}
if os.path.isfile(expname + 'json'):
    with open((expname + '.json'), 'r') as f:
        expstatus = json.loads(f.read())

print (expstatus)
expstatus['status'] = 'running'
print (expstatus)
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()


dataset = tld.read_data_sets(train_dir='./',
    train_images=None,
    train_labels=None,
    validation_images=None,
    validation_labels=None,
    test_images = BASE_FOLDER + 'saw_data-ubyte.gz', test_labels=None, nbclasses=2, one_hot=True,
    imageOnOutput=False, reshape=True, source_url='http://tl.di.fc.ul.pt/s/TF/data/')

def scale (a):
    return tf.math.divide(tf.subtract(a, tf.reduce_min(a)), tf.subtract(tf.reduce_max(a), tf.reduce_min(a)))

def to8bits (a, name):
    return tf.cast(tf.round(tf.multiply(255.0, a)), tf.int32, name=name)

def weight_variable (name):
    file = (name + '.npy')
    init = tf.constant(np.load(file))
    return init

def bias_variable (name):
    file = (name + '.npy')
    init = tf.constant(np.load(file))
    return init

def conv2d (x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


x = tf.placeholder(tf.float32, shape=[None, 163840], name='x')
y_ = tf.placeholder(tf.float32, shape=[None, 2], name='y_')

# Change the following line to use the validation set o the training set.
datasetImages = dataset.test

x_row = x
results = {'duration':0}
x_image = tf.reshape(x_row, [-1, 512, 320, 1])

w1 = weight_variable('W1')
b1 = bias_variable('B1')
y1 = (conv2d(x_image, w1) + b1)
a1 = tf.nn.relu(y1)

w2 = weight_variable('W2')
b2 = bias_variable('B2')
y2 = (conv2d(a1, w2) + b2)
a2 = tf.nn.relu(y2)

mp3 = tf.nn.max_pool(a2, ksize=[1, 8, 2, 1], strides=[1, 8, 2, 1], padding='SAME')
w3 = weight_variable('W3')
b3 = bias_variable('B3')
y3 = (conv2d(mp3, w3) + b3)
a3 = tf.nn.relu(y3)

w4 = weight_variable('W4')
b4 = bias_variable('B4')
y4 = (conv2d(a3, w4) + b4)
a4 = tf.nn.relu(y4)

mp5 = tf.nn.max_pool(a4, ksize=[1, 4, 4, 1], strides=[1, 4, 4, 1], padding='SAME')
w5 = weight_variable('W5')
b5 = bias_variable('B5')
y5 = (conv2d(mp5, w5) + b5)
a5 = tf.nn.relu(y5)

w6 = weight_variable('W6')
b6 = bias_variable('B6')
y6 = (conv2d(a5, w6) + b6)
a6 = tf.nn.relu(y6)

mp7 = tf.nn.max_pool(a6, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
w7 = weight_variable('W7')
b7 = bias_variable('B7')
y7 = (conv2d(mp7, w7) + b7)
a7 = tf.nn.relu(y7)

w8 = weight_variable('W8')
b8 = bias_variable('B8')
y8 = (conv2d(a7, w8) + b8)
a8 = tf.nn.relu(y8)

mp9 = tf.nn.max_pool(a8, ksize=[1, 8, 10, 1], strides=[1, 8, 10, 1], padding='SAME')
w9 = weight_variable('W9')
b9 = bias_variable('B9')
in_flat9 = tf.reshape(mp9, [-1, 512])
y9 = (tf.matmul(in_flat9, w9) + b9)
a9 = tf.nn.relu(y9)

w10 = weight_variable('W10')
b10 = bias_variable('B10')
y10 = (tf.matmul(a9, w10) + b10)
a10 = tf.nn.relu(y10)

w11 = weight_variable('W11')
b11 = bias_variable('B11')
y11 = (tf.matmul(a10, w11) + b11)


netOutput = tf.identity(y11, name='NETOUTPUT')
netOutputScaled = to8bits(scale(netOutput), 'NETOUTPUTSCALED')
loss = tf.losses.mean_squared_error(labels=y_, predictions=netOutput)
losses = tf.reduce_mean(tf.squared_difference(y_, netOutput), axis=1)

def getOutput (sess, set):

    print ('..................................................................')
    allOutputs = np.empty(shape=[0, 2], dtype=int)
    
    nPatterns = set.nPatterns

    set.resetIndex()

    n = int(np.ceil((nPatterns / 32)))
    k = 32

    while not set.epochCompleted:

        batch = set.next_batch_no_labels(32, shuffle=False, circular=False)
        batchOutputs = netOutput.eval(session=sess, feed_dict={x:batch[0]})
        allOutputs = np.append(allOutputs, batchOutputs, axis=0)

    print('NUMBER OF OUTPUTS: {}'.format(len(allOutputs)))
    return allOutputs.tolist()

config = tf.ConfigProto(
        device_count = {'GPU': 1}
    )

init = tf.global_variables_initializer()
start_time = time.time()
with tf.Session(config=config) as sess:
    tf.set_random_seed(10)
    sess.run(init)
    results['outputs'] = getOutput(sess, datasetImages)
    results['duration'] = math.ceil((time.time() - start_time))
    results['hostname'] = socket.gethostname()
    res = resource.getrusage(resource.RUSAGE_SELF)
    results['maxmem'] = res.ru_maxrss
    # Change the following line to save the results in a different file.
    with open('saw-results.json', 'w') as f:
        f.write(json.dumps(results))
        f.close()

    expstatus['status'] = 'finished'
    with open((expname + '.json'), 'w') as f:
        f.write(json.dumps(expstatus))
        f.close()
