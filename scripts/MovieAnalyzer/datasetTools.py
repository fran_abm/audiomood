import csv
import random
import os
import stftFeatures
import datetime
import gzip
import numpy as np
import json
import sys
from collections import namedtuple, Counter




### TODO: documentation is missing !
def convert_labels(pos_classes, neg_classes, file):
    """
    Converte as labels do AudioSet, que estão em Unicode, para texto.

    Args:
        pos_classes (List): Lista com as labels da classe positiva
        neg_classes (List): Lista com as labels da classe negativa
        file (Str): Caminho para o ficheiro do Audioset com as labels.

    Returns:
        conv -> Dict: Dicionário com as todas as conversões
        pos_labels -> List: Lista com as labels positivas convertidas
        neg_labels -> List: Lista com as labels negativas convertidas

    """
    conv = {}
    pos_labels = []
    neg_labels = []
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        for i in reader:
            clas = i[2].split(",")[0]
            if clas in pos_classes:
                pos_labels.append(i[1])
                conv[i[1]] = clas
            elif clas in neg_classes:
                neg_labels.append(i[1])
                conv[i[1]] = clas
            elif clas == 'Single-lens reflex camera':
                neg_labels.append(i[1])
                conv[i[1]] = 'Single-lens'
            elif clas == 'Machine gun':
                pos_labels.append(i[1])
                conv[i[1]] = 'Machine-gun'

    return pos_labels, neg_labels, conv

def build_base_dataset(positive_labels, negative_labels):
    """
    Cria uma pré-dataset que será utilizado para o donwload.

    Args:
        positive_labels (List): Lista das labels que constituem a classe positiva
        negative_labels (List): Lista das labels que constintuem a classe negativa

    Returns:
        Dict: Dicionário que será utilizado para o download.
    """         
    dataset = {'pos_videos': {}, 'neg_videos':{}}

    for i in positive_labels:
        if i not in dataset['pos_videos'].keys():
            dataset['pos_videos'][i] = []

    for i in negative_labels:
        if i not in dataset['neg_videos'].keys():
            dataset['neg_videos'][i] = []
    
    return dataset


def write_dataset_json(dataset, filePath):
    """
    Escreve o pré-dataset para um ficheiro json.

    Args:
        dataset (dict): Pré-Dataset
        filePath (str): Caminho onde será guardado este ficheiro.
    """
    with open(filePath, 'w') as outfile:
        json.dump(dataset, outfile)


def read_dataset_json(filePath):
    """Lê o ficheiro JSON com o pré-dataset.

    Args:
        filePath (str): Caminho para o ficheiro dataset.

    Returns:
        Dict: Pré-Dataset
    """
    with open(filePath) as json_file:
        return json.load(json_file)

def cleanLabels(conv, labels):
    a = []
    for i in labels:
        try:
            a.append(conv[i.strip().replace('"', '')])
        except:
            pass
    return a


def separateValues(conv, line):
    """
    Retrieves the necessary data from the dataset.
    Each line is a line in the csv.
    """

    Line = {'id': '', 'srtTime': '', 'endTime': '', 'labels': ''}
    labels = cleanLabels(conv, line[3:])
    newLine = Line(line[0], line[1], line[2], labels)
    return newLine

def separateValuesAudioset(conv, line):
    """
    Retrieves the necessary data from the dataset.
    Each dictionary is a line in the dataset.
    """
    labels = cleanLabels(conv, line[3:])
    Line = {'id': line[0], 'srtTime': line[1], 'endTime': line[2], 'labels': labels}
    return Line

def writeVideosToFile(filename, line):
    """
    Appends a new line to the end of a csv file.
    Used to create smaller files with the correct audio.
    """
    with open(filename, 'a+') as csv_file:
        spamwriter = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(line)


def extractVideos(pos_classes, neg_classes, conv, file):
    """
    WARNING: DEPRECEATED

    Reads the large files containing the Audioset dataset, and creates two new files \
        one for a the positive audio, and a second for the negative audio.
    Uses writeVideosToFile as the writting utility.
    """
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        next(reader)
        for i in reader:
            line = separateValuesAudioset(conv, i)
            for j in line["labels"]:
                if j in pos_classes:
                    writeVideosToFile('pos_videos.csv', line)
                elif j in neg_classes:
                    writeVideosToFile('neg_videos.csv', line)
                elif j == 'Single-lens reflex camera':
                    writeVideosToFile('neg_videos.csv', line)
                elif j == 'Machine gun':
                    writeVideosToFile('pos_videos.csv', line)


def extractVideosAudioset(dataset, conv, file):
    """
    Reads the Audioset file, and creates a dataset structure to be used by the rest of the program.
    This 
    """
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        next(reader)
        next(reader)

        labels = []
        counter = 0
        for i in reader:
            line = separateValuesAudioset(conv, i)

            for j in line['labels']:
                if j not in labels:
                    labels.append(j)

                if j in dataset['pos_videos'].keys():
                    dataset['pos_videos'][j].append(line)
                elif j in dataset['neg_videos'].keys():
                    dataset['neg_videos'][j].append(line)
                elif j == 'Single-lens reflex camera':
                    line['labels'] = 'Single-lens'
                    dataset['neg_videos']['Single-lens'].append(line)
                elif j == 'Machine gun':
                    line['labels'] = 'Machine-gun'
                    dataset['pos_videos']['Machine-gun'].append(line)
            


            counter += 1
        print(labels)
        print(counter)
        return dataset


def structurizeDataset(pos_videos, neg_videos):
    """
    Reads the two smaller CSV files with the videos to be downloaded
        and creates a 2 structures, one for the positive videos, and a second
        for the negative videos.
    """

    pos_structure = []
    neg_structure = []

    with open(pos_videos, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for i in reader:
            pos_structure.append([i[0], i[1], i[2], 0])

    with open(neg_videos, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for i in reader:
            neg_structure.append([i[0], i[1], i[2], 1])

    random.shuffle(neg_structure)
    neg_structure = neg_structure[0:len(pos_structure)]
    return pos_structure, neg_structure

def createPreDataset(folderPath, dataset):
    """
    TODO: Check if depreceated for Audioset.

    Creates a pre dataset, to be used in the making of the spectogram.
    """
    labels = []
    data = []
    allFiles = os.listdir(folderPath)
    for i in dataset:
        path = folderPath + i[0] + '.wav'
        data.append(path)
        labels.append(i[-1])
    return data, labels

def create_dataset(data, labels, dataname, labelname, height, width):
    """
    Esta função lê os áudios, converte para espectogramas, e escreve os espectograms e as 
        respectivas labels para os seus ficheiros, assim criando so datasets. 

    Chama:
        - stftFeatures.writeStft8bitsFiles

    Args:
        data (List): Lista com os caminhos para os áudios
        labels (List): Lista com a labels que correspondem aos áudios
        dataname (Str): Caminho para p ficheiro de dados
        labelname (Str): Caminho para o ficheiro de labels
        height (Int): Altura esperada do espectograma.
        width (Int): Largura esperada do espectograma.
    """
    print('Data')
    stftFeatures.writeStft8bitsFiles(data, dataname, height, width)

    # Label Creation
    outputFile = gzip.open(labelname, "wb")

    print('Labels')
    outputFile.write(b'\x00')
    outputFile.write(b'\x00')
    outputFile.write(b'\x08')
    outputFile.write(b'\x01')
    outputFile.write((np.int32(len(labels)).byteswap()).tobytes())

    total = len(labels)
    current = 1
    for i in labels:
        print('{} of {}'.format(current, total), end="\r", flush=True)

        outputFile.write(int(i).to_bytes(1, 'big'))
        current += 1
    outputFile.close()


def readLabels(f):
    """
    Reads the labels file from a dataset
    """

    data = []
    with gzip.open(f, "rb") as o:
        magic = o.read(4)
        print(magic)
        numOfItems = int.from_bytes(o.read(4), 'big')
        print(numOfItems)
        for i in range(numOfItems):
            a = o.read(1)
            data.append(a)
    return data


def build_directories(basePath, labels):
    """
    Função que cria as diretorias base, se não existirem.

    Args:
        basePath (str): Caminho para onde serão criadas as diretorias
        labels (List): Lista com as labels das classes.
    """
    for i in labels:
        if i not in os.listdir(basePath):
            os.mkdir(basePath + i)

def clean_directory(path):
    for i in os.listdir(path):
        if os.path.isfile(path + i):
            os.unlink(path + i)


def datasetStatistics(download_folder, pos_labels, neg_labels):
    """
    Dataset statistics for Audioset.

    Reads the download folder where the videos are stored.
    TODO: Add video lengths
    """
    dataset = {'pos':{}, 'neg': {}}
    
    counter_total = 0
    counter_pos = 0
    counter_neg = 0

    for label in pos_labels:

        num = len(os.listdir(download_folder + label))
        
        if label not in dataset['pos']:
            dataset['pos'][label] = {'num_videos': num}
            counter_pos += num
            counter_total += num

    for label in neg_labels:

        num = len(os.listdir(download_folder + label))

        if label not in dataset['neg']:
            dataset['neg'][label] = {'num_videos': num}
            counter_neg += num
            counter_total += num    

    dataset['total'] = counter_total
    dataset['total_pos'] = counter_pos
    dataset['total_neg'] = counter_neg

    return dataset
        


def positiveDownloadedVideos(positive_labels, download_folder):
    numberPos = 0
    for i in positive_labels:
        numberPos += len(os.listdir(download_folder + i))
    print(numberPos)
    return numberPos
