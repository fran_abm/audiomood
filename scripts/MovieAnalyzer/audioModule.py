import librosa
import os
import sys

def audioDuration(filepath):
    return librosa.core.get_duration(filename=filepath)

def convertToMono(inputFilepath, outputFilepath):
    """ comments please! """
    os.system('sox {} -c 1 {}'.format(inputFilepath, outputFilepath))
    return outputFilepath

def convertToMono16k(inputFilepath, outputFilepath):
    """ comments please! """
    os.system('sox {} -c 1 -r 16k {}'.format(inputFilepath, outputFilepath))
    return outputFilepath

def extend_audio(inputFilepath, outputFilepath, desirableLength):
    """
    This function extends the length of a audio file.
    It takes a audio file that is shorter than 10 seconds,
    and extends it to make it 10 seconds long.

    To do so, it takes the difference between the length and 10 seconds:
      10 - 4 (length of audio) = 6
      6 / 2 = 3
    So it will take 3 seconds from the beginning and from the end, and repeat it.

    Arguments:
        inputFilepath {string} -- Path to the audio file to be extended.
        outputFilepath {[type]} -- Path for the extended audio file.
        desirableLength {[type]} -- Desired length of the output audio.
    """

    length = librosa.core.get_duration(filename=inputFilepath)

    cutDuration = (desirableLength - int(length)) / 2

    endTrimStartPoint = length - cutDuration
    audioCutName = inputFilepath.replace('.wav', '')
    startCut = audioCutName + "_firstCut.wav"
    endCut = audioCutName + "_endCut.wav"


    os.system('sox {} {} trim 0 {}'.format(inputFilepath, startCut, cutDuration))
    os.system('sox {} {} trim {} ={}'.format(inputFilepath, endCut, endTrimStartPoint, length))
    os.system('sox {} {} {} {}'.format(startCut, inputFilepath, endCut, outputFilepath))
    os.unlink(startCut)
    os.unlink(endCut)
    os.unlink(inputFilepath)
    return outputFilepath


def getYTAudio(ytid, destFolder, audioFolder='wav'):
    """
     PRE: ytid is YT's video ID (ex: 'ujwGBpRSZuU');
          destFolder is the folder where the audio is saved.
          audioFormat is a string accepted by youtube-dl (wav, mp3 etc..)
     POS: an audio file with filename composed with YT's video title and YT's
          ID is saved in destFolder.
     """
    os.system('/usr/bin/youtube-dl -x --audio-format "' + audioFormat +
              '" --audio-quality 0 --output "' + destFolder +
              '%(title)s-%(id)s.%(ext)s" '+
              'https://www.youtube.com/watch?v=' + ytid )


def wavCut(wavFile, start, stop, outputFilename=None, destFolder=None):
    """
     PRE: wavFile is the filename of the audio file; start and stop are
          starting end ending times in seconds;
          outputFilename is optional. If not specified the input name is
          used plus an indication of starting and ending times.
          the resulting audio is saved.
          destFolder (optional) is the destination folder unless specified
          in outputFolder.
     POS: an excerpt of the audio is written.
    """
    if destFolder == None:
        destFolder = os.path.dirname(wavFile)
    if outputFilename == None:
        f = os.path.basename(wavFile).replace('.wav', '')
        outputFilename = destFolder + '/' + f + '_' +str(start).replace(' ', '')+'to'+str(stop).replace(' ', '') + '.wav'
    os.system('/usr/bin/sox ' + wavFile + ' ' + outputFilename +' trim ' + str(start) + ' =' + str(stop).replace(' ', ''))
    return outputFilename

def wavSlice(wavFile, sliceDurationInSec, hopSize):
    """
     PRE: wavFile is the filename of the audio file; sliceDurationInSec is the
          slice duration in seconds (!); hopSize is the hop size (seconds).
     POS: audio excerpts are written into files.
    """
    duration = librosa.core.get_duration(filename=wavFile)
    allFiles = []
    for time in np.arange(0, duration, hopSize):
        outputFile = os.path.dirname(wavFile) + '/' + os.path.basename(wavFile) + '-' + str(time) + 'to' + str(time+hopSize)
        allFiles = allFiles + outputFile
        os.system('/usr/bin/sox ' + wavFile + ' -t wavpcm ' + outputFile + ' trim ' + \
                  str(time) + ' =' + str(time+hopSize))
    return allFiles

def wavConcat(wavFile1, wavFile2, outputFile):
    # TODO :-)

    os.system('/usr/bin/sox ' + wavFile1 + ' ' + wavFile2 + ' ' + outputFile)
    # sox wavFile1 wavFile2 outputFile

def wavShift(wavFile, delta, outputFile=None, destFolder=None):
    """
    |0123456789abcdefghij|
    |<---------><-delta->| => |<-delta-><--------->|
                              |bcdefghij0123456789a|
    """
    if outputFile != None and destFolder != None:
        outputFile = destFolder + outputFile
    if outputFile == None and destFolder == None:
        base = '.'.join(wavFile.split('.')[0:-1])
        outputFile = base + '-shift' + str(delta) + '.wav'
    if outputFile == None and destFolder != None:
        base = '.'.join(wavFile.split(str='.')[0:-1])
        outputFile = destFolder + '/' + base + '-shift' + str(delta) + '.wav'
    print(outputFile)
    
    length = audioDuration(wavFile)
    firstPart = wavCut(wavFile, 0, length - delta, destFolder='/tmp')
    secondPart = wavCut(wavFile, length - delta, length, destFolder = '/tmp')
    wavConcat(secondPart, firstPart, outputFile)
    os.unlink(firstPart)
    os.unlink(secondPart)
    return outputFile
