import json
import os
import csv
import sys
import argparse
import shutil
import zipfile
import statistics
# from natsorted import natsorted
import numpy as np


tableData = {"screaming":132, 
        "yell":116, 
        "gunshot": 18, 
        "music":72, 
        "speech":515, 
        "singing": 8, 
        "camera": 32, 
        "run": 0, 
        "silence": 9, 
        "laughter": 1, 
        "other": 354
    }


def train_output(path):
    with open(path) as jsonfile:
        a = json.load(jsonfile)
        final_train = a["training-accuracy"][-1][1] * 100
        final_validation = float(a["final-validation-accuracy"]) * 100
        return final_train, final_validation 


def groundTruth(positive_classes, negative_classes):
    movie_table = {}

    truePositives = []
    trueNegatives = []

    with open('movie-table.csv') as csvFile:
        spamreader = csv.reader(csvFile, delimiter=',', quotechar='|')

        header = []

        for i in spamreader:

            if i[0] == 'segment':
                header = i[1:]
                for j in header:
                    movie_table[j] = []
            else:
                position = [int(pos) for pos, value in enumerate(i[1:]) if int(value) == 1]
                segment = int(i[0])
                for pos in position:
                    movie_table[header[pos]].append(int(segment))

    for classe in movie_table:
        if classe in positive_classes:
            truePositives.extend(movie_table[classe])
        else:
            for segment in movie_table[classe]:
                if segment not in truePositives:
                    trueNegatives.append(segment)
    
    return movie_table, truePositives, trueNegatives


def test(pos, neg):
    movie_table, truePositives, trueNegatives = groundTruth(pos, neg)
    positive = []
    output = []
    
    with open('saw-results.json', 'r') as jsonFile:
        a = json.load(jsonFile)
        
        outputs = a['outputs']
        for number, value in enumerate(outputs):
            if value[1] > value[0]:
                positive.append(number)
            output.append(number)
    
    negative = len(output) - len(positive)

    truPos = []
    truNeg = []
    falsePos = []
    falseNeg = []

    for i in output:
        if i in positive and i in truePositives:
            truPos.append(i)
        elif i not in positive and i in trueNegatives:
            truNeg.append(i)
        elif i in positive and i in trueNegatives:
            falsePos.append(i)
        elif i not in positive and i in truePositives:
            falseNeg.append(i)

    truPosLenght = len(truPos)
    falsePosLenght = len(falsePos)
    falseNegLenght = len(falseNeg)

    if len(truPos) > 0:
        precision = (truPosLenght/(truPosLenght + falsePosLenght)) * 100
        recall = (truPosLenght/(truPosLenght + falseNegLenght)) * 100
        f_score = (2 * ((precision * recall) / (precision + recall)))

    else:
        precision = 0
        recall = 0
        f_score = 0

    # Output is the number of FP per Label
    output = {}

    # Records is Percentage of FP per Label
    records = {}

    for i in movie_table.keys():
        output[i] = []
        records[i] = 0

    for i in falsePos:
        for j in movie_table:
            if i in movie_table[j]:
                output[j].append(i)
    
    for i in output:
        try:
            records[i] = (len(output[i]) / len(movie_table[i])) * 100
        except:
            records[i] = 0
    
    return precision, recall, f_score, output, records

def average(list):
    if len(list) > 1 and type(list[0]) != type([]):
        return sum(list)/len(list)
    elif len(list) > 1 and type(list[0]) == type([]):
        return average([average(inner_list) for inner_list in list])
    else:
        return "No Data"

def sdv(list):
    if len(list) > 1 and type(list[0]) != type([]) and type(list) == type([]):
        return np.std(list)
    elif len(list) > 1 and type(list[0]) == type([]):
        return np.std(list[0])
    else:
        return "No Data"

def convergedOutputs(all_ex):
    converged = {}

    for ex in all_ex:
        for key in all_ex[ex]:
            if key not in converged.keys():
                converged[key] = []
            for a in all_ex[ex][key]:
                if a not in converged[key]:
                    converged[key].append(a)
    return converged

def conversionTable(output, pos):
    for key in output:
        if key not in pos:
            print("{} classified as Positive: {}".format(key, len(output[key])))


def fileAnalisys(path):
    removable = []
    testing_files = []
    with zipfile.ZipFile(path ) as z:
        listOfFileNames = z.namelist()
    
        for fileName in listOfFileNames:
            if fileName.endswith('.json'):
                testing_files.append(fileName)
                if fileName not in removable:

                    removable.append(fileName)

                # Extract a single file from zip
                z.extract(fileName, '.')

        try:
            train_file = [i for i in testing_files if "yt" in i][0]
            final_train, final_validation = train_output(train_file)
        except:
            final_train, final_validation = 0, 0


        precision, recall, f_score, output, records = test(positive_classes, negative_classes)
        for file in removable:
            os.remove(file)
        return final_train, final_validation, precision, recall, f_score, output, records

def folderAnalysis(path):
    outputs = {}
    fp = {}
    total_records = {}
    for folder in os.listdir(path):
        
        final_train, final_validation, precision, recall, f_score, output, records = fileAnalisys(path + folder)
        outputs[folder] = {"train": final_train, "val": final_validation, "precision":precision, "recall": recall, "f-score": f_score}
        fp[folder] = output
        total_records[folder] = records

    train = [outputs[i]["train"] for i in outputs]
    val = [outputs[i]["val"] for i in outputs]
    precision = [outputs[i]["precision"] for i in outputs]
    recall = [outputs[i]["recall"] for i in outputs]
    fscore = [outputs[i]["f-score"] for i in outputs]

    return fp, train, val, precision, recall, fscore, total_records

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--fullFolder', metavar='N', type=str, nargs='+',
                        help='Runs testing for the full folder of Outputs - Stores as JSON. No output')
    
    parser.add_argument('--fullVersion', metavar='N', type=str, nargs='+',
                        help='Runs testing for the full folder of Outputs - Stores as JSON. No output')

    parser.add_argument('--folder', metavar='N', type=str, nargs='+',
                        help='Runs testing for a folder of outputs - Output')

    parser.add_argument('--file', metavar='N', type=str, nargs='+',
                        help='Runs Testing for a single file - Output.')
    
    parser.add_argument('--individual', action="store_true",
                        help='Used with --folder, shows output for each file in folder')
    
    parser.add_argument('--movieAnalysis', action="store_true",
                        help='Used to only show the movie details')

    parser.add_argument('--comparison', metavar='N', type=str, nargs='+',
                    help='Compares 2 versions.')
    
    positive_classes = ['screaming']
    negative_classes = []
    args = parser.parse_args()

    # path = args.folder[0]
    removable = []
    fp1 = {}

    if args.folder and not args.individual:
        path = args.folder[0]

        fp, train, val, precision, recall, fscore, records = folderAnalysis(path)
        conOutputs = convergedOutputs(fp)
        avg_train = "{:.2f}".format(average(train))
        sdv_train = "{:.2f}".format(sdv(train))
        avg_val = "{:.2f}".format(average(val))
        sdv_val = "{:.2f}".format(sdv(val))
        avg_precision = "{:.2f}".format(average(precision))
        sdv_precison = "{:.2f}".format(sdv(precision))
        avg_recall = "{:.2f}".format(average(recall))
        sdv_recall = "{:.2f}".format(sdv(recall))
        avg_fscore = "{:.2f}".format(average(fscore))
        sdv_fscore = "{:.2f}".format(sdv(fscore))

        with open("results.csv", "w", newline="") as csfFile:
            fieldnames = ["type",'train_precision', 'validation_precision', "precision", "recall", "fscore"]
            spamwriter = csv.writer(csfFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(fieldnames)
            spamwriter.writerow(["avg", avg_train, avg_val, avg_precision, avg_recall, avg_fscore])
            spamwriter.writerow(["sdv", sdv_train, sdv_val, sdv_precison, sdv_recall, sdv_fscore])
        if not args.movieAnalysis:
            print("Training AVG: {}".format(average(train)))
            print("Training SDV: {}".format(sdv(train)))

            print()

            print("Val AVG: {}".format(average(val)))
            print("Val SDV: {}".format(sdv(val)))

            print()

            print("Precision AVG: {}".format(average(precision)))
            print("Precision SDV: {}".format(sdv(precision)))

            print()

            print("Recall AVG: {}".format(average(recall)))
            print("Recall SDV: {}".format(sdv(recall)))


            print()

            print("F-Score AVG: {}".format(average(fscore)))
            print("F-Score SDV: {}".format(sdv(fscore)))

            print()

        conversionTable(conOutputs, positive_classes)


    elif args.folder and args.individual:
        path = args.folder[0]
        fp1 = {}
        with open("results.csv", "w", newline="") as csfFile:
            fieldnames = ["id",'train_precision', 'validation_precision', "precision", "recall", "fscore"]
            spamwriter = csv.writer(csfFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(fieldnames)

            for folder in sorted(os.listdir(path)):
                
                print(folder)
                print()
                final_train, final_validation, precision, recall, f_score, output, records = fileAnalisys(path + folder)
                spamwriter.writerow([folder, "{:.2f}".format(final_train), "{:.2f}".format(final_validation), "{:.2f}".format(precision), "{:.2f}".format(recall), "{:.2f}".format(f_score)])
                
                if not args.movieAnalysis:

                    print("Training: {}".format(final_train))
                    print("Val: {}".format(final_validation))

                    print()

                    print("Precision: {}".format(precision))
                    print("Recall: {}".format(recall))
                    print("F-Score: {}".format(f_score))
                    print()
                
                conversionTable(output, positive_classes)
                print()
            
    elif args.file:

        path = args.file[0]
       
        final_train, final_validation, precision, recall, f_score, output, records = fileAnalisys(path)
        with open("results.csv", "w", newline="") as csfFile:
            fieldnames = ["id",'train_precision', 'validation_precision', "precision", "recall", "fscore"]
            spamwriter = csv.writer(csfFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(fieldnames)
            spamwriter.writerow([path, "{:.2f}".format(final_train), "{:.2f}".format(final_validation), "{:.2f}".format(precision), "{:.2f}".format(recall), "{:.2f}".format(f_score)])

        if not args.movieAnalysis:
            print("Training: {}".format(final_train))
            print("Val: {}".format(final_validation))

            print()

            print("Precision: {}".format(precision))
            print("Recall: {}".format(recall))
            print("F-Score: {}".format(f_score))
            print()

        conversionTable(output, positive_classes)
    
    elif args.fullVersion:
        with open("results.csv", "w", newline="") as csfFile:
            fieldnames = ["id",'train_precision', 'validation_precision', "precision", "recall", "fscore"]
            spamwriter = csv.writer(csfFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(fieldnames)
            base_path = args.fullVersion[0]
            fp1 = {}
            version_output = {"train": [], "validation": [], "precision":[], "recall": [], "fscore": []}
            all_records = {}
            for exc in os.listdir(base_path):
                fp, train, val, precision, recall, fscore, records = folderAnalysis(base_path + exc + '/')
                spamwriter.writerow(
                [exc, "{:.2f}".format(average(train)), 
                "{:.2f}".format(average(val)), 
                "{:.2f}".format(average(precision)), 
                "{:.2f}".format(average(recall)), 
                "{:.2f}".format(average(fscore))
                ]
                )

                version_output["train"].append(train)
                version_output["validation"].append(val)

                version_output["precision"].append(precision)
                version_output["recall"].append(recall)
                version_output["fscore"].append(fscore)
                fp1[exc] = fp
                all_records[exc] = records

            # Not need as we are working with Percentages of FP.
            avg1 = {}
            for folder in fp1:
                for version in fp1[folder]:
                    for t in fp1[folder][version]:
                        if t not in avg1:
                            avg1[t] = []

                        avg1[t].append((len(fp1[folder][version][t])))

            avg_records = {}
            for folder in all_records:
                for zip in all_records[folder]:
                    for t in all_records[folder][zip]:
                        if t not in avg_records.keys():
                            avg_records[t] = []
                        avg_records[t].append(all_records[folder][zip][t])


            spamwriter.writerow(
                ["version","avg", "{:.2f}".format(average(version_output["train"])), 
                "{:.2f}".format(average(version_output["validation"])), 
                "{:.2f}".format(average(version_output["precision"])), 
                "{:.2f}".format(average(version_output["recall"])), 
                "{:.2f}".format(average(version_output["fscore"]))
                ]
                )
            spamwriter.writerow(
                ["version","sdv", "{:.2f}".format(sdv(version_output["train"])), 
                "{:.2f}".format(sdv(version_output["validation"])), 
                "{:.2f}".format(sdv(version_output["precision"])), 
                "{:.2f}".format(sdv(version_output["recall"])), 
                "{:.2f}".format(sdv(version_output["fscore"]))]
                )


            for t in avg_records:
                spamwriter.writerow(
                    [t, average(avg_records[t]), sdv(avg_records[t])]
                    )
                print("")
                print("Total Number of {}: {}".format(t, tableData[t]))

                print("Average Number of {} as FP: {}".format(t, average(avg1[t])))
                print("Average Percentage of {} as FP: {}".format(t, average(avg_records[t])))
                print("SDV Number of {} as FP: {}".format(t, sdv(avg_records[t])))
                print()

        # print("Average Train Precision for Version 1: {}".format(average(version_output["train"])))
        # print("SDV Train for Version: {}".format(sdv(version_output["train"])))
        # print()
        # print("Average Val Precision for Version 1: {}".format(average(version_output["validation"])))
        # print("SDV Val Precision for Version: {}".format(sdv(version_output["validation"])))

        # print()
        # print("Average Test Precision for Version 1: {}".format(average(version_output["precision"])))
        # print("SDV Test Precision for Version: {}".format(sdv(version_output["precision"])))

        # print()
        # print("Average Test Recall for Version 1: {}".format(average(version_output["recall"])))
        # print("SDV Test Recall for Version: {}".format(sdv(version_output["recall"])))

        # print()
        # print("Average Test Fscore for Version 1: {}".format(average(version_output["fscore"])))
        # print("SDV Test Fscore for Version: {}".format(sdv(version_output["fscore"])))

        # print()
        # print()



    elif args.fullFolder and not args.individual:
        base_path = args.fullFolder[0]

        full_folder = {}
        fp1 = {}
        for folder in os.listdir(base_path):
            path = base_path + folder + "/"
            full_folder[folder] = {}
            fp1[folder] = {}
            for zip in os.listdir(path):
                zip_path = path + zip + "/"
                fp, train, val, precision, recall, fscore = folderAnalysis(zip_path)
                fp1[folder][zip] = fp
                full_folder[folder][zip] = {"fp": fp, "train": train, "precision": precision, "recall": recall, "fscore": fscore}


        with open("full_output.json", "w") as jsonFile:
            json.dump(full_folder, jsonFile)

    elif args.comparison:
        if len(args.comparison) != 2:
            print("Input Incorrect.")
        else:
            version1 = args.comparison[0]
            version2 = args.comparison[1]

            version1_output = {"train": [], "validation": [], "precision":[], "recall": [], "fscore": []}
            version2_output = {"train": [], "validation": [], "precision":[], "recall": [], "fscore": []}

            fp1 = {}
            fp2 = {}

            for folder in os.listdir(version1):
                fp, train, val, precision, recall, fscore = folderAnalysis(version1 + folder + '/')
                version1_output["train"].append(train)
                version1_output["validation"].append(val)

                version1_output["precision"].append(precision)
                version1_output["recall"].append(recall)
                version1_output["fscore"].append(fscore)

                fp1[folder] = fp

            for folder in os.listdir(version2):
                fp, train, val, precision, recall, fscore = folderAnalysis(version2 +  folder + '/')
                version2_output["train"].append(train)
                version2_output["validation"].append(val)

                version2_output["precision"].append(precision)
                version2_output["recall"].append(recall)
                version2_output["fscore"].append(fscore)
                fp2[folder] = fp

            avg1 = {}
            avg2 = {}

            for folder in fp1:
                for version in fp1[folder]:
                    for t in fp1[folder][version]:
                        if t not in avg1:
                            avg1[t] = []
                    
                        avg1[t].append(len(fp1[folder][version][t]))
            
            for folder in fp2:
                for version in fp2[folder]:
                    for t in fp2[folder][version]:
                        if t not in avg2:
                            avg2[t] = []
                    
                        avg2[t].append(len(fp2[folder][version][t]))
            
            print("Average Train Precision for Version 1: {}".format(average(version1_output["train"])))
            print("Average Train Precision for Version 2: {}".format(average(version2_output["train"])))
            print()
            print("Average Val Precision for Version 1: {}".format(average(version1_output["validation"])))
            print("Average Val Precision for Version 2: {}".format(average(version2_output["validation"])))
            print()
            print("Average Test Precision for Version 1: {}".format(average(version1_output["precision"])))
            print("Average Test Precision for Version 2: {}".format(average(version2_output["precision"])))
            print()
            print("Average Test Recall for Version 1: {}".format(average(version1_output["recall"])))
            print("Average Test Recall for Version 2: {}".format(average(version2_output["recall"])))
            print()
            print("Average Test Fscore for Version 1: {}".format(average(version1_output["fscore"])))
            print("Average Test Fscore for Version 2: {}".format(average(version2_output["fscore"])))
            print()

            for t in avg1:
                print("Average Number of {} as FP in Version 1: {}".format(t, average(avg1[t])))
                print("Average Number of {} as FP in Version 2: {}".format(t, average(avg2[t])))
                print()