#
# Script fully working.
#


import os
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import random
import numpy as np
from collections import OrderedDict
import time

from Audiomood import Ubyte
from Audiomood import Audio
from Audiomood import Spectogram
from Audiomood import Header
from Audiomood import Conversion


def getAlternativeList(filename):
    with open(filename, "r") as f:
        return f.read().splitlines()


def getAllFoldersFromAudio(folderpath):
    return os.listdir(folderpath)


def getLabel(filepath):
    """
    As the labels are in the file path, we need to retrieve the labels, so that we can 
        create a dataset with labels. 
    """
    return filepath.split("/")[-2]  

def getLabelConversion(fullList, l):
    return fullList.index(l)


def createSpectogram(list_of_files, folders):
    """
    Given a list of files, this function will give the data already
    """
    data = []
    labels = []
    shape = (128, 44)
    for i in list_of_files:
        # print("Starting: ", i)
        labels.append(getLabelConversion(folders, getLabel(i)))
        spec = Conversion.audioToSpec(i)
        print(spec.spectogram.shape)
        
        rec = np.rint(spec.rectifyValues(0,255))
        toAppend = []
        for j in rec:
            for k in j:
                toAppend.append(k)
        # print("Size Before: ", len(toAppend))
        diff = shape[0] * shape[1] - len(toAppend)
        # print("Diff: ", diff)
        if diff > 0:
            for i in range(diff):
                toAppend.append(0)
            
        # print("Size after: ", len(toAppend))
        for j in toAppend:
            data.append(int(j))
        shape = spec.spectogram.shape
    return data, labels, shape

def createUbyte(data, labels, shape):
    """
    The shape is needed for the number of columns and rows.
    The shape is retrieved when the spectogram is done. Is the size of the image.
    """
    d = Ubyte.Ubyte()
    l = Ubyte.Ubyte()
    dH = Header.Header()
    lH = Header.Header()
    dH.setNumOfRows(shape[0])
    dH.setNumOfCols(shape[1])
    dH.setNumOfItems(len(labels))
    lH.setType("labels")
    lH.setMagic(2049)
    lH.setNumOfItems(len(labels))
    d.setHeader(dH)
    l.setHeader(lH)
    d.setData(data)
    l.setData(labels)
    return d, l

if __name__ == "__main__":
    base_folder ="/Users/itsector/Documents/tfsrc/" 
    audioPath = base_folder
    folders = getAllFoldersFromAudio(audioPath)
    usedFolders = [audioPath + i+"/" for i in folders if os.path.isdir(i)]
    testingList = [i.split("/") for i in getAlternativeList(base_folder + "testing_list.txt")]
    valList = [i.split("/") for i in getAlternativeList(base_folder + "validation_list.txt")]
    testingList = [audioPath + i[0] + "/" + i[1] for i in testingList if i[0] in folders]
    valList = [audioPath + i[0] + "/" + i[1] for i in valList if i[0] in folders]
    full = [os.listdir(i) for i in usedFolders if i != '.DS_Store']
    print(len(full))
    result = OrderedDict(zip(usedFolders, full))
    train = []
    for k,v in result.items():
        for i in v:
            if k+i not in testingList and  k+i not in valList:
                train.append(k+i)
    random.shuffle(train, seed=11)
    trainData, trainLabel, shape = createSpectogram(train, folders)
    print("Train Data: %s, Expected Data Lenght: %s" % (len(trainData), shape[0]*shape[1]*len(trainLabel)))
    print("Starting Ubyte file creation.")
    if not os.path.exists(base_folder + "ubyte/"):
        print("Creating Folder")
        os.mkdir(base_folder + "ubyte/")
    trainData, trainLabel = createUbyte(trainData, trainLabel, shape)
    trainData.saveFile("TFSRC-full-train-data-idx3-ubyte")
    trainLabel.saveFile("TFSRC-full-train-label-idx1-ubyte")

    random.shuffle(valList)
    valData, valLabel, valShape = createSpectogram(valList, folders)
    print("Validation Data: %s, Expected Validation Lenght: %s" % (len(valData), shape[0]*shape[1]*len(valLabel)))
    print("Starting Ubyte file creation.")

    valData, valLabel = createUbyte(valData, valLabel, shape)
    valData.saveFile("TFSRC-full-val-data-idx3-ubyte")
    valLabel.saveFile("TFSRC-full-val-label-idx1-ubyte")

    random.shuffle(testingList)
    testData, testLabel, testShape = createSpectogram(testingList, folders)
    print("Testing Data: %s, Expected Testing Length: %s" % (len(testData), shape[0]*shape[1]*len(testLabel)))
    print("Starting Ubyte file creation.")

    testData, testLabel = createUbyte(testData, testLabel, shape)
    testData.saveFile("TFSRC-full-test-data-idx3-ubyte")
    testLabel.saveFile("TFSRC-full-test-label-idx1-ubyte")

    print("Finished.")



    