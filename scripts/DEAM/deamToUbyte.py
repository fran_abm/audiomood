
import os
import sys, os
sys.path.insert(0, os.path.abspath('/Users/itsector/Documents/audiomood/'))

import random
import numpy as np
from collections import OrderedDict
import time
import csv 
import librosa

from Audiomood import Ubyte
from Audiomood import Audio
from Audiomood import Spectogram
from Audiomood import Header
from Audiomood import Conversion

#
# TODO:
#   - Criar o mesmo mas com as anotacoes dinamicas

def rectify_values(low, high, mn, mx, value):
    """
    Normally, the values created in the spectogram are very small floats.
    This is not optimal for a CNN, so we need to rectify the output values.
    Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
        which is the values on the greyscale.
    
    Args:
        - low, a int, the smallest value in the rectified spectogram.
        - high, a int, the largest value in the rectified spectogram.

    Returns:
        - a list of lists, with the spectogram rectified.
    """

    alpha = 0
    
    if mn != mx:
        alpha = (low - high)/(mn - mx)
    
    beta = low - alpha*mn
    return value * alpha + beta


def rectifyValues(spectogram, low, high):
    minimum = spectogram.min()
    maximum = spectogram.max()
    alpha = 0
    
    if minimum != maximum:
        alpha = (low - high)/(minimum - maximum)
    
    beta = low - alpha*minimum
    rectified = spectogram * alpha + beta
    return rectified



def get_static_anotation(anotation_folder):
    file_1 = 'static_annotations_averaged_songs_1_2000.csv'
    file_2 = 'static_annotations_averaged_songs_2000_2058.csv'
    id_va_pair = []
    with open(anotation_folder + file_1) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                id_va_pair.append( [ row[0], ( float(row[1]), float(row[3]) ) ] ) 
            line_count += 1    
    with open(anotation_folder + file_2) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                id_va_pair.append([row[0], (float(row[1]), float(row[3]))])
            line_count += 1    
    return id_va_pair

def conversion_va(v, a):
    if v > 0 and a > 0:
        # HVHA
        return 0
    elif v < 0 and a > 0:
        # LVHA
        return 1
    elif v < 0 and a < 0:
        # LVLA
        return 2
    elif v > 0 and a < 0:
        # HVLA
        return 3

def transform_va_values(l):
    s_v = 1000
    s_a = 1000
    h_v = 0
    h_a = 0
    for i in l:
        v = i[1][0]
        a = i[1][1]
        if v < s_v:
            s_v = v
        if v > h_v:
            h_v = v
        if a < s_a:
            s_a = a
        if a > h_a:
            h_a = a

    new_list = []
    for i in l:
        v = rectify_values(-1, 1, s_v, h_v, i[1][0])
        a = rectify_values(-1, 1, s_a, h_a, i[1][1])
        new_list.append([i[0], conversion_va(v, a)])
    return new_list

def open_audio(l, audio_folder):
    '''
    After making the list with the ids, and the VA, we pass the list to this function to add the tacks.
    '''
    audio_list = [i for i in os.listdir(audio_folder)]
    for i in l:
        print(i[0])
        i.append([])
        file_name = i[0] + '.mp3'
        if file_name in audio_list:
            for j in range(int(librosa.get_duration(filename=audio_folder + file_name))):
                new_audio = Audio.Audio()
                new_audio.setAudio(audio_folder + file_name, offset=j, duration=1)
                spec = new_audio.createSpectogram()
                spec.createSpectogram()
                rec = np.rint(rectifyValues(spec.spectogram, 0, 255))
                i[2].append(rec)
    return l

def save_ubyte(l, label_name, data_name):
    labels = []
    data = []
    shape = (128, 44)

    for i in l:
        labels.append(i[1])
        for j in i[2]:
            to_append = []
            for k in j:
                for h in k:
                    to_append.append(int(h))

            diff = 128*44 - len(to_append)
            if diff > 0:
                for i in range(diff):
                    to_append.append(0)

            for j in to_append:
                data.append(j)

    d = Ubyte.Ubyte()
    l = Ubyte.Ubyte()
    dH = Header.Header()
    lH = Header.Header()
    dH.setNumOfRows(shape[0])
    dH.setNumOfCols(shape[1])
    dH.setNumOfItems(len(labels))
    lH.setType("labels")
    lH.setMagic(2049)
    lH.setNumOfItems(len(labels))
    d.setHeader(dH)
    l.setHeader(lH)
    d.setData(data)
    l.setData(labels)
    d.saveFile(data_name)
    l.saveFile(label_name)

if __name__ == "__main__":
    a = transform_va_values(get_static_anotation('/Users/itsector/Documents/DEAM/annotations/annotations_averaged_per_song/song_level/'))
    k = open_audio(a[:500], '/Users/itsector/Documents/DEAM/audio/')
    save_ubyte(k,"DEAM-label-idx1-ubyte", "DEAM-data-idx3-ubyte")
    test = open_audio(a[500:600], '/Users/itsector/Documents/DEAM/audio/')
    val  = open_audio(a[600:700], '/Users/itsector/Documents/DEAM/audio/')
    save_ubyte(test ,"DEAM-train-label-idx1-ubyte", "DEAM-train-data-idx3-ubyte")
    save_ubyte(val, "DEAM-val-label-idx1-ubyte", "DEAM-val-data-idx3-ubyte")
