import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to show the comparison between the average emotion and the emotion by person
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_per_each_rater/dynamic/'
    arousal = base_folder + 'arousal/'
    valence = base_folder + 'valence/'
    base_folder_2 = '/home/quietswami/Documents/DEAM/annotations/annotations_averaged_per_song/dynamic/'
    arousal_csv = base_folder_2 + 'arousal.csv'
    valence_csv = base_folder_2 + 'valence.csv'

    arousal_dir = os.listdir(arousal)
    # random.shuffle(arousal_dir)

    combine_csv = {}
    song = ''
    a = {}
    b={}
    with open(arousal_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        r = next(reader)
        for k in reader:
            a[k[0]]  = [j for j in k[1:]]


    with open(valence_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        r = next(reader)
        for k in reader:
            b[k[0]] = [j for j in k[1:]]


    for j in a:
        combine_csv[j] = list(zip(a[j],b[j]))


    combine = {}
    for i in arousal_dir:
        song = i
        a = {}
        b = {}
        with open(arousal + i, 'r') as csv_reader:
            reader = csv.reader(csv_reader, delimiter=',')
            r = next(reader)
            rater = 0 
            for k in reader:
                if r[0] == "WorkerId":
                    a[rater] = [j for j in k[1:]]
                else:
                    a[rater] = [j for j in k]

                rater+=1

        with open(valence + i, 'r') as csv_reader:
            reader = csv.reader(csv_reader, delimiter=',')
            r = next(reader)
            rater = 0 
            for k in reader:
                if r[0] == "WorkerId":
                    b[rater] = [j for j in k[1:]]
                else:
                    b[rater] = [j for j in k]

                rater+=1


        name = i.split('.')[0]
        combine[name] = {}

        for j in a:
            if j in b.keys():
                combine[name][j] = list(zip(a[j],b[j]))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    for i in list(combine_csv.keys())[1]:
        fig.suptitle('Music: ' + i)
        data = np.array(combine_csv[i]).astype(float)
        ax.plot(data[:, 0], data[:, 1], linestyle='--', label='Average')
        user = 1
        for j in combine[i]:
            data_2 = np.array(combine[i][j]).astype(float)
            ax.plot(data_2[:, 0], data_2[:, 1], label='User '+ str(user)  )
            user+=1


    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_xlabel('Arousal')
    ax.set_ylabel('Valence')
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    ax.legend()
    plt.show()