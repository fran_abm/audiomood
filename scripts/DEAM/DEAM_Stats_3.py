import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to display the averaged VA per song.
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_averaged_per_song/song_level/'
    first_2000 = base_folder + 'static_annotations_averaged_songs_1_2000.csv'
    second_2000 = base_folder + 'static_annotations_averaged_songs_2000_2058.csv'

    song_dic = {}

    with open(first_2000,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:

            song_dic[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[3], 'arousal_std': i[4]}

    # The newer songs have more values.
    with open(second_2000,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:
            song_dic[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[7], 'arousal_std': i[8]}

    
    dataset = []
    for k, v in song_dic.items():
        dataset.append([v['valence_mean'], v['arousal_mean']])


    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    fig.suptitle('Valores totais do VA')


    data = np.array(dataset).astype(float)
    plt.scatter(data[:, 0], data[:, 1])

    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))
    ax.set_xlabel('Arousal')
    ax.set_ylabel('Valence')
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    ax.legend()
    plt.show()
    

