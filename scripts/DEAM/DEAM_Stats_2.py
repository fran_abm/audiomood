import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to show the average emotion per person
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_per_each_rater/dynamic/'
    arousal = base_folder + 'arousal/'
    valence = base_folder + 'valence/'
    base_folder_2 = '/home/quietswami/Documents/DEAM/annotations/annotations_averaged_per_song/song_level/'
    first_2000 = base_folder_2 + 'static_annotations_averaged_songs_1_2000.csv'
    second_2000 = base_folder_2 + 'static_annotations_averaged_songs_2000_2058.csv'

    arousal_dir = os.listdir(arousal)
    random.shuffle(arousal_dir)

    combine = {}
    song = ''
    for i in arousal_dir[:3]:
        song = i
        a = {}
        b = {}
        with open(arousal + i, 'r') as csv_reader:
            reader = csv.reader(csv_reader, delimiter=',')
            r = next(reader)
            rater = 0 
            for k in reader:
                if r[0] == "WorkerId":
                    a[rater] = [j for j in k[1:]]
                else:
                    a[rater] = [j for j in k]

                rater+=1

        with open(valence + i, 'r') as csv_reader:
            reader = csv.reader(csv_reader, delimiter=',')
            r = next(reader)
            rater = 0 
            for k in reader:
                if r[0] == "WorkerId":
                    b[rater] = [j for j in k[1:]]
                else:
                    b[rater] = [j for j in k]

                rater+=1


        name = i.split('.')[0]
        combine[name] = {}

        for j in a:
            combine[name][j] = list(zip(a[j],b[j]))
    print(combine.keys())

    # for i in combine[list(combine.keys())[0]]:
    song_dic = {}

    with open(first_2000,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:

            song_dic[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[3], 'arousal_std': i[4]}

    # The newer songs have more values.
    with open(second_2000,'r') as csv_reader:
        reader = csv.reader(csv_reader)
        header = next(reader)
        for i in reader:
            song_dic[i[0]] = {'valence_mean': i[1], 'valence_std': i[2], 'arousal_mean': i[7], 'arousal_std': i[8]}

    
    # dataset = []
    # for k, v in song_dic.items():
    #     dataset.append([v['valence_mean'], v['arousal_mean']])

    # data = np.array(dataset)
    # plt.scatter(data[:, 0], data[:, 1])
    # plt.show()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    array = []
    for i in combine[list(combine.keys())[0]]:
        data = np.array(combine[list(combine.keys())[0]][i]).astype(float)
        print(data)
        ax.plot(data[:, 0], data[:, 1])

    # print(list(combine.keys())[0])
    # print(song_dic[list(combine.keys())[0]])
    # ax.scatter(song_dic[list(combine.keys())[0]]['valence_mean'], song_dic[list(combine.keys())[0]]['arousal_mean'])
    ax.xaxis.set_major_locator(plt.MaxNLocator(4))
    ax.yaxis.set_major_locator(plt.MaxNLocator(4))

    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    plt.show()