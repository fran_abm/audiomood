import csv
import os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import numpy as np
import random

#
# Script to show the average emotion per music
#

if __name__ == "__main__":
    base_folder = '/home/quietswami/Documents/DEAM/annotations/annotations_averaged_per_song/dynamic/'
    arousal_csv = base_folder + 'arousal.csv'
    valence_csv = base_folder + 'valence.csv'

    arousal = {}
    valence = {}
    song_dic = {}

    with open(arousal_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            arousal[i[0]] = [j for j in i[1:]]

    with open(valence_csv, 'r') as csv_reader:
        reader = csv.reader(csv_reader, delimiter=',')
        next(reader)
        for i in reader:
            valence[i[0]] = [j for j in i[1:]]
    
    for i in arousal.keys():
        song_dic[i] = list(zip(arousal[i], valence[i]))

    keys = list(song_dic.keys())
    print(type(keys))
    # random.shuffle(keys)
    print(keys)
    
    
    for i in keys[:3]:
        data = np.array(song_dic[i])
        plt.plot(data[:, 0], data[:, 1])
    plt.axhline(y=0, color='k')
    plt.axvline(x=0, color='k')
    plt.show()