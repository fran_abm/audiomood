import librosa
import scipy
import os
from pydub import AudioSegment
import numpy as np
import datetime

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    base_folder = '/home/quietswami/Documents/EMOMUSIC/clips_45seconds/'
    musics = os.listdir(base_folder)
    for k, i in enumerate(musics[:3]):
        for j in np.arange(15, 45, 0.5):
            path = 'Audio/' + str(k) + '-' + str(j) + '.wav'
            audio, sampRate = librosa.core.load(base_folder + i, sr=16000, offset=j, duration=1)
            librosa.output.write_wav(path, audio, sampRate)

    end_time = datetime.datetime.now()
    elapsed_time = end_time - start_time

    print('Time Elapsed: ', str(elapsed_time))