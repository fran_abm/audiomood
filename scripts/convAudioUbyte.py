#
# This script converts a given audio file, to an ubyte file.
# Also, it takes care of rectifying the values to the grey scale.
#

import sys, os
sys.path.insert(0, os.path.abspath('..'))

from display import Display
from sys import argv
from Audiomood import Conversion


def getFilename(f):
    print(f)
    return os.path.splitext(os.path.basename(f))


def getUbyteName(f):
    print(f)
    return f[0] + "-data-idx3-ubyte"

if __name__ == "__main__":
    debug = argv[1]
    input = argv[2]
    output = argv[3]
    print(argv)


    flags = {}

    flags["debug"] = bool(debug)

    if flags["debug"]:
        Display.debugMessage("convAudioUbyte", "", "Starting Conversion")

    if flags["debug"]:
        Display.debugMessage("convAudioUbyte", "", "Creating Folder...")

    os.mkdir(output)

    if flags["debug"]:
        Display.debugMessage("convAudioUbyte", "", "Folder Created...")

    if os.path.isdir(input):
        if flags["debug"]:
            Display.debugMessage("convAudioUbyte", "", "Input is a Folder!")
        allAudioName = os.listdir(input)

        for i in allAudioName:
            Conversion.audioToUbyte(input+ i, output+getUbyteName(getFilename(i)), 10, True)
    else:
        if flags["debug"]:
            Display.debugMessage("convAudioUbyte", "", "Input is a File!")

        Conversion.audioToUbyte(input, output+getUbyteName(getFilename(input)), 40, True)

