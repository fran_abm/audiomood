#
# This library stores all the functions that have a chain of conversion
#  e.g: Audio -> Ubyte 
#
import sys, os
sys.path.insert(0, os.path.abspath('..'))

from display import Display
from Audiomood import Audio
from Audiomood import Ubyte
from Audiomood import Spectogram
from Audiomood import Header


def audioToSpec(input=None, nFTT=1024, nMels=128, hop=512, debug=False):
    """
    This function is responsible for return the spectogram of a given sound.
    The spectogram can be customizable, by changing the arguments.

    Args:
        - input: a string, with the file path of the input audio.
        - nFTT: a int, the number of FTT required.
        - nMels: a int, the number of melodies requires.
        - hop: a int, the size of the window hop (or stride of the window).

    Returns:
        - An array containg the spectogram.
    """
    if debug:
        Display.debugMessage("Conversion", "audioToSpec", "Starting conversion.")

    if input==None:
        Display.errorMessage("Conversion", "audioToSpec", "No input file given. Not converting.")
        return None
    else:
        a1 = Audio.Audio(debug=debug)
        a1.setAudio(input)
        spec = a1.createSpectogram(nFTT=nFTT, hop=hop, nMels=nMels)
        spec.createSpectogram()
        return spec



def audioToUbyte(input=None, output=None, nMFCC=10, debug=False):
    """
    This function takes a audio file, and outputs a ubyte file.

    Args: 
        - input: a string, the filepath of the audio file.
        - output: a string, the filepath of the ubyte file to be saved.
    
    Returns:
        - The Ubyte object
    """
    if debug:
        Display.debugMessage("Conversion", "audioToUbyte", "Starting conversion.")

    if input==None:
        Display.errorMessage("Conversion", "audioToUbyte", "No input file given. Not converting.")
        return None
    else:
        a1 = Audio.Audio(debug=debug)
        a1.setAudio(input)
        spec = a1.createSpectogram(nMFCC)
        spec.createSpectogram()
        print(spec.spectogram.shape)
        print(spec.spectogram.max())
        print(spec.spectogram.min())
        recVal = spec.rectifyValues(0, 255)
        nLines = len(recVal)
        nCols = len(recVal[0])
        print("Number of Lines: ", len(recVal))
        print("Number of Columns: ", len(recVal[0]))
        spec.getSpecImage()
        ub = Ubyte.Ubyte()
        hd = Header.Header()
        hd.setMagic(2051)
        hd.setNumOfRows(nLines)
        hd.setNumOfCols(nCols)
        ub.setHeader(hd)
        ub.setData(recVal)
        filepath = "audiotest-data-idx3-ubyte" if output == None else output
        print(filepath)
        ub.saveFile(filepath)
        return ub






if __name__ == "__main__":
    audioToUbyte("test.wav", debug=True)