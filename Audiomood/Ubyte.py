#
# Ubyte object
# 

# t10k-images-idx3-ubyte
# t10k-labels-idx1-ubyte


# This is here to be able to test the functions.
# This can be commented when a stable version is done.
import sys, os
sys.path.insert(0, os.path.abspath('..'))


import gzip
from Audiomood import Header
from display import Display
from converter import Image

import numpy as np

class Ubyte:
    """
    Ubyte class, used to create valid ubyte files.
    
    The Ubyte object has two distinct fields:
        - Header: has the header data. 
        - Data: has the object data.
    This will be used to make easier the ubyte creation.
    """

    def __init__(self, debug=False, t="data"):
        self.header = []
        self.data = []
        self.type = t
        self.debug = debug
        if self.debug:
            Display.debugMessage("Ubyte", "__init__", "Created Ubyte object.")


    def merge(self, ubyte):
        """
        Merges two ubyte objects.

        Args:
            - ubyte: the ubyte object to be merged.
        
        TODO:
            - check if the objects in the ubyte are the same size (line and column). 
            - Add the IDs of the objects to the messages.
            - Finish adding 
        """
        
        if self.debug:
            Display.debugMessage("Ubyte", "merge", "Merge two Ubyte objects")

        # First we need to change the number of items in the header
        self.header.setNumOfItems(self.header.getNumOfItems() + ubyte.header.getNumOfItems())

        # Then, we merge both data lists
        self.setData(self.data + ubyte.data)


    def parseFile(self, f, t):
        """
        This function is responsible for parsing a file to the object.
        It reads the file and fills the object.

        Args:
            - f: the file name of the ubyte file to be read.
            - t: the type of the file. Either "data" or "label"
        """
        
        if self.debug:
            Display.debugMessage("Ubyte", "parseFile", "Parsing " + f + " into a Ubyte object. File type: " + t)

        self.type = t
        self.header = Header.Header()

        # Always open the file as read+byte. Because the ubyte files is writen in bytes
        # Python might not read the bytes if not in this setting.
        # TODO: Check if files are gziped, and open them that way.
        with open(f, "rb") as o:

            # The first 4 bytes are for the magic number.
            # Currently we don't know if the magic number has any significance.
            magic = o.read(4)
            magicC = self.bytes2int(magic)

            if self.debug:
                Display.debugMessage("Ubyte", "parseFile", "Magic Number before conversion: " + str(magic))
            
            self.header.setMagic(magicC)

            if self.debug:
                Display.debugMessage("Ubyte", "parseFile", "Magic Number: " + str(magicC))

            # Beacause the first 8 bytes of the file mean the same thing in both types of files
            #   we can read and set the headers without knowing the type.
            numOfItems = self.bytes2int(o.read(4))
            self.header.setNumOfItems(numOfItems)

            if self.debug:
                Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfItems))

            self.header.type = t

            # This part of the header is only for the data files.
            if self.type == "data":
                numOfRows = self.bytes2int(o.read(4))
                self.header.setNumOfRows(numOfRows)
                if self.debug:
                    Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfRows))

                numOfCols = self.bytes2int(o.read(4))
                self.header.setNumOfCols(numOfCols)
                if self.debug:
                    Display.debugMessage("Ubyte", "parseFile", "Number of Items: " + str(numOfCols))


            # This routine might be optimized.
            # We are reading until there is no more to read.
            # TODO: Check if it can be broken.
            n = True
            if self.debug:
                Display.debugMessage("Ubyte", "parseFile", "Starting data collection...")
            

            # This will be used to limit the iterations. 
            it = 0

            if self.header.getType() == "data":
                # If the type is data, we need to account for the number of pixels in all the images
                it = self.header.getNumOfItems() * self.header.getNumOfRows() * self.header.getNumOfCols()
            else:
                # If the type is labels, we only need to know how many items are there.
                it = self.header.getNumOfItems()
            
            for i in range(it):
                try:
                    a = self.bytes2int(o.read(1))
                    self.data.append(a)
                except:
                    Display.errorMessage("Ubyte", "parseFile", "Unable to read byte.")
            if self.debug:
                Display.debugMessage("Ubyte", "parseFile", "Finished data collection.")
                Display.debugMessage("Ubyte", "parseFile", "Number of data points: " + str(len(self.data)))


    def int2bytes(self, i, length=None):
        """
        Converts a int to a byte.
        It is set to big endian as it is the format used in the 
            MNIST dataset.

        Args:
            - i: the number to be converted to a byte, as an int.

        Return:
            - the number converted to a byte.
        """
        if length == None:
            return i.to_bytes((i.bit_length() + 7) // 8, 'big')
        else:
            return i.to_bytes(length, "big")


    def bytes2int(self, i):
        """
        Converts bytes to int. It is set to big endian, as it is
            the format used in the MNIST dataset.

        Args:
            i: the number, in bytes, to be converted.

        Returns:
            - the converted number as an int.
        """
        return int.from_bytes(i, "big")


    def saveFile(self, filename):
        """
        Writes the object to a file.
        Used to create ubyte files.

        Args:
            - filename: the file name of the desired saved file.

        """
        if self.debug:
            Display.debugMessage("Ubyte", "saveFile", "Starting saving file with name: " + filename)

        with gzip.open(filename + ".gz", "wb") as dataFile:
            magic = self.header.getMagic()
            magicC = self.int2bytes(magic, 4)
            magicD = self.bytes2int(magicC)

            if self.debug:
                Display.debugMessage("Ubyte", "saveFile", "Magic before conversion: " + str(magic))
                Display.debugMessage("Ubyte", "saveFile", "Magic after conversion: " + str(magicC))
                Display.debugMessage("Ubyte", "saveFile", "Magic after deconversion: " + str(magicD))

            if magicD == magic:
                dataFile.write(magicC) # The first number is always the magic number
            else: # safety procedure...
                Display.errorMessage("Ubyte", "saveFile", "Magic number conversion wrong.")

            # Now we need to differenciate between data files and label files.
            if self.header.getType() == "data":

                nI = self.header.getNumOfItems()
                nIC = self.int2bytes(nI, 4)
                nID = self.bytes2int(nIC)

                if self.debug:
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items before conversion: " + str(nI))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items conversion: " + str(nIC))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items deconversion: " + str(nID))


                nR = self.header.getNumOfRows()
                nRC = self.int2bytes(nR,4)
                nRD = self.bytes2int(nRC)

                if self.debug:
                    Display.debugMessage("Ubyte", "saveFile", "Num of Rows before conversion: " + str(nR))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Rows after conversion: " + str(nRC))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Rows after deconversion: " + str(nRD))

                nC = self.header.getNumOfCols()
                nCC = self.int2bytes(nC,4)
                nCD = self.bytes2int(nCC)

                if self.debug:
                    Display.debugMessage("Ubyte", "saveFile", "Num of Cols before conversion: " + str(nC))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Cols after conversion: " + str(nCC))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Cols after deconversion: " + str(nCD))

                dataFile.write(nIC) # Write the number of Items
                dataFile.write(nRC) # Write the number of rows in a image
                dataFile.write(nCC) # Write the number of cols in a image
            else:

                nI = self.header.getNumOfItems()
                nIC = self.int2bytes(nI,4)
                nID = self.bytes2int(nIC)

                if self.debug:
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items before conversion: " + str(nI))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items conversion: " + str(nIC))
                    Display.debugMessage("Ubyte", "saveFile", "Num of Items deconversion: " + str(nID))

                dataFile.write(nIC) # Write the number of items
            
            # Now we write the data to the file
            # We are writing in the sequence defined in the MNIST dataset specifications.
            bArray = bytearray(self.data)   
            dataFile.write(bArray)
            # for i in self.data:
            #     dataFile.write(self.int2bytes(i))


    def separate(self):
        """
        Used to separate the data into the images.
        Uses the lines and the columns in the header to do so.
        Only to be used with the data dataset.

        Returns:
            - The dataset separated in to images.

        TODO:
            - Check if the dataset is for data and not for labels
            - Check if it works
        """
        index = 0
        ret = []
        for i in range(self.header.getNumOfItems()):
            for j in range(self.header.getNumOfRows()):
                li = []
                for k in range(self.header.getNumOfCols()):
                    li.append(self.data[index])
                    index=+1
                ret.append(li)


    def getHeader(self):
        """
        Returns the header of the ubyte file/object

        Returns:
            the header of the ubyte object.
        """
        if self.debug:
            Display.debugMessage("Ubyte", "getHeader", "Get Header: " + str(self.header))
        return self.header

    
    def setHeader(self, header):
        """
        Sets the header of the ubyte object

        Args:
            header: a list with the header. 
        """
        if self.debug:
            Display.debugMessage("Ubyte", "setHeader", "Set Header: " + str(header.toString()))
        self.header = header
    

    def getData(self):
        """
        Returns the ubyte data.

        Returns:
            The ubyte data as a list.
        """
        if self.debug:
            Display.debugMessage("Ubyte", "getData", "Get Data: ")
        return self.data


    def setData(self, data):
        """
        Sets the data on the ubyte file/object
        
        Args:
            - data: the data being set, as a list.
        """
        if self.debug:
            Display.debugMessage("Ubyte", "setData", "Set Data")
        self.data = data


    def getType(self):
        """
        Returns the type of the ubyte file/object, whether is a data file or a label file

        Returns:
            - A string containing the type: "data" or "label"
        """
        if self.debug:
            Display.debugMessage("Ubyte", "setType", "Get Type: " + str(self.type))
        return self.type
    

    def setType(self, t):
        """
        Sets the type of the ubyte file/object

        Args:
            - t: a string, "data" or "label"
        """
        if self.debug:
            Display.debugMessage("Ubyte", "setType", "Set Type: " + str(t))
        self.type = t


    def copy(self, ubyte):
        """
        Copies the info of another ubyte object to its own.
        Useful when testing

        Args:
            - ubyte: the ubyte object to be copied.
        
        TODO:
            - Add objects ID to the debug
        """
        if self.debug:
            Display.debugMessage("Ubyte", "copy", "Copying Ubyte Object.")

        self.setData(ubyte.getData())
        self.setHeader(ubyte.getHeader())


    def to_string(self):
        """
        Returns the information of the Ubyte object as a string.

        Returns:
            - The object as a string.
        """
        return []


    def ubyteToImage(self):
        """
        Transforms the ubyte object into an Image object.

        Returns:
            - A Image object containing the Ubyte transformed into an image.
        """
        two_d = (np.reshape(self.data, (self.header.getNumOfCols(), self.header.getNumOfRows()))).astype(np.uint8)
    
        # Then we convert it to an image. The argument "L" is the type of color scheme 
        #   of the image. In this case it is greyscale.
        # FUTURE: This might be changed in the future, if we need to add color.
        img = Image.fromarray(two_d, 'L')
        image = Image()
        image.setFile = img
        
        return img


if __name__ == "__main__":
    # Initializing the ubyte object with debug true
    ub1 = Ubyte(True)
    # Testing parseFile
    # ub1.parseFile("../datasets/t10k-images-idx3-ubyte", "data")
    
    print(ub1.int2bytes(2051, 4))

    # ub1.saveFile("teste2")
    # Creating a second object to be able to test a couple functions.
    # ub2 = Ubyte(True)
    # # Testing Copy
    # ub2.copy(ub1)



    # if ub1.data == ub2.data and ub1.header == ub2.header:
    #     Display.testingMessage("Ubyte", "2nd Ubyte Num of Items", str(ub2.header.getNumOfItems()))
    #     Display.testingMessage("Ubyte", "2nd Ubyte Num of col", str(ub2.header.getNumOfCols()))
    #     Display.testingMessage("Ubyte", "2nd Ubyte Num of lines", str(ub2.header.getNumOfRows()))
    #     Display.testingMessage("Ubyte", "copy", "Copy Is True")

    # ub1.merge(ub2)
    # Display.testingMessage("Ubyte", "Merge: Num of Items", str(ub1.header.getNumOfItems()))
    # Display.testingMessage("Ubyte", "Merge: Num of elements", str(len(ub1.data)))

    # # Testing save file
    # ub2.saveFile("teste-idx3-ubyte")

    # ub3 = Ubyte(True)
    # ub3.parseFile("teste-idx3-ubyte", "data")
    
    # if ub3.data == ub2.data:
    #     Display.testingMessage("Ubyte", "save", "Save Is True")

    

    