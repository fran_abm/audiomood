#
# Audio object, with all the functions to upload and convert audio tracks.

#
# Author: Francisco Mendonça
#
import sys, os
sys.path.insert(0, os.path.abspath('..'))

from display import Display

from Audiomood import Spectogram

import librosa


class Audio:

    def __init__(self, debug=False, audio=None, ft=None, sampRate=0):
        self.debug = debug
        self.audio = audio
        self.filetype = ft
        self.sampRate = sampRate
        if self.debug:
            Display.debugMessage("Audio", "init", "Starting new Audio object. ")



    # GETTER AND SETTERS

    def getAudio(self):
        """
        This function will return the audio that is read.

        Returns:
            - The read audio, or None if the object has no audio uploaded.
        """
        return self.audio


    def setAudio(self, audio, offset=None, duration=None):
        """
        Sets the audio in the object.

        Args:
            - audio, the audio to be uploaded to the object.

        TODO: Decide if it has to be an already opened audio, or if it can be a string.
        """
        if type(audio) == type(""):
            # Opening procedure.
            if offset != None and duration != None:
                self.audio, self.sampRate = librosa.core.load(audio, offset=offset, duration=duration)
            else:
                self.audio, self.sampRate = librosa.core.load(audio)
        else:
            self.audio = audio


    def setFiletype(self, ft):
        """
        Sets a new file type, if there is no filetype yet set.

        Args:
            - ft: the filetype, as a string.
        """
        if self.debug:
            Display.debugMessage("Audio", "setFiletype", "Setting new filetype.")

        if self.filetype == None:
            self.filetype = ft
            if self.debug:
                Display.debugMessage("Audio", "setFiletype", "Filetype was added correctly.")
        else:
            Display.errorMessage("Audio", "setFiletype", "Filetype already added! Not changing.")



    def getFiletype(self):
        """
        Returns the opened file's type.

        Returns:
            - the opened file's type.
        """
        if self.debug:
            Display.debugMessage("Audio", "getFiletype", "Getting the opened filetype.")

        # We need to check if there is a file added.
        # If not we will return as None.
        # We need to check, when we use this function, whether the file is added or not.
        if self.file != None:
            if self.debug:
                Display.debugMessage("Audio", "getFiletype", "File was added. Returning filetype.")
            return self.filetype
        else:
            if self.file == None:
                Display.errorMessage("Audio", "getFiletype", "No file added...")
                return None
            else:
                Display.errorMessage("Audio", "getFiletype", "File was added, but \
                    no filetype was added.")
                return None



    def saveWav(self, filename):
        """
        Saves the audio track that was converted into a wav.

        Args:
            - filename, a string that contains the path of the saved wav file.
        """

        if self.debug:
            Display.debugMessage("Audio", "saveWav", "Saving wav file.")

        if self.audio != None and self.sampRate != 0:
            librosa.output.write_wav(filename, self.audio, self.sampRate)
            if self.debug:
                Display.debugMessage("Audio", "saveWav", "Saving successful.")
        else:
            if self.debug:
                Display.debugMessage("Audio", "saveWav", "Not able to save.")


    def createSpectogram(self, nFTT=1024, nMels=128, hop=512):
        """
        This function will create and return a spectogram object, with the loaded audio clip.

        Returns:
            - a Spectogram object, with the loaded audio clip.
        """
        if self.debug:
            Display.debugMessage("Audio", "createSpectogram", "Creating a Spectogram")
        return Spectogram.Spectogram(self.audio, self.sampRate, nFTT, hop, nMels, self.debug)
