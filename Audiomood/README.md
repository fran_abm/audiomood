# Audiomood Library
This folder will contain a series of functions that will create a library. This will allow for a faster integration 
of many functions that were dispersed through several folders.
The following libraries are in order of expected use. An audio file will be transformed to a spectrogram, and later turned 
into a Ubyte object.

## Audio
The Audio library consists of an object that opens the audio file, and has all of the needed tools to manipulate the tracks
in this project.

## Spectrogram
The Spectogram object is responsible for creating the spectrogram that later will be made into a Ubyte object, and a Ubyte file. With this object we are able to create the spectrogram, rectify the values, and get an image of the spectrogram.

## Ubyte
The Ubyte object creates and parses Ubyte files. This, coupled with the Header object, has all the functions needed to read,
write, and manipulate Ubyte files.

## Conversion
This library has all the functions that convert from an object to another. 