#
# Header object of the Ubyte file
#
import sys, os
sys.path.insert(0, os.path.abspath('..'))



from display import Display

class Header:

    def __init__(self, debug=False, t="data"):
        self.type = t
        self.numOfItems = 0
        self.magic = 2051
        self.debug = debug
        if self.debug:
            Display.debugMessage("Header ", "setMagic", "Header Initialized")
    

    def setMagic(self, num):
        """
        Sets the magic number of the file.

        Args:
            - num: the magic number of the Ubyte object.
        """
        if self.debug:
            Display.debugMessage("Header ", "setMagic", "Set Magic Number: " + str(num))

        self.magic = num


    def getMagic(self):
        """
        Returns the magic number of the Ubyte file.

        Returns:
            - the magic number of the Ubyte file.
        """
        return self.magic


    def setNumOfRows(self, num):
        """
        Sets the number of rows, per image, in the Ubyte object.

        Args:
            - num: the number of rows, per image, in the Ubyte object. Must be a valid integer.
        """
        if self.debug:
            Display.debugMessage("Header ", "setNumOfRows", "Set Number of Rows: " + str(num))
        
        if isinstance(num, int):
            self.numOfRows = num
        else:
            Display.errorMessage("Header", "setNumOfRows", "Invalid integer: " + str(num))

        return "The object has no number of rows set."

    def getNumOfRows(self):
        """
        Returns the number of rows (lines), per image, in the Ubyte object.

        Returns:
            - The number of rows, per image, in the Ubyte object.
        """
        if self.debug:
            if hasattr(self, 'getNumOfRows') or self.type == "data":
                Display.debugMessage("Header ", "getNumOfRows", "Get Number of Rows: " + str(self.getNumOfRows))
            else:
                Display.debugMessage("Header", "getNumOfRows", "The object has no number of rows set")

        if hasattr(self, 'getNumOfRows'):
            return self.numOfRows
        else:
            Display.errorMessage("Header", "getNumOfRows", "The object has no number of Rows set.")
            return "The object has no number of rows set."

    def setNumOfCols(self, num):
        if self.debug:
            Display.debugMessage("Header ", "setNumOfCols", "Set Number of Columns: " + str(num))
        
        if isinstance(num, int):
            self.numOfCols = num
        else:
            Display.errorMessage("Header", "setNumOfCols", "Invalid integer: " + str(num))


    def getNumOfCols(self):
        """
        Returns the number of columns, by image, in the Ubyte object.

        Returns:
            - The number of columns in the Ubyte object.
        """
        if self.debug:
            if hasattr(self, 'getNumOfCols') or self.type == "data":
                Display.debugMessage("Header ", "getNumOfCols", "Get Number of Columns: " + str(self.getNumOfCols))
            else:
                Display.debugMessage("Header", "getNumOfCols", "The object has no number of columns set")

        if hasattr(self, 'getNumOfCols'):
            return self.numOfCols
        else:
            Display.errorMessage("Header", "getNumOfCols", "The object has no number of columns set.")
            return "The object has no number of columns set."


    def setType(self, t):
        """
        Sets the type of the Ubyte object.

        Args:
            - t: the type of the Ubyte object. One of two possible strings:
                "data" or "label"
        """
        if self.debug:
            Display.debugMessage("Header", "setType", "Trying to set type. Type: " + str(t))
        
        if str(t) != "data" and str(t) != "labels":
            Display.errorMessage("Header", "setType", "Error setting type. Invalid type: " + str(t))
        else:
            self.type = t


    def getType(self):
        """
        Returns the type of the Ubyte object.

        Returns:
            - The type of the Ubyte object.
        """
        if self.debug:
            Display.debugMessage("Header", "getType", "Get Type: " + str(self.type))
        return self.type


    def setNumOfItems(self, num):
        """
        Sets the number of items in the Ubyte object

        Args:
            - num: the number of items in the Ubyte object
        """
        if self.debug:
            Display.debugMessage("Header", "setNumOfItems", "Trying to set number of items. Num: " + str(num))
        
        if isinstance(num, int):
            self.numOfItems = num
        else:
            Display.errorMessage("Header", "setNumOfItems", "Failed setting number. " + \
                str(num) +  " is not a valid integer.")
        

    def getNumOfItems(self):
        """
        Returns the number of items in the Ubyte object.

        Returns:
            - The number of items in the Ubyte object.
        """
        if self.debug:
            Display.debugMessage("Header", "getNumOfItems", "Get Number of Items: " + str(self.numOfItems))
        return self.numOfItems


    def toString(self):
        """
        Returns the information of the Header object as a string.

        Returns:
            - The object as a string.
        """
        if self.type == "data":
            return "Type: " + self.type + ", Num. of Items: " + str(self.numOfItems) + \
                ", Num. of Lines: " + str(self.numOfRows) + ", Num. of Columns: " + str(self.numOfCols) 
        else:
            return "Type: " + self.type + ", Num. of Items: " + str(self.numOfItems) 


if __name__ == "__main__":
    h = Header()
    print(type(h))