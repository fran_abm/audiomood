#
# Spectogram object, with all the functions to create and convert spectograms.

#
# Author: Francisco Mendonça
#

import sys, os
sys.path.insert(0, os.path.abspath('..'))

import numpy as np
from display import Display
import matplotlib.pyplot as plt
import librosa
import librosa.display

class Spectogram:

    def __init__(self, audio=None, sampRate=22050, nFTT=1024, hop=512, mels=128, debug=False):
        self.audio = audio
        self.sampRate = sampRate
        self.nFTT = nFTT
        self.hop = hop
        self.mels = mels
        self.spectogram = None
        self.debug = debug
        if self.debug:
            Display.debugMessage("Spectogram", "init", "Spectogram Created.")


    def createSpectogram(self):
        """
        This function is responsible for creating the spectogram.

        TODO: Add try and catch spectogram creation.
        """
        if self.debug:
            Display.debugMessage("Spectogram", "createSpectogram", "Trying to create the Spectogram.")

        #self.spectogram = librosa.feature.mfcc(self.audio, self.sampRate, n_mfcc=128)
        self.spectogram = librosa.feature.melspectrogram(self.audio, self.sampRate, n_fft=self.nFTT, hop_length=self.hop, n_mels=self.mels)

    def getSpectogram(self):
        """
        Returns spectogram.
        """
        if self.debug:
            Display.debugMessage("Spectogram", "getSpectogram", "Getting spectogram.")
        return self.spectogram


    def setSpectogram(self, spectogram):
        """
        Sets the spectogram, if it was created outside of this object.

        Args:
            - spectogram, a created spectogram.
        """
        if self.debug:
            Display.debugMessage("Spectogram", "setSpectogram", "Setting the spectogram.")
        self.spectogram = spectogram


    def rectifyValues(self, low, high):
        """
        Normally, the values created in the spectogram are very small floats.
        This is not optimal for a CNN, so we need to rectify the output values.
        Because CNNs are optimize for images, we will constrain the values of the output between 0 and 255,
            which is the values on the greyscale.

        Args:
            - low, a int, the smallest value in the rectified spectogram.
            - high, a int, the largest value in the rectified spectogram.

        Returns:
            - a list of lists, with the spectogram rectified.
        """
        if len(self.spectogram) == 0:
            return None
        else:
            minimum = self.spectogram.min()
            maximum = self.spectogram.max()
            alpha = 0

            if minimum != maximum:
                alpha = (low - high)/(minimum - maximum)

            beta = low - alpha*minimum
            rectified = self.spectogram * alpha + beta
            return rectified


    def getSpecImage(self):
        """
        This function shows the image created by the spectogram.
        """
        plt.figure(figsize=(10, 4))
        librosa.display.specshow(librosa.power_to_db(self.spectogram,ref=np.max),y_axis='mel', fmax=8000,x_axis='time')
        plt.colorbar(format='%+2.0f dB')
        plt.title('Mel spectrogram')
        plt.tight_layout()
        plt.show()
