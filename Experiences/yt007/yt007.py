import sys as sys
sys.path = (sys.path + [('/home/hpc/tl/' + 'PointCentral/python/')])
import tldatasets as tld
import json as json
import numpy as np
import time as time
import math as math
import socket as socket
import os as os
import csv as csv
import resource as resource
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

#
# Setup
#

dataset = tld.read_data_sets(train_dir='./',
    train_images='audioset_train_data_v4-ubyte.gz',
    train_labels='audioset_train_labels_v4-ubyte.gz',
    validation_images='audioset_test_data_v4-ubyte.gz',
    validation_labels='audioset_test_labels_v4-ubyte.gz',
    test_images=None, test_labels=None, nbclasses=2, one_hot=True,
    imageOnOutput=False, reshape=True, seed=11, source_url='http://tl.di.fc.ul.pt/s/TF/data/')

nPatterns = dataset.train.nPatterns

expname = 'yt007'

with open((expname + '.json'), 'r') as f:
    expstatus = json.loads(f.read())

expstatus['status'] = 'running'

if not ('globalEpochs' in expstatus):
    expstatus['globalEpochs'] = 0

startEpochs = expstatus['globalEpochs']
firstRun = (startEpochs == 0)

print (('global Epochs: ' + str(expstatus['globalEpochs'])))
print (('firstRun:' + str(firstRun)))

nbEpochs = (startEpochs + 100)
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()

#
# Functions
#

def weight_variable (shape, name, seed, freeze):
    file = (name + '.npy')
    if os.path.isfile(file):
        init = tf.constant(np.load(file))
    else:
        init = tf.truncated_normal(shape, seed=seed, stddev=0.1)
    if freeze:
        return tf.get_variable(name, initializer=init, trainable=False)
    else:
        return tf.get_variable(name, initializer=init)

def bias_variable (shape, name, freeze):
    file = (name + '.npy')
    if os.path.isfile(file):
        init = tf.constant(np.load(file))
    else:
        init = tf.constant(0.1, shape=shape)
    if freeze:
        return tf.get_variable(name, initializer=init, trainable=False)
    else:
        return tf.get_variable(name, initializer=init)

def conv2d (x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def conv3d (x, W):
    return tf.nn.conv3d(x, W, strides=[1, 1, 1, 1, 1], padding='SAME')

def getAccuracy (set, sess):
    print ('..................................................................')
    accuracy = 0
    nPatterns = set.nPatterns
    set.resetIndex()
    n = int(np.ceil((nPatterns / 32)))
    while not set.epochCompleted:
        batch = set.next_batch(32, shuffle=False, circular=False)
        accuracy = (accuracy + n_corrects.eval(feed_dict={x:batch[0], y_:batch[1]}, session=sess))
    accuracy = (accuracy / nPatterns)
    print ('..................................................................')
    return accuracy

x = tf.placeholder(tf.float32, shape=[None, 163840], name='x')
y_ = tf.placeholder(tf.float32, shape=[None, 2], name='y_')
_idx = tf.placeholder(tf.int32, shape=[None, 1], name='idx')

x_row = x
x_image = tf.reshape(x_row, [-1, 512, 320, 1])

w1 = weight_variable([3, 3, 1, 64], 'W1', 20, False)
b1 = bias_variable([64], 'B1', False)
y1 = (conv2d(x_image, w1) + b1)
a1 = tf.nn.relu(y1)

w2 = weight_variable([3, 3, 64, 64], 'W2', 20, False)
b2 = bias_variable([64], 'B2', False)
y2 = (conv2d(a1, w2) + b2)
a2 = tf.nn.relu(y2)

mp3 = tf.nn.max_pool(a2, ksize=[1, 8, 2, 1], strides=[1, 8, 2, 1], padding='SAME')
w3 = weight_variable([3, 3, 64, 128], 'W3', 20, False)
b3 = bias_variable([128], 'B3', False)
y3 = (conv2d(mp3, w3) + b3)
a3 = tf.nn.relu(y3)

w4 = weight_variable([3, 3, 128, 128], 'W4', 20, False)
b4 = bias_variable([128], 'B4', False)
y4 = (conv2d(a3, w4) + b4)
a4 = tf.nn.relu(y4)

mp5 = tf.nn.max_pool(a4, ksize=[1, 4, 4, 1], strides=[1, 4, 4, 1], padding='SAME')
w5 = weight_variable([3, 3, 128, 256], 'W5', 20, False)
b5 = bias_variable([256], 'B5', False)
y5 = (conv2d(mp5, w5) + b5)
a5 = tf.nn.relu(y5)

w6 = weight_variable([3, 3, 256, 256], 'W6', 20, False)
b6 = bias_variable([256], 'B6', False)
y6 = (conv2d(a5, w6) + b6)
a6 = tf.nn.relu(y6)

mp7 = tf.nn.max_pool(a6, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
w7 = weight_variable([3, 3, 256, 256], 'W7', 20, False)
b7 = bias_variable([256], 'B7', False)
y7 = (conv2d(mp7, w7) + b7)
a7 = tf.nn.relu(y7)

w8 = weight_variable([3, 3, 256, 256], 'W8', 20, False)
b8 = bias_variable([256], 'B8', False)
y8 = (conv2d(a7, w8) + b8)
a8 = tf.nn.relu(y8)

mp9 = tf.nn.max_pool(a8, ksize=[1, 8, 10, 1], strides=[1, 8, 10, 1], padding='SAME')
w9 = weight_variable([512, 2048], 'W9', 20, False)
b9 = bias_variable([2048], 'B9', False)
in_flat9 = tf.reshape(mp9, [-1, 512])
y9 = (tf.matmul(in_flat9, w9) + b9)
a9 = tf.nn.relu(y9)

w10 = weight_variable([2048, 1024], 'W10', 20, False)
b10 = bias_variable([1024], 'B10', False)
y10 = (tf.matmul(a9, w10) + b10)
a10 = tf.nn.relu(y10)

w11 = weight_variable([1024, 2], 'W11', 20, False)
b11 = bias_variable([2], 'B11', False)
y11 = (tf.matmul(a10, w11) + b11)

netOutput = tf.identity(y11, name='NETOUTPUT')
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=netOutput, name='COST'))
optimizer = tf.train.AdamOptimizer(1.0e-4)
train_step = optimizer.minimize(cost)
just_class_number = tf.argmax(y11, axis=1, name='class_number')
correct_prediction = tf.equal(just_class_number, tf.argmax(y_, 1))
n_corrects = tf.reduce_sum(tf.cast(correct_prediction, tf.float32))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')
get_bads = tf.boolean_mask(tf.concat([_idx, tf.reshape(tf.cast(tf.argmax(netOutput, axis=1), tf.int32), [-1, 1])], 1), tf.logical_not(correct_prediction), name='get_bads')

init = tf.global_variables_initializer()
if firstRun:
    results = {'nb-epochs':100, 'duration':0, 'optimizer':'ADAM-OPTIMIZER', 'cost-function':'SOFTMAX-CROSS-ENTROPY-WITH-LOGITS', 'training-accuracy':[], 'final-validation-accuracy':0}
else:
    with open(expname + '-results.json', 'r') as f:
        results = json.loads(f.read())

start_time = time.time()
with tf.Session() as sess:
    tf.set_random_seed(11)
    sess.run(init)
    pat = 0
    epoch = startEpochs
    stop = False
    if firstRun:
        allValidationAccuracies = np.empty(shape=[0, 2])
        allTrainingAccuracies = np.empty(shape=[0, 2])
    else:
        allValidationAccuracies = np.array(results['validation-accuracy'])
        allTrainingAccuracies = np.array(results['training-accuracy'])
    print ('--Start iterations------------------------------------------')
    while not stop:
        batch = dataset.train.next_batch(32)
        pat = (pat + 32)
        sess.run([train_step, cost], feed_dict={x:batch[0], y_:batch[1]})
        if (pat >= nPatterns):
            pat = 0
            epoch = (epoch + 1)
            if (epoch >= nbEpochs):
                stop = True
            if (0 == (epoch % 1)):
                validationAccuracy = getAccuracy(dataset.validation, sess)
                print (('validation accuracy %g' % validationAccuracy))
                allValidationAccuracies = np.append(allValidationAccuracies, [[epoch, validationAccuracy]], axis=0)
                trainingAccuracy = getAccuracy(dataset.train, sess)
                print (('training accuracy %g' % trainingAccuracy))
                allTrainingAccuracies = np.append(allTrainingAccuracies, [[epoch, trainingAccuracy]], axis=0)
            if (0 == (epoch % 1)):
                expstatus['status'] = 'running'
                expstatus['progress'] = ((epoch - startEpochs) / 100)
                sec_per_epoch = ((time.time() - start_time) / (epoch - startEpochs))
                eta = round((sec_per_epoch * (100 - (epoch - startEpochs))))
                expstatus['eta'] = eta
                with open((expname + '.json'), 'w') as f:
                    f.write(json.dumps(expstatus))
                    f.close()

    print ('--End iterations--------------------------------------------')
    validationAccuracy = getAccuracy(dataset.validation, sess)
    print (('validation accuracy %g' % validationAccuracy))
    results['final-validation-accuracy'] = str(validationAccuracy)

    np.save('W1', sess.run(w1))
    np.save('B1', sess.run(b1))
    np.save('W2', sess.run(w2))
    np.save('B2', sess.run(b2))
    np.save('W3', sess.run(w3))
    np.save('B3', sess.run(b3))
    np.save('W4', sess.run(w4))
    np.save('B4', sess.run(b4))
    np.save('W5', sess.run(w5))
    np.save('B5', sess.run(b5))
    np.save('W6', sess.run(w6))
    np.save('B6', sess.run(b6))
    np.save('W7', sess.run(w7))
    np.save('B7', sess.run(b7))
    np.save('W8', sess.run(w8))
    np.save('B8', sess.run(b8))
    np.save('W9', sess.run(w9))
    np.save('B9', sess.run(b9))
    np.save('W10', sess.run(w10))
    np.save('B10', sess.run(b10))
    np.save('W11', sess.run(w11))
    np.save('B11', sess.run(b11))

    results['duration'] = (results['duration'] + math.ceil((time.time() - start_time)))
    results['hostname'] = socket.gethostname()
    res = resource.getrusage(resource.RUSAGE_SELF)
    results['maxmem'] = res.ru_maxrss
    results['nb-epochs'] = (startEpochs + 100)
    results['training-accuracy'] = allTrainingAccuracies.tolist()
    results['validation-accuracy'] = allValidationAccuracies.tolist()
    with open(expname + '-results.json', 'w') as f:
        f.write(json.dumps(results))
        f.close()

    expstatus['status'] = 'finished'
    expstatus['globalEpochs'] = (startEpochs + 100)
    with open((expname + '.json'), 'w') as f:
        f.write(json.dumps(expstatus))
        f.close()
