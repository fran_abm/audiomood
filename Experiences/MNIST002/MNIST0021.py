#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 11:13:08 2018

@author: tl
"""
import sys

# find tldatasets.py here: http://tl.di.fc.ul.pt/s/tldatasets.py
# base.py (http://tl.di.fc.ul.pt/s/base.py) is needed.

# Change the path to your favorite python directory
# sys.path = (sys.path + [('/home/quietswami/' + 'audiomood/platform/')])
# Only needed if you have several different Python's installed at the same time

# Name of the experience
# Needed for the datacenter.
# Will be the name of the folder, in which this script will be.

expname='MNIST0021'

# Load MNIST Data
import tldatasets as tld

import json as json
import tensorflow as tf

# train_dir - pasta dos datasets.
# DATACENTER: /home/hpc/tl/Datasets/MNIST_data

mnist = tld.read_data_sets(train_dir='./', 
                               train_images='train-images-idx3-ubyte.gz', 
                               train_labels='train-labels-idx1-ubyte.gz',
                               validation_images=None, 
                               validation_labels=None, 
                               test_images='t10k-images-idx3-ubyte.gz', 
                               test_labels='t10k-labels-idx1-ubyte.gz', 
                               nbclasses=10, one_hot=True, 
                               reshape=True, 
                               source_url='http://tl.di.fc.ul.pt/s/TF/data/')

# This JSON is responsible for having the status of the experience.
# This will be changed throughout the script 
expstatus={'status':'running'}
with open(expname+'.json', 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()
  


def weight_variable(shape):
  """
  This function returns a tf.Variable, that is used
    to remember the weights that are given to the graph.
  
  Args:
    - shape
  """
  initial = tf.truncated_normal(shape, stddev=0.1) 
  # Outputs random values from a truncated normal distribution.
  # The generated values follow a normal distribution with specified 
  # mean and standard deviation, except that values whose magnitude 
  # is more than 2 standard deviations from the mean are dropped and re-picked.
  
  return tf.Variable(initial)
  # Used to mantain state in the graph that is used to run.
  # We use this output to maintain the state of the weighted variables.

def bias_variable(shape):
  """
  This function returns a tf.Variable, that is used
    to remember the bias that are given to the graph.
  """
  initial = tf.constant(0.1, shape=shape)
  # This will create a tensor, filled with 0.1, in the shape given
  return tf.Variable(initial)

def conv2d(x, W):
  """
  Returns a 2D graph, when given a 4D graph (x), and a filter (W) tensor.

  Args:
    - x: input tensor.
    - W: filter tensor.
  """
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  """
  This function performs max pooling to a given input tensor.

  Args:
    - x: a 4D input tensor .
  """
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

  # ksize: The size of the window for each dimension of the input tensor.
  # strides: The stride of the sliding window for each dimension of the input tensor.
  # padding: A string, either 'VALID' or 'SAME'. The padding algorithm.


# We start building the computation graph by creating nodes for the input 
# images and target output classes.


x = tf.placeholder(tf.float32, shape=[None, 784])
# placeholder will create a placeholder tensor for a tensor that will be fed.

# x will consist of a 2d tensor of floating point numbers. Here we assign it a 
# shape of [None, 784], where 784 is the dimensionality of a single flattened 
# 28 by 28 pixel MNIST image, and None indicates that the first dimension, 
# corresponding to the batch size, can be of any size.
# If trying with different size windows, the number 784 must be changed for 
# the size of the new window.
# TODO: Create a function to automaticate this.

y_ = tf.placeholder(tf.float32, shape=[None, 10])

x_image = tf.reshape(x, [-1, 28, 28, 1]) 
# Reshape the x tensor, for a tensor with the given (-1, 28,28,1) shape.
# Not forgeting that it is a 4D tensor.

# Create the weight and bias variables.
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])

# TODO: What is H? 
# The first two layers are:
# - a convolutional layer using relu
# - a polling layer using max pool.
h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])

h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])

y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

with tf.Session() as sess:
  sess.run(tf.global_variables_initializer())
  numOfSteps = 1000
  for i in range(1000):
    batch = mnist.train.next_batch(50)
    if i % 10 == 0:
      train_accuracy = accuracy.eval(feed_dict={
          x: batch[0], y_: batch[1], keep_prob: 1.0})
      print('step %d, training accuracy %g' % (i, train_accuracy))
      expstatus['progress']=i/numOfSteps
      print("Progress: ", expstatus['progress'])
      with open(expname+'.json', 'w') as f:
          f.write(json.dumps(expstatus))
          f.close()
    train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

  print('test accuracy %g' % accuracy.eval(feed_dict={
      x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))

    
expstatus['status']='finished'
with open(expname+'.json', 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()
    