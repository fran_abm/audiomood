# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Converts MNIST data to TFRecords file format with Example protos."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tensorflow as tf
import numpy
from numpy import genfromtxt
from six.moves import xrange  # pylint: disable=redefined-builtin
from tensorflow.python.framework import dtypes

FLAGS = None


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


class DataSet(object):
    def __init__(self,
                 images,
                 labels,
                 dtype=dtypes.float32,
                 reshape=False,
                 seed=None,
                 imageOnOutput=False,
                 rescale=True):
        """
        Construct a DataSet.
        one_hot arg is used only if fake_data is true.  `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.  Seed arg provides for convenient deterministic testing.
        """
        print("in DataSet constructor seed:" + str(seed))
        print('imageOnOutput: ' + str(imageOnOutput))
        # print(images.shape)
        # print(labels)
        # if labels is not None:
        #     print(labels.shape)
        # About differences between random.random() and numpy.random.random
        # numpy version is not thread safe.
        # https://stackoverflow.com/questions/7029993/differences-between-numpy-random-and-random-random-in-python
        # seed1, seed2 = random_seed.get_seed(seed)
        # If op level seed is not set, use whatever graph level seed is returned
        # numpy.random.seed(seed1 if seed is None else seed2)
        numpy.random.seed(seed)
        dtype = dtypes.as_dtype(dtype).base_dtype
        if dtype not in (dtypes.uint8, dtypes.float32):
            raise TypeError('Invalid image dtype %r, expected uint8 or float32' %
                            dtype)
        if labels is not None:
            assert images.shape[0] == labels.shape[0], ('images.shape: %s labels.shape: %s' % (images.shape, labels.shape))
        self._num_examples = images.shape[0]
        # Convert shape from module import symbol [num examples, rows, columns, depth]
        # to [num examples, rows*columns] (assuming depth == 1)
        #                                  *******************
        if reshape:
            # assert images.shape[3] == 1
            if images.shape[3] == 1:
                print("reshaping images -> ", images.shape[0],
                      "(# patterns)x", images.shape[1] * images.shape[2],
                      "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2])
            else: # more than 1 channel
                print("reshaping images -> ", images.shape[0],
                      "(# patterns)x", images.shape[1] * images.shape[2] * images.shape[3],
                      "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2] * images.shape[3])
            if imageOnOutput:
                if labels.shape[3] == 1:
                    print("reshaping images (labels) -> ", labels.shape[0],
                          "(# patterns)x",
                          labels.shape[1] * labels.shape[2],
                          "(linearized size of image)")
                    labels = labels.reshape(labels.shape[0],
                                            labels.shape[1] * labels.shape[2])
                else:
                    print("reshaping images (labels) -> ", labels.shape[0],
                          "(# patterns)x",
                          labels.shape[1] * labels.shape[2] * labels.shape[3],
                          "(linearized size of image)")
                    labels = labels.reshape(labels.shape[0],
                                            labels.shape[1] * labels.shape[2] * labels.shape[3])                    
        else:
            print("no image reshaping.")
        print("images shape", images.shape)
        if dtype == dtypes.float32 and rescale:
            print("image dtype = ", dtype, " rescaling-> [0.0, 1.0].")
            # Convert from [0, 255] -> [0.0, 1.0].
            images = images.astype(numpy.float32)
            images = numpy.multiply(images, 1.0 / 255.0)
            if imageOnOutput and rescale:
                print("labels dtype = ", dtype, " rescaling-> [0.0, 1.0].")
                # Convert from [0, 255] -> [0.0, 1.0].
                labels = labels.astype(numpy.float32)
                labels = numpy.multiply(labels, 1.0 / 255.0)
        else:
            print("dtype = ", dtype, "no rescaling.")
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, shuffle=True, circular=True):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        # Shuffle for the first epoch
        if self._epochs_completed == 0 and start == 0 and shuffle:
            # print("shuffle")
            perm0 = numpy.arange(self._num_examples)
            numpy.random.shuffle(perm0)
            self._images = self.images[perm0]
            self._labels = self.labels[perm0]
        # Go to the next epoch
        if start + batch_size > self._num_examples:
            # print("end of epoch " +str(start) + " " + str(batch_size) + ">" + str(self._num_examples))
            # Finished epoch
            self._epochs_completed += 1
            # Get the rest examples in this epoch
            rest_num_examples = self._num_examples - start
            images_rest_part = self._images[start:self._num_examples]
            labels_rest_part = self._labels[start:self._num_examples]
            # Shuffle the data
            if shuffle:
                # print("eoe shuffle")
                perm = numpy.arange(self._num_examples)
                numpy.random.shuffle(perm)
                self._images = self.images[perm]
                self._labels = self.labels[perm]
            # Start next epoch
            if circular:
                # print("eoe circular")
                start = 0
                self._index_in_epoch = batch_size - rest_num_examples
                end = self._index_in_epoch
                images_new_part = self._images[start:end]
                labels_new_part = self._labels[start:end]
                return numpy.concatenate((images_rest_part, images_new_part), axis=0), numpy.concatenate((labels_rest_part, labels_new_part), axis=0)
            else:
                # print("eoe not circular " + str(start) + " " + str(self._num_examples) + " " + str(self._num_examples - start))
                self._index_in_epoch = 0
                return images_rest_part, labels_rest_part
        else:
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            # print("continue "+ str(start) + " " + str(end) + " " + str(end - start))
            return self._images[start:end], self._labels[start:end]


    def next_batch_no_labels(self, batch_size, shuffle=True, circular=True):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        # Shuffle for the first epoch
        if self._epochs_completed == 0 and start == 0 and shuffle:
            # print("shuffle")
            perm0 = numpy.arange(self._num_examples)
            numpy.random.shuffle(perm0)
            self._images = self.images[perm0]
        # Go to the next epoch
        if start + batch_size > self._num_examples:
            # print("end of epoch " +str(start) + " " + str(batch_size) + ">" + str(self._num_examples))
            # Finished epoch
            self._epochs_completed += 1
            # Get the rest examples in this epoch
            rest_num_examples = self._num_examples - start
            images_rest_part = self._images[start:self._num_examples]
            # Shuffle the data
            if shuffle:
                # print("eoe shuffle")
                perm = numpy.arange(self._num_examples)
                numpy.random.shuffle(perm)
                self._images = self.images[perm]
            # Start next epoch
            if circular:
                # print("eoe circular")
                start = 0
                self._index_in_epoch = batch_size - rest_num_examples
                end = self._index_in_epoch
                images_new_part = self._images[start:end]
                return numpy.concatenate((images_rest_part, images_new_part), axis=0), None
            else:
                # print("eoe not circular " + str(start) + " " + str(self._num_examples) + " " + str(self._num_examples - start))
                self._index_in_epoch = 0
                return images_rest_part, None
        else:
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            # print("continue "+ str(start) + " " + str(end) + " " + str(end - start))
            return self._images[start:end], None

#----------------------------------------------------------------------
import gzip


def _read32(bytestream):
    dt = numpy.dtype(numpy.uint32).newbyteorder('>')
    return numpy.frombuffer(bytestream.read(4), dtype=dt)[0]


def extract_images(f):
    """Extract the images into a 4D uint8 numpy array [index, y, x, depth].
    Args:
    f: A file object that can be passed into a gzip reader.
    Returns:
    data: A 4D uint8 numpy array [index, y, x, depth].
    Raises:
    ValueError: If the bytestream does not start with 2051.
    """
    print('Extracting', f.name)
    with gzip.GzipFile(fileobj=f) as bytestream:
        magic = _read32(bytestream)
        if magic != 2051 and magic != 2052:
            raise ValueError('Invalid magic number %d in ubyte image file: %s' % (magic, f.name))
        num_images = _read32(bytestream)
        rows = _read32(bytestream)
        cols = _read32(bytestream)
        if magic == 2051:
            buf = bytestream.read(rows * cols * num_images)
            data = numpy.frombuffer(buf, dtype=numpy.uint8)
            data = data.reshape(num_images, rows, cols, 1)
        else:
            # magic == 2052
            buf = bytestream.read(rows * cols * num_images * 2)
            data = numpy.frombuffer(buf, dtype=numpy.uint8)
            data = data.reshape(num_images, rows, cols, 2)
        return data

    
def dense_to_one_hot(labels_dense, num_classes):
    """Convert class labels from scalars to one-hot vectors."""
    num_labels = labels_dense.shape[0]
    index_offset = numpy.arange(num_labels) * num_classes
    labels_one_hot = numpy.zeros((num_labels, num_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot


def extract_labels(f, num_classes, one_hot=False):
    """Extract the labels into a 1D uint8 numpy array [index].

  Args:
    f: A file object that can be passed into a gzip reader.
    one_hot: Does one hot encoding for the result.
    num_classes: Number of classes for the one hot encoding.

  Returns:
    labels: a 1D uint8 numpy array.

  Raises:
    ValueError: If the bystream doesn't start with 2049.
    """
    print('Extracting', f.name)
    with gzip.GzipFile(fileobj=f) as bytestream:
        magic = _read32(bytestream)
        if magic != 2049:
            raise ValueError('Invalid magic number %d in MNIST label file: %s' %
                             (magic, f.name))
        num_items = _read32(bytestream)
        buf = bytestream.read(num_items)
        labels = numpy.frombuffer(buf, dtype=numpy.uint8)
        if one_hot:
            print("one_hot is True")
            return dense_to_one_hot(labels, num_classes)
        print("one_hot is False")
        return labels


#==============================================================================
# from tensorflow.contrib.learn.python.learn.datasets import base
import base as base
from tensorflow.python.framework import random_seed
from tensorflow.python.platform import gfile

DEFAULT_SOURCE_URL = 'https://storage.googleapis.com/cvdf-datasets/mnist/'

def load_ubyte(images_ubyte, labels_ubyte, train_dir, source_url, imageOnOutput, nbclasses, one_hot):
    print('image_ubyte:', images_ubyte, ' labels_ubyte: ', labels_ubyte)
    if images_ubyte is None:
        return (None, None)
    # Load data set----------------------------------------------------------
    local_file = base.maybe_download(images_ubyte, train_dir,
                                     source_url + images_ubyte)
    with gfile.Open(local_file, 'rb') as f:
        images = extract_images(f)
    if labels_ubyte is not None:    
        local_file = base.maybe_download(labels_ubyte, train_dir,
                                         source_url + labels_ubyte)
        with gfile.Open(local_file, 'rb') as f:
            if imageOnOutput:
                labels = extract_images(f)
            else:
                labels = extract_labels(f, nbclasses, one_hot=one_hot)
                return (images, labels)
    else:
        return (images, None)
    
def read_data_sets(train_dir,
                   train_images='train-images-idx3-ubyte.gz',
                   train_labels='train-labels-idx1-ubyte.gz',
                   validation_images='validation-images-idx3-ubyte.gz',
                   validation_labels='validation-labels-idx1-ubyte.gz',
                   test_images=None,
                   test_labels=None,
                   nbclasses=10,
                   one_hot=False,
                   # if dtype is float32 then values are rescaled between
                   # 0.0 and 1,0
                   dtype=dtypes.float32,
                   reshape=False,        # passed to DataSet constructor
                   validation_size=0,
                   seed=None,            # passed to DataSet constructor
                   source_url=DEFAULT_SOURCE_URL,
                   imageOnOutput=False):
    #----------------------------------------------------------------------------
    print('imageOnOutput: ' + str(imageOnOutput))
    if not source_url:  # empty string check
        source_url = DEFAULT_SOURCE_URL
    options = dict(dtype=dtype, reshape=reshape, seed=seed, imageOnOutput=imageOnOutput)
    # special case where all available data is used for training
    if validation_images == None and test_images == None:
        (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                      source_url, imageOnOutput, nbclasses, one_hot)
        train = DataSet(train_images, train_labels, **options)
        return base.Datasets(train=train, validation=train, test=None)
    # Load training set----------------------------------------------------------
    (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                              source_url, imageOnOutput, nbclasses, one_hot)
    # Load validation set--------------------------------------------------------
    (validation_images, validation_labels) = load_ubyte(validation_images, validation_labels, train_dir,
                                                        source_url, imageOnOutput, nbclasses, one_hot)
    # Load test set--------------------------------------------------------------
    (test_images, test_labels) = load_ubyte(test_images, test_labels, train_dir,
                                            source_url, imageOnOutput, nbclasses, one_hot)
    #----------------------------------------------------------------------------
    if train_images is None:
        train = None
    else:
        train = DataSet(train_images, train_labels, **options)
    if validation_images is None:
        validation = None
    else:
        validation = DataSet(validation_images, validation_labels, **options)
    if test_images is None:
        test = None
    else:
        test = DataSet(test_images, test_labels, **options)
    return base.Datasets(train=train, validation=validation, test=test)


def loadCSVTestData(imagesCSVFile,
                    dtype=dtypes.float32,
                    reshape=False,
                    seed=None,
                    imageOnOutput=False,
                    delimiter=',',
                    num_images=0,
                    rows=0,
                    cols=0,
                    rescale=True):
    images = genfromtxt(imagesCSVFile, delimiter=delimiter)
    images = images.reshape(num_images, rows, cols, 1)
    # labels = genfromtxt(labels, delimiter=delimiter)
    options = dict(dtype=dtype, reshape=reshape, seed=seed, rescale=rescale)
    test = DataSet(images, None, **options)
    return test


