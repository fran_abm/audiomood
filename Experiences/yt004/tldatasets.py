# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Converts MNIST data to TFRecords file format with Example protos."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime as datetime
import gc as gc
import json as json
# import os
import tensorflow as tf
import numpy
from numpy import genfromtxt
# from six.moves import xrange  # pylint: disable=redefined-builtin
from tensorflow.python.framework import dtypes

FLAGS = None
verbose = True

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


class DataSet(object):
    def __init__(self,
                 images,
                 labels,
                 dtype=dtypes.float32,
                 reshape=False,
                 seed=None,
                 imageOnOutput=False,
                 rescale=True):
        """
        Construct a DataSet.
        one_hot arg is used only if fake_data is true.  `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.  Seed arg provides for convenient deterministic testing.
        """
        print("in DataSet constructor seed:" + str(seed))
        print('imageOnOutput: ' + str(imageOnOutput))
        # print(images.shape)
        # print(labels)
        # if labels is not None:
        #     print(labels.shape)
        # About differences between random.random() and numpy.random.random
        # numpy version is not thread safe.
        # https://stackoverflow.com/questions/7029993/differences-between-numpy-random-and-random-random-in-python
        # seed1, seed2 = random_seed.get_seed(seed)
        # If op level seed is not set, use whatever graph level seed is returned
        # numpy.random.seed(seed1 if seed is None else seed2)
        numpy.random.seed(seed)
        dtype = dtypes.as_dtype(dtype).base_dtype
        if dtype not in (dtypes.uint8, dtypes.float32):
            raise TypeError('Invalid image dtype %r, expected uint8 or float32' %
                            dtype)
        if labels is not None:
            assert images.shape[0] == labels.shape[0], ('images.shape: %s labels.shape: %s' % (images.shape, labels.shape))
        self.nPatterns = images.shape[0]
        self.epochCompleted = False
        # Convert shape from module import symbol [num examples, rows, columns, depth]
        # to [num examples, rows*columns] (assuming depth == 1)
        #                                  *******************
        if reshape:
            # assert images.shape[3] == 1
            if images.shape[3] == 1:
                print("reshaping images -> ", images.shape[0],
                      "(# patterns)x", images.shape[1] * images.shape[2],
                      "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2])
            else: # more than 1 channel
                print("reshaping images -> ", images.shape[0],
                      "(# patterns)x", images.shape[1] * images.shape[2] * images.shape[3],
                      "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2] * images.shape[3])
            if imageOnOutput:
                if labels.shape[3] == 1:
                    print("reshaping images (labels) -> ", labels.shape[0],
                          "(# patterns)x",
                          labels.shape[1] * labels.shape[2],
                          "(linearized size of image)")
                    labels = labels.reshape(labels.shape[0],
                                            labels.shape[1] * labels.shape[2])
                else:
                    print("reshaping images (labels) -> ", labels.shape[0],
                          "(# patterns)x",
                          labels.shape[1] * labels.shape[2] * labels.shape[3],
                          "(linearized size of image)")
                    labels = labels.reshape(labels.shape[0],
                                            labels.shape[1] * labels.shape[2] * labels.shape[3])                    
        else:
            print("no image reshaping.")
        print("images shape", images.shape)
        if dtype == dtypes.float32 and rescale:
            print("image dtype = ", dtype, " rescaling-> [0.0, 1.0].")
            # Convert from [0, 255] -> [0.0, 1.0].
            images = images.astype(numpy.float32)
            images = numpy.multiply(images, 1.0 / 255.0)
            if imageOnOutput and rescale:
                print("labels dtype = ", dtype, " rescaling-> [0.0, 1.0].")
                # Convert from [0, 255] -> [0.0, 1.0].
                labels = labels.astype(numpy.float32)
                labels = numpy.multiply(labels, 1.0 / 255.0)
        else:
            print("dtype = ", dtype, "no rescaling.")
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    # @property
    # def nPatterns(self):
    #     return self.nPatterns

    # @nPatterns.setter
    # def nPatterns(self, value):
    #     self.nPatterns = value
    
    @property
    def epochs_completed(self):
        return self._epochs_completed

    def resetIndex(self):
        self._index_in_epoch = 0
        self.epochCompleted = False

    def next_batch(self, batch_size, shuffle=True, circular=True):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        # Shuffle for the first epoch
        if self._epochs_completed == 0 and start == 0 and shuffle:
            self.epochCompleted = False
            perm0 = numpy.arange(self.nPatterns)
            numpy.random.shuffle(perm0)
            self._images = self.images[perm0]
            self._labels = self.labels[perm0]
        # Go to the next epoch
        if start + batch_size > self.nPatterns:
            # Finished epoch
            self._epochs_completed += 1
            self.epochCompleted = True
            # Get the rest examples in this epoch
            restnPatterns = self.nPatterns - start
            images_rest_part = self._images[start:self.nPatterns]
            labels_rest_part = self._labels[start:self.nPatterns]
            if shuffle:
                # End of epoch: let's shuffle
                perm = numpy.arange(self.nPatterns)
                numpy.random.shuffle(perm)
                self._images = self.images[perm]
                self._labels = self.labels[perm]
            # Start next epoch
            if circular:
                # End of epoch: circular case. Take the number of patterns
                # necessary to complete the batch at the beginning of the set.
                start = 0
                self._index_in_epoch = batch_size - restnPatterns
                end = self._index_in_epoch
                images_new_part = self._images[start:end]
                labels_new_part = self._labels[start:end]
                return (numpy.concatenate((images_rest_part, images_new_part), axis=0),
                        numpy.concatenate((labels_rest_part, labels_new_part), axis=0))
            else:
                # End of epoch not circular: return only the rest of the data.
                self._index_in_epoch = 0
                return images_rest_part, labels_rest_part
        else:
            # not the end of epoch, continue 
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            return self._images[start:end], self._labels[start:end]


    def next_batch_no_labels(self, batch_size, shuffle=True, circular=True):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        # Shuffle for the first epoch
        if self._epochs_completed == 0 and start == 0 and shuffle:
            # print("shuffle")
            perm0 = numpy.arange(self.nPatterns)
            numpy.random.shuffle(perm0)
            self._images = self.images[perm0]
        # Go to the next epoch
        if start + batch_size > self.nPatterns:
            # print("end of epoch " +str(start) + " " + str(batch_size) + ">" + str(self.nPatterns))
            # Finished epoch
            self._epochs_completed += 1
            # Get the rest examples in this epoch
            restnPatterns = self.nPatterns - start
            images_rest_part = self._images[start:self.nPatterns]
            # Shuffle the data
            if shuffle:
                # print("eoe shuffle")
                perm = numpy.arange(self.nPatterns)
                numpy.random.shuffle(perm)
                self._images = self.images[perm]
            # Start next epoch
            if circular:
                # print("eoe circular")
                start = 0
                self._index_in_epoch = batch_size - restnPatterns
                end = self._index_in_epoch
                images_new_part = self._images[start:end]
                return numpy.concatenate((images_rest_part, images_new_part), axis=0), None
            else:
                # print("eoe not circular " + str(start) + " " + str(self.nPatterns) + " " + str(self.nPatterns - start))
                self._index_in_epoch = 0
                return images_rest_part, None
        else:
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            # print("continue "+ str(start) + " " + str(end) + " " + str(end - start))
            return self._images[start:end], None

#----------------------------------------------------------------------
import gzip


def _read32(bytestream):
    dt = numpy.dtype(numpy.uint32).newbyteorder('>')
    return numpy.frombuffer(bytestream.read(4), dtype=dt)[0]


def extract_images(f):
    """Extract the images into a 4D uint8 numpy array [index, y, x, depth].
    Args:
    f: A file object that can be passed into a gzip reader.
    Returns:
    data: A 4D uint8 numpy array [index, y, x, depth].
    Raises:
    ValueError: If the bytestream does not start with 2051.
    """
    global verbose
    if verbose:
        print('Extracting', f.name)
    with gzip.GzipFile(fileobj=f) as bytestream:
        magic = _read32(bytestream)
        if magic != 2051 and magic != 2052:
            raise ValueError('Invalid magic number %d in ubyte image file: %s' % (magic, f.name))
        num_images = _read32(bytestream)
        rows = _read32(bytestream)
        cols = _read32(bytestream)
        if magic == 2051:
            buf = bytestream.read(rows * cols * num_images)
            data = numpy.frombuffer(buf, dtype=numpy.uint8)
            data = data.reshape(num_images, rows, cols, 1)
        else:
            # magic == 2052
            buf = bytestream.read(rows * cols * num_images * 2)
            data = numpy.frombuffer(buf, dtype=numpy.uint8)
            data = data.reshape(num_images, rows, cols, 2)
        return data

    
def dense_to_one_hot(labels_dense, num_classes):
    """Convert class labels from scalars to one-hot vectors."""
    num_labels = labels_dense.shape[0]
    index_offset = numpy.arange(num_labels) * num_classes
    labels_one_hot = numpy.zeros((num_labels, num_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot


def extract_labels(f, num_classes, one_hot=False):
    """Extract the labels into a 1D uint8 numpy array [index].

  Args:
    f: A file object that can be passed into a gzip reader.
    one_hot: Does one hot encoding for the result.
    num_classes: Number of classes for the one hot encoding.

  Returns:
    labels: a 1D uint8 numpy array.

  Raises:
    ValueError: If the bystream doesn't start with 2049.
    """
    global verbose
    if verbose:
        print('Extracting', f.name)
    with gzip.GzipFile(fileobj=f) as bytestream:
        magic = _read32(bytestream)
        if magic != 2049:
            raise ValueError('Invalid magic number %d in MNIST label file: %s' %
                             (magic, f.name))
        num_items = _read32(bytestream)
        buf = bytestream.read(num_items)
        labels = numpy.frombuffer(buf, dtype=numpy.uint8)
        if one_hot:
            if verbose:
                print("one_hot is True")
            return dense_to_one_hot(labels, num_classes)
        if verbose:
            print("one_hot is False")
        return labels


#==============================================================================
# from tensorflow.contrib.learn.python.learn.datasets import base
import base as base
from tensorflow.python.framework import random_seed
from tensorflow.python.platform import gfile

DEFAULT_SOURCE_URL = 'https://storage.googleapis.com/cvdf-datasets/mnist/'


def load_ubyte(images_ubyte, labels_ubyte, train_dir, source_url,
               imageOnOutput, nbclasses, one_hot):
    if verbose:
        print('image_ubyte:', images_ubyte, ' labels_ubyte: ', labels_ubyte)
    if images_ubyte is None:
        return (None, None)
    # Load data set----------------------------------------------------------
    local_file = base.maybe_download(images_ubyte, train_dir,
                                     source_url + images_ubyte)
    with gfile.Open(local_file, 'rb') as f:
        images = extract_images(f)
    if labels_ubyte is not None:    
        local_file = base.maybe_download(labels_ubyte, train_dir,
                                         source_url + labels_ubyte)
        with gfile.Open(local_file, 'rb') as f:
            if imageOnOutput:
                labels = extract_images(f)
            else:
                labels = extract_labels(f, nbclasses, one_hot=one_hot)
        return (images, labels)
    else:
        return (images, None)


def read_data_sets(train_dir,
                   train_images='train-images-idx3-ubyte.gz',
                   train_labels='train-labels-idx1-ubyte.gz',
                   validation_images='validation-images-idx3-ubyte.gz',
                   validation_labels='validation-labels-idx1-ubyte.gz',
                   test_images=None,
                   test_labels=None,
                   nbclasses=10,
                   one_hot=False,
                   # if dtype is float32 then values are rescaled between
                   # 0.0 and 1,0
                   dtype=dtypes.float32,
                   reshape=False,        # passed to DataSet constructor
                   validation_size=0,
                   seed=None,            # passed to DataSet constructor
                   source_url=DEFAULT_SOURCE_URL,
                   imageOnOutput=False):
    #----------------------------------------------------------------------------
    print('imageOnOutput: ' + str(imageOnOutput))
    if not source_url:  # empty string check
        source_url = DEFAULT_SOURCE_URL
    options = dict(dtype=dtype, reshape=reshape, seed=seed, imageOnOutput=imageOnOutput)
    # special case where all available data is used for training
    if validation_images == None and test_images == None:
        (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                      source_url, imageOnOutput, nbclasses, one_hot)
        train = DataSet(train_images, train_labels, **options)
        return base.Datasets(train=train, validation=train, test=None)
    # Load training set----------------------------------------------------------
    (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                              source_url, imageOnOutput, nbclasses, one_hot)
    # Load validation set--------------------------------------------------------
    (validation_images, validation_labels) = load_ubyte(validation_images, validation_labels, train_dir,
                                                        source_url, imageOnOutput, nbclasses, one_hot)
    # Load test set--------------------------------------------------------------
    (test_images, test_labels) = load_ubyte(test_images, test_labels, train_dir,
                                            source_url, imageOnOutput, nbclasses, one_hot)
    #----------------------------------------------------------------------------
    if train_images is None:
        train = None
    else:
        train = DataSet(train_images, train_labels, **options)
    if validation_images is None:
        validation = None
    else:
        validation = DataSet(validation_images, validation_labels, **options)
    if test_images is None:
        test = None
    else:
        test = DataSet(test_images, test_labels, **options)
    return base.Datasets(train=train, validation=validation, test=test)

# ================================================================================


class DataSetFolds(object):
    
    def __init__(self,
                 # the folds numbers:
                 folds,
                 nfolds,
                 imagesUbytePrefix,
                 labelsUbytePrefix,
                 nPatterns,
                 dtype=dtypes.float32,
                 reshape=False,
                 seed=None,
                 imageOnOutput=False,
                 rescale=True,
                 train_dir=".",
                 source_url="",
                 nbclasses=0,
                 one_hot=True,
                 nextFoldStrategy="circular"):
        """
        Construct a DataSetFolds.
        one_hot arg is used only if fake_data is true.  `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.  Seed arg provides for convenient deterministic testing.
        """
        verbose = True
        print("in DataSetFolds constructor seed:" + str(seed))
        print('imageOnOutput: ' + str(imageOnOutput))
        # print(images.shape)
        # print(labels)
        # if labels is not None:
        #     print(labels.shape)
        # About differences between random.random() and numpy.random.random
        # numpy version is not thread safe.
        # https://stackoverflow.com/questions/7029993/differences-between-numpy-random-and-random-random-in-python
        # seed1, seed2 = random_seed.get_seed(seed)
        # If op level seed is not set, use whatever graph level seed is returned
        # numpy.random.seed(seed1 if seed is None else seed2)
        numpy.random.seed(seed)
        dtype = dtypes.as_dtype(dtype).base_dtype
        if dtype not in (dtypes.uint8, dtypes.float32):
            raise TypeError('Invalid image dtype %r, expected uint8 or float32' %
                            dtype)
        #--------------------
        self.nfolds = nfolds
        self.folds = folds
        self.imagesUbytePrefix = imagesUbytePrefix
        self.labelsUbytePrefix = labelsUbytePrefix
        self.train_dir = train_dir
        self.source_url = source_url
        self.nbclasses = nbclasses
        self.nPatterns = nPatterns
        self.reshape = reshape
        self.rescale = rescale
        self.dtype = dtype
        self.one_hot = one_hot
        self.imageOnOutput = imageOnOutput
        self.nextFoldStrategy = nextFoldStrategy
        self.images = None
        self.labels = None
        self.fromPatternIndice = 0
        self.currentFoldIndice = 0
        self.epochCompleted = False
        #--------------------
        (self.images, self.labels) = self.loadFold(self.currentFoldIndice)
        verbose = False

    def loadFold(self, n):
        global verbose
        foldNumber = self.folds[n]
        imagesFile = (self.imagesUbytePrefix + "-" + str(self.nfolds)
                      + "f" + str(foldNumber) + "-ubyte.gz")
        labelsFile = (self.labelsUbytePrefix + "-" + str(self.nfolds)
                      + "f" + str(foldNumber) + "-ubyte.gz")
        print("Loading fold ", n, ":", foldNumber)
        (images, labels) = load_ubyte(imagesFile, labelsFile, self.train_dir,
                                      self.source_url, self.imageOnOutput,
                                      self.nbclasses, self.one_hot)
        if self.reshape:
            # assert images.shape[3] == 1
            if images.shape[3] == 1:
                if verbose:
                    print("reshaping images -> ", images.shape[0],
                          "(# patterns)x", images.shape[1] * images.shape[2],
                          "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2])
            else: # more than 1 channel
                if verbose:
                    print("reshaping images -> ", images.shape[0],
                          "(# patterns)x", images.shape[1] * images.shape[2] * images.shape[3],
                          "(linearized size of image)")
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2] * images.shape[3])
                if self.imageOnOutput:
                    if labels.shape[3] == 1:
                        if verbose:
                            print("reshaping images (labels) -> ", labels.shape[0],
                                  "(# patterns)x",
                                  labels.shape[1] * labels.shape[2],
                                  "(linearized size of image)")
                        labels = labels.reshape(labels.shape[0],
                                                labels.shape[1] * labels.shape[2])
                    else:
                        if verbose:
                            print("reshaping images (labels) -> ", labels.shape[0],
                                  "(# patterns)x",
                                  labels.shape[1] * labels.shape[2] * labels.shape[3],
                                  "(linearized size of image)")
                        labels = labels.reshape(labels.shape[0],
                                                labels.shape[1] *
                                                labels.shape[2] * labels.shape[3])
                else:
                    if verbose:
                        print("no image reshaping.")
                        print("images shape", images.shape)
        if self.dtype == dtypes.float32 and self.rescale:
            if verbose:
                print("image dtype = ", self.dtype, " rescaling-> [0.0, 1.0].")
            # Convert from [0, 255] -> [0.0, 1.0].
            images = images.astype(numpy.float32)
            images = numpy.multiply(images, 1.0 / 255.0)
            if self.imageOnOutput and self.rescale:
                if verbose:
                    print("labels dtype = ", self.dtype, " rescaling-> [0.0, 1.0].")
                # Convert from [0, 255] -> [0.0, 1.0].
                labels = labels.astype(numpy.float32)
                labels = numpy.multiply(labels, 1.0 / 255.0)
            else:
                if verbose:
                    print("dtype = ", self.dtype, "no rescaling.")
        if labels is not None:
            assert images.shape[0] == labels.shape[0], ('images.shape: %s labels.shape: %s' % (images.shape, labels.shape))
        self.nPatternsInFold = images.shape[0]
        # print("Loading fold ", foldNumber, " done.")
        verbose = False
        return (images, labels)

    def resetIndex(self):
        self.currentFoldIndice = 0
        self.fromPatternIndice = 0
        self.epochCompleted = False
        
    def next_batch(self, batch_size, shuffle=True, circular=False):
        """Return the next `batch_size` examples from this data set."""
        # if verbose:
        #     print("in next_batch(",batch_size,"). currentFoldIndice=", self.currentFoldIndice, ":", self.folds[self.currentFoldIndice] , " fromPatternIndice: ", self.fromPatternIndice, " nPatternsInFold: ", self.nPatternsInFold)
        start = self.fromPatternIndice
        if start == 0:
            if self.nfolds > 1:
                self.images = None
                self.labels = None
                # print('Start GC...')
                gc.collect()
                # print('...done')
                (self.images, self.labels) = self.loadFold(self.currentFoldIndice)
            
        # Shuffle for the first epoch
        if start == 0 and shuffle:
            perm0 = numpy.arange(self.nPatternsInFold)
            numpy.random.shuffle(perm0)
            self.images = self.images[perm0]
            self.labels = self.labels[perm0]
        if start + batch_size > self.nPatternsInFold:
            # Go to the next fold
            # Get the rest examples in this epoch
            # print('last batch in fold fold indice:', self.currentFoldIndice,
            #       ' nfolds in set: ' , len(self.folds), ' remaining: ', self.nPatternsInFold - start)
            images_rest_part = self.images[start:self.nPatternsInFold]
            labels_rest_part = self.labels[start:self.nPatternsInFold]
            self.currentFoldIndice += 1
            if self.currentFoldIndice == len(self.folds):
                print('set currentFoldIndice to 0 self.epochCompleted = True')
                self.currentFoldIndice = 0
                self.epochCompleted = True
            # else:
                # print('This is not the last fold.')
            self.fromPatternIndice = 0
            return images_rest_part, labels_rest_part
        else:
            self.fromPatternIndice += batch_size
            end = self.fromPatternIndice
            return self.images[start:end], self.labels[start:end]
        
            

def read_data_sets_folds(
                train_dir,
                nfolds,
                imagesUbytePrefix,
                labelsUbytePrefix,
                nbclasses=10,
                one_hot=False,
                # if dtype is float32 then values are rescaled between
                # 0.0 and 1,0
                dtype=dtypes.float32,
                reshape=False,        # passed to DataSet constructor
                seed=None,            # passed to DataSet constructor
                source_url=DEFAULT_SOURCE_URL,
                imageOnOutput=False):
    #----------------------------------------------------------------------------
    print('imageOnOutput: ' + str(imageOnOutput))
    if not source_url:  # empty string check
        source_url = DEFAULT_SOURCE_URL
    options = dict(dtype=dtype, reshape=reshape, seed=seed, imageOnOutput=imageOnOutput)
    # special case where all available data is used for training
    if validation_images == None and test_images == None:
        (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                      source_url, imageOnOutput, nbclasses, one_hot)
        train = DataSet(train_images, train_labels, **options)
        return base.Datasets(train=train, validation=train, test=None)
    # Load training set----------------------------------------------------------
    (train_images, train_labels) = load_ubyte(train_images, train_labels, train_dir,
                                              source_url, imageOnOutput, nbclasses, one_hot)
    # Load validation set--------------------------------------------------------
    (validation_images, validation_labels) = load_ubyte(validation_images, validation_labels, train_dir,
                                                        source_url, imageOnOutput, nbclasses, one_hot)
    # Load test set--------------------------------------------------------------
    (test_images, test_labels) = load_ubyte(test_images, test_labels, train_dir,
                                            source_url, imageOnOutput, nbclasses, one_hot)
    #----------------------------------------------------------------------------
    if train_images is None:
        train = None
    else:
        train = DataSet(train_images, train_labels, **options)
    if validation_images is None:
        validation = None
    else:
        validation = DataSet(validation_images, validation_labels, **options)
    if test_images is None:
        test = None
    else:
        test = DataSet(test_images, test_labels, **options)
    return base.Datasets(train=train, validation=validation, test=test)

#================================================================================


def loadCSVTestData(imagesCSVFile,
                    dtype=dtypes.float32,
                    reshape=False,
                    seed=None,
                    imageOnOutput=False,
                    delimiter=',',
                    num_images=0,
                    rows=0,
                    cols=0,
                    rescale=True):
    images = genfromtxt(imagesCSVFile, delimiter=delimiter)
    images = images.reshape(num_images, rows, cols, 1)
    # labels = genfromtxt(labels, delimiter=delimiter)
    options = dict(dtype=dtype, reshape=reshape, seed=seed, rescale=rescale)
    test = DataSet(images, None, **options)
    return test


def saveStatus(expname, status):
    try:
        with open((expname + '.json'), 'w') as f:
            f.write(json.dumps(status))
            f.close()
    except:
        print(str(datetime.datetime.now()) + ' Could not save status.')

def loadStatus(expname):
    try:
        with open((expname + '.json'), 'r') as f:
            return json.loads(f.read())
    except:
        print(str(datetime.datetime.now()) + ' Could not load status.')
