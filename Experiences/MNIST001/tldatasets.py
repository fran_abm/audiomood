# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Converts MNIST data to TFRecords file format with Example protos."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

#import argparse
import os
#import sys

import tensorflow as tf

# from tensorflow.contrib.learn.python.learn.datasets import mnist

FLAGS = None

def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def convert_to(data_set, name):
  """Converts a dataset to tfrecords."""
  images = data_set.images
  labels = data_set.labels
  num_examples = data_set.num_examples
  if images.shape[0] != num_examples:
    raise ValueError('Images size %d does not match label size %d.' %
                     (images.shape[0], num_examples))
  print("image.shape ",images.shape)  
  rows = images.shape[1]
  cols = images.shape[2]
  depth = images.shape[3]
  # images is a tensor <nb-images> x <rows> x <cols> x <channels>
  # filename = os.path.join(FLAGS.directory, name + '.tfrecords')
  filename = os.path.join("/tmp/data/", name + '.tfrecords')
  print('Writing', filename)
  with tf.python_io.TFRecordWriter(filename) as writer:
    for index in range(num_examples):
      image_raw = images[index].tostring()
      example = tf.train.Example(
          features=tf.train.Features(
              feature={
                  'height': _int64_feature(rows),
                  'width': _int64_feature(cols),
                  'depth': _int64_feature(depth),
                  'label': _int64_feature(int(labels[index])),
                  'image_raw': _bytes_feature(image_raw)
              }))
      writer.write(example.SerializeToString())
#==============================================================================
import numpy
from six.moves import xrange  # pylint: disable=redefined-builtin
from tensorflow.python.framework import dtypes

class DataSet(object):
  def __init__(self,
               images,
               labels,
               # fake_data=False,
               # one_hot=False,
               dtype=dtypes.float32,
               # if the objective is to convert to TFrecords, reshape should 
               # be False because the convert-to function expects images to be 
               # a tensor <nb-images> x <rows> x <cols> x <channels> 
               reshape=False,
               seed=None):
    """Construct a DataSet.
    one_hot arg is used only if fake_data is true.  `dtype` can be either
    `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
    `[0, 1]`.  Seed arg provides for convenient deterministic testing.
    """
    print("in DataSet constructor")
    seed1, seed2 = random_seed.get_seed(seed)
    # If op level seed is not set, use whatever graph level seed is returned
    numpy.random.seed(seed1 if seed is None else seed2)
    dtype = dtypes.as_dtype(dtype).base_dtype
    if dtype not in (dtypes.uint8, dtypes.float32):
      raise TypeError('Invalid image dtype %r, expected uint8 or float32' %
                      dtype)
    # Will we use fake data ?
    # if fake_data:
    #   self._num_examples = 10000
    #   self.one_hot = one_hot
    #  else:
    if labels is not None:
      assert images.shape[0] == labels.shape[0], (
        'images.shape: %s labels.shape: %s' % (images.shape, labels.shape))
    self._num_examples = images.shape[0]
      # Convert shape from [num examples, rows, columns, depth]
      # to [num examples, rows*columns] (assuming depth == 1)
      #                                  *******************
    if reshape:
        assert images.shape[3] == 1
        print("reshaping images -> ",images.shape[0],"(# patterns)x",images.shape[1] * images.shape[2],"(linearized size of image)")
        images = images.reshape(images.shape[0],
                                images.shape[1] * images.shape[2])
    else:
        print("no image reshaping.")
    print("images shape", images.shape)
    if dtype == dtypes.float32:
        print("dtype = ", dtype, " rescaling-> [0.0, 1.0].")
        # Convert from [0, 255] -> [0.0, 1.0].
        images = images.astype(numpy.float32)
        images = numpy.multiply(images, 1.0 / 255.0)
    else:
        print("dtype = ", dtype, "no rescaling.")
    self._images = images
    self._labels = labels
    self._epochs_completed = 0
    self._index_in_epoch = 0

  @property
  def images(self):
    return self._images

  @property
  def labels(self):
    return self._labels

  @property
  def num_examples(self):
    return self._num_examples

  @property
  def epochs_completed(self):
    return self._epochs_completed

  def next_batch(self, batch_size, fake_data=False, shuffle=True):
    """Return the next `batch_size` examples from this data set."""
    # Not sure we will use fake data. Anyway 784 = 28*28 why a constant here ?
    if fake_data:
      fake_image = [1] * 784
      if self.one_hot:
          # MNIST has ten classes
        fake_label = [1] + [0] * 9
      else:
        fake_label = 0
      return [fake_image for _ in xrange(batch_size)], [
          fake_label for _ in xrange(batch_size)
      ]
    start = self._index_in_epoch
    # Shuffle for the first epoch
    if self._epochs_completed == 0 and start == 0 and shuffle:
      perm0 = numpy.arange(self._num_examples)
      numpy.random.shuffle(perm0)
      self._images = self.images[perm0]
      self._labels = self.labels[perm0]
    # Go to the next epoch
    if start + batch_size > self._num_examples:
      # Finished epoch
      self._epochs_completed += 1
      # Get the rest examples in this epoch
      rest_num_examples = self._num_examples - start
      images_rest_part = self._images[start:self._num_examples]
      labels_rest_part = self._labels[start:self._num_examples]
      # Shuffle the data
      if shuffle:
        perm = numpy.arange(self._num_examples)
        numpy.random.shuffle(perm)
        self._images = self.images[perm]
        self._labels = self.labels[perm]
      # Start next epoch
      start = 0
      self._index_in_epoch = batch_size - rest_num_examples
      end = self._index_in_epoch
      images_new_part = self._images[start:end]
      labels_new_part = self._labels[start:end]
      return numpy.concatenate((images_rest_part, images_new_part), axis=0) , numpy.concatenate((labels_rest_part, labels_new_part), axis=0)
    else:
      self._index_in_epoch += batch_size
      end = self._index_in_epoch
      return self._images[start:end], self._labels[start:end]
#==============================================================================    
import gzip
def _read32(bytestream):
  dt = numpy.dtype(numpy.uint32).newbyteorder('>')
  return numpy.frombuffer(bytestream.read(4), dtype=dt)[0]

def extract_images(f):
  """Extract the images into a 4D uint8 numpy array [index, y, x, depth].

  Args:
    f: A file object that can be passed into a gzip reader.

  Returns:
    data: A 4D uint8 numpy array [index, y, x, depth].

  Raises:
    ValueError: If the bytestream does not start with 2051.

  """
  print('Extracting', f.name)
  with gzip.GzipFile(fileobj=f) as bytestream:
    magic = _read32(bytestream)
    if magic != 2051:
      raise ValueError('Invalid magic number %d in MNIST image file: %s' %
                       (magic, f.name))
    num_images = _read32(bytestream)
    rows = _read32(bytestream)
    cols = _read32(bytestream)
    buf = bytestream.read(rows * cols * num_images)
    data = numpy.frombuffer(buf, dtype=numpy.uint8)
    data = data.reshape(num_images, rows, cols, 1)
    return data

def dense_to_one_hot(labels_dense, num_classes):
  """Convert class labels from scalars to one-hot vectors."""
  num_labels = labels_dense.shape[0]
  index_offset = numpy.arange(num_labels) * num_classes
  labels_one_hot = numpy.zeros((num_labels, num_classes))
  labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
  return labels_one_hot

def extract_labels(f, num_classes, one_hot=False):
  """Extract the labels into a 1D uint8 numpy array [index].

  Args:
    f: A file object that can be passed into a gzip reader.
    one_hot: Does one hot encoding for the result.
    num_classes: Number of classes for the one hot encoding.

  Returns:
    labels: a 1D uint8 numpy array.

  Raises:
    ValueError: If the bystream doesn't start with 2049.
  """
  print('Extracting', f.name)
  with gzip.GzipFile(fileobj=f) as bytestream:
    magic = _read32(bytestream)
    if magic != 2049:
      raise ValueError('Invalid magic number %d in MNIST label file: %s' %
                       (magic, f.name))
    num_items = _read32(bytestream)
    buf = bytestream.read(num_items)
    labels = numpy.frombuffer(buf, dtype=numpy.uint8)
    if one_hot:
      print("one_hot is True")
      return dense_to_one_hot(labels, num_classes)
    print("one_hot is False")
    return labels
#==============================================================================
# from tensorflow.contrib.learn.python.learn.datasets import base
import base as base
from tensorflow.python.framework import random_seed
from tensorflow.python.platform import gfile

DEFAULT_SOURCE_URL = 'https://storage.googleapis.com/cvdf-datasets/mnist/'

def read_data_sets(train_dir,
                   train_images='train-images-idx3-ubyte.gz',
                   train_labels='train-labels-idx1-ubyte.gz',
                   validation_images='validation-images-idx3-ubyte.gz',
                   validation_labels='validation-labels-idx1-ubyte.gz',
                   test_images=None,
                   test_labels=None,
                   nbclasses=10,
                   # fake_data=False
                   # one_hot should be False if the objective is to convert to 
                   # TFrecords else it should be True
                   one_hot=False,
                   # if dtype is float32 then values are rescaled between
                   # 0.0 and 1,0
                   dtype=dtypes.float32, # passed to Dataset constructor
                   # If the objective is to convert to TFRecords reshape should 
                   # be False else it should be True.
                   reshape=False,        # passed to DataSet constructor
                   validation_size=0,
                   seed=None,            # passed to DataSet constructor
                   source_url=DEFAULT_SOURCE_URL):
  #----------------------------------------------------------------------------
  #if fake_data:
  #  def fake():
  #    return DataSet(
  #        [], [], fake_data=True, one_hot=one_hot, dtype=dtype, seed=seed)
  #  train = fake()
  #  validation = fake()
  #  test = fake()
  #  return base.Datasets(train=train, validation=validation, test=test)
  #----------------------------------------------------------------------------
  if not source_url:  # empty string check
    source_url = DEFAULT_SOURCE_URL
  # Load training set
  if train_images is not None:
    local_file = base.maybe_download(train_images, train_dir,
                                     source_url + train_images)
    with gfile.Open(local_file, 'rb') as f:
      train_images = extract_images(f)
  # 
  if train_labels is not None:
    local_file = base.maybe_download(train_labels, train_dir,
                                     source_url + train_labels)
    with gfile.Open(local_file, 'rb') as f:
      train_labels = extract_labels(f, nbclasses, one_hot=one_hot)

  # Load validation set
  if validation_images is not None:
    local_file = base.maybe_download(validation_images, train_dir,
                                     source_url + validation_images)
    with gfile.Open(local_file, 'rb') as f:
      validation_images = extract_images(f)
  if validation_labels is not None:
    local_file = base.maybe_download(validation_labels, train_dir,
                                     source_url + validation_labels)
    with gfile.Open(local_file, 'rb') as f:
      validation_labels = extract_labels(f, nbclasses, one_hot=one_hot)
  # Load test set
  if test_images is not None:
    local_file = base.maybe_download(test_images, train_dir,
                                     source_url + test_images)
    with gfile.Open(local_file, 'rb') as f:
      test_images = extract_images(f)
  if test_labels is not None:
    local_file = base.maybe_download(test_labels, train_dir,
                                     source_url + test_labels)
    with gfile.Open(local_file, 'rb') as f:
      test_labels = extract_labels(f, nbclasses, one_hot=one_hot)
  #
  options = dict(dtype=dtype, reshape=reshape, seed=seed)
  if train_images is None:
    train = None
  else:
    train = DataSet(train_images, train_labels, **options)
  if validation_images is None:
    validation = None
  else:
    validation = DataSet(validation_images, validation_labels, **options)
  if test_images is None:
    test = None
  else:
    test = DataSet(test_images, test_labels, **options)
  return base.Datasets(train=train, validation=validation, test=test)
#==============================================================================
      
# def main(unused_argv):
#  # Get the data from the disk or from the net.
#  # The data is in MNIST format.
#  # TODO: make a version of mnist.read_data_sets() for any dataset.
#  data_sets = mnist.read_data_sets(FLAGS.directory,
#                                   dtype=tf.uint8,
#                                   reshape=False,
#                                   validation_size=FLAGS.validation_size)
#  # Convert to Examples and write the result to TFRecords.
#  convert_to(data_sets.train, 'train')
#  convert_to(data_sets.validation, 'validation')
#  convert_to(data_sets.test, 'test')


def runConvert(dir="/tmp/data/", 
        validation_size=1000,
        source_url="",
        train_images='train-images-idx3-ubyte.gz',
        train_labels='train-labels-idx1-ubyte.gz',
        test_images='t10k-images-idx3-ubyte.gz',
        test_labels='t10k-labels-idx1-ubyte.gz'):
  data = read_data_sets(dir,
                        # if dtype is float32 then values are rescaled between
                        # 0.0 and 1,0
                        dtype=tf.float32,
                        # reshape should be False because the convert-to 
                        # function expects images to be a tensor <nb-images> x 
                        # <rows> x <cols> x <channels> 
                        reshape=False,
                        # one_hot should be False if the objective is to 
                        # convert to TFrecords
                        one_hot = False,
                        validation_size=validation_size,
                        source_url=source_url,
                        train_images=train_images,
                        train_labels=train_labels,
                        test_images=test_images,
                        test_labels=test_labels)
  # Convert to Examples and write the result to TFRecords.
  convert_to(data.train, 'train')
  convert_to(data.validation, 'validation')
  convert_to(data.test, 'test')

  
# WHAT is this code ???!!!
# if __name__ == '__main__':
#   parser = argparse.ArgumentParser()
#   parser.add_argument(
#       '--directory',
#       type=str,
#       default='/tmp/data',
#       help='Directory to download data files and write the converted result'
#   )
#   parser.add_argument(
#       '--validation_size',
#       type=int,
#       default=5000,
#       help="""\
#       Number of examples to separate from the training data for the validation
#       set.\
#       """
#   )
#   FLAGS, unparsed = parser.parse_known_args()
#   tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
