Experiência YT001

Criei um conjunto de datasets, baseado em músicas retiradas do youtube. Estas músicas foram escolhidas ao acaso, de duas playlists - uma de músicas "felizes" e outra de músicas "tristes".
Assim, este dataset tem apenas dois outputs possíveis - happy ou sad.

Os datasets de treino têm 13 músicas de cada uma das classificacões (13 músicas felizes, e 13 músicas tristes). Ao todo, existem cerca de 6000 imagens no dataset de treino. Cada imagem tem o tamanho de 512x32
Foi utilizada e convertida a música toda.
Na criação do dataset, primeiro foram inseridas as músicas felizes e depois as tristes, não havendo um shuffle posterior.

O modelo para testar o dataset foi uma adaptação de um utilizado para o MNIST. Tem 3 camadas de convolução e duas camadas Fully Connected.
As camadas de convolução têm um número crescente de filtros (começa com 16, e a terceira camada tem 64). O tamanho do filtro é 5x5.
Entre cada camada de convolução existe uma camada de pooling. A primeira camada tem um filtro 8x1, que torna a imagem em 64x32. A segunda camada de pooling tem um filtro 2x1 - passando a imagem para 32x32, e a última camada
tem um filtro 2x2, passando a imagem para a camada FC como uma imagem 16x16.

A primeira camada FC tem 1024 nós, e a segunda tem 2 - sendo o output.

O treino foi feito com 50 epochs. O resultado da precisão após o treino de cada epoch:
[1, "0.48535156"], [2, "0.5"], [3, "0.5097656"], [4, "0.62109375"], [5, "0.65625"], [6, "0.6738281"], [7, "0.6796875"], [8, "0.7011719"], [9, "0.7363281"], [10, "0.7470703"], [11, "0.7558594"], [12, "0.73828125"], [13, "0.74902344"],
[14, "0.79785156"], [15, "0.8125"], [16, "0.7998047"], [17, "0.7919922"], [18, "0.8222656"], [19, "0.81640625"], [20, "0.8486328"], [21, "0.8300781"], [22, "0.828125"], [23, "0.828125"], [24, "0.84472656"], [25, "0.8642578"],
[26, "0.8486328"], [27, "0.859375"], [28, "0.8847656"], [29, "0.83984375"], [30, "0.88183594"], [31, "0.8798828"], [32, "0.88183594"], [33, "0.8964844"], [34, "0.84472656"], [35, "0.87109375"], [36, "0.89453125"], [37, "0.8984375"],
[38, "0.91503906"], [39, "0.92871094"], [40, "0.92871094"], [41, "0.9326172"], [42, "0.9267578"], [43, "0.92871094"], [44, "0.9482422"], [45, "0.9394531"], [46, "0.9199219"], [47, "0.9433594"], [48, "0.9501953"], [49, "0.93847656"],
[50, "0.9501953"]],

O resultado da loss ao longo dos epochs:
"loss": [[1, "5.5077753"], [2, "0.7540106"], [3, "0.7333112"], [4, "0.65179795"], [5, "0.6337669"], [6, "0.6125824"], [7, "0.60039145"], [8, "0.5799441"], [9, "0.5517076"], [10, "0.54472506"], [11, "0.5341992"],
[12, "0.52939844"], [13, "0.5216718"], [14, "0.45960122"], [15, "0.4348142"], [16, "0.43760353"], [17, "0.43834049"], [18, "0.41869777"], [19, "0.40725935"], [20, "0.36178872"], [21, "0.37681225"], [22, "0.3968867"],
[23, "0.3854598"], [24, "0.35394305"], [25, "0.33714214"], [26, "0.33731994"], [27, "0.31679747"], [28, "0.29434222"], [29, "0.3883123"], [30, "0.28732848"], [31, "0.2867244"], [32, "0.27952242"], [33, "0.262612"],
[34, "0.34262002"], [35, "0.28570762"], [36, "0.24863327"], [37, "0.24449007"], [38, "0.21753511"], [39, "0.20370102"], [40, "0.19449618"], [41, "0.19644575"], [42, "0.19007033"], [43, "0.1905594"], [44, "0.16503403"],
[45, "0.16606724"], [46, "0.20364869"], [47, "0.16798554"], [48, "0.14735436"], [49, "0.15593109"], [50, "0.15004641"]

Matriz de confusão do treino:
[125602, 27371]
[29677, 124550]


Precisão da Validação: 0.006013870502296845

Precisão do Teste: 0.004743076731103638