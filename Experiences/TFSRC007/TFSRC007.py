from __future__ import print_function
import itertools
# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
# mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import sys as sys
import tldatasets as tld
import json as json
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import numpy as np
import configparser
import tensorflow as tf
from config import *

# config = configparser.ConfigParser()
# config.read("./config.ini")



# currentFolder = config["Main"]["base_folder"] + config["Main"]["expname"] + "/"
# print("CURRENT FOLDER: ", currentFolder)
# savingName = config["Main"]["expname"] + "-results"
# print("SAVING NAME: ", savingName)



def weight_variable (shape, name, seed):
    """
    Creates a consistent weight variable.

    Args:
        - shape: shape of the output variable.
        - name: name of the output variable. Needed when retraining and testing.
        - seed: seed for the generation of the variable

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.truncated_normal(shape, stddev=0.1, seed=seed)
    return tf.Variable(initial, name=name)

def bias_variable (shape, name):
    """
    Creates a consistent bias variable.

    Args:
        - shape: shape of the bias variable.
        - name: the name of the bias variable. Needed for retraining and testing.

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)

def conv2d (x, W):
    """
    Convolutional operation to a given input, using a given filter.

    Args:
        - x: input, a 4D tensor.
        - W: filter (weight ??), a 4D tensor.
    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def neural_net(input, config):
    weights = {
        'h1': weight_variable(config["layer_1"]["weight"], config["layer_1"]["weight_name"], 11),
        'h2': weight_variable(config["layer_2"]["weight"], config["layer_2"]["weight_name"], 11),
        'h3': weight_variable(config["layer_3"]["weight"], config["layer_3"]["weight_name"], 11),
        'h4': weight_variable(config["layer_4"]["weight"], config["layer_4"]["weight_name"], 11),
        'out': weight_variable(config["layer_5"]["weight"], config["layer_5"]["weight_name"], 11)
    }
    biases = {
        'b1': bias_variable([config["layer_1"]["bias"]], config["layer_1"]["bias_name"]),
        'b2': bias_variable([config["layer_2"]["bias"]], config["layer_2"]["bias_name"]),
        'b3': bias_variable([config["layer_3"]["bias"]], config["layer_3"]["bias_name"]),
        'b4': bias_variable([config["layer_4"]["bias"]], config["layer_4"]["bias_name"]),
        'out': bias_variable([config["layer_5"]["bias"]], config["layer_5"]["bias_name"])
    }

    # layer_ 1
    layer_1 = tf.nn.relu(conv2d(input, weights['h1']) + biases['b1'])
    
    # layer_ 2
    layer_2 = tf.nn.relu(conv2d(tf.nn.max_pool(layer_1, 
        ksize = config["layer_2"]["pool"], 
        strides = config["layer_2"]["pool_strides"], 
        padding = 'SAME'), 
        weights['h2']) + biases['b2'])

    layer_3 = tf.nn.relu(conv2d(tf.nn.max_pool(layer_2, 
        ksize = config["layer_3"]["pool"],
        strides = config["layer_3"]["pool_strides"], 
        padding = 'SAME'),
        weights['h3']) + biases['b3'])
   
    layer_4 = tf.nn.relu(tf.matmul(tf.reshape(tf.nn.max_pool(layer_3, 
        ksize = config["layer_4"]["pool"], 
        strides = config["layer_4"]["pool_strides"], 
        padding = 'SAME'), config["layer_4"]["reshape"]), 
        weights['h4']) + biases['b4'])
   
    # Output fully connected layer with a neuron for each class
    out_layer = (tf.matmul(layer_4, weights['out']) + biases['out'])
    netoutput = tf.identity(out_layer, name="NETOUTPUT")
    return netoutput

def training(list_epochs, input_image, input_labels, config):
    '''
    This functions creates and trains several models, based on the number of epochs given, or the different models given.

    Args:
        - list_epochs: list, with the different epochs.
    '''

    for j in list_epochs:
        logits = neural_net(input_image, config)
        prediction = tf.nn.softmax(logits)
        just_class_number = tf.argmax(prediction, axis=1, name="class_number")

        # Define loss and optimizer
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(
            logits=logits, labels=input_labels))
        optimizer = tf.train.AdamOptimizer(learning_rate=float(config["network"]["learning_rate"]))
        train_op = optimizer.minimize(cost)

        # Evaluate model
        correct_prediction = tf.equal(just_class_number, tf.argmax(input_labels, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")


        validation_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, sizeOfVal)))), [sizeOfVal, 1]), tf.reshape(tf.cast(tf.argmax(logits, axis=1), tf.int32), [sizeOfVal, 1])], 1), tf.logical_not(correct_prediction))
        test_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, sizeOfVal)))), [sizeOfVal, 1]), tf.reshape(tf.cast(tf.argmax(logits, axis=1), tf.int32), [sizeOfVal, 1])], 1), tf.logical_not(correct_prediction))

        # This is a dictionary containing the options made for this experience.
        results = {'epochs':j, 'optimizer':'ADAM-OPTIMIZER', 'costFunction':'SOFTMAX-CROSS-ENTROPY-WITH-LOGITS', 
                    'training-accuracy':[], 'final-validation-accuracy':0, "name": config["main"]["name"], "dataName": config["main"]["train_data"], "labelName": config["main"]["train_label"], 
                    'dataPoints': "", 'labelPoints': '', 'dataPointsImage': '', 'imageSize': config["main"]["image_size"], 'learningStep': config["network"]["learning_rate"], 
                    'batchSize': config["network"]["batch_size"], 'numLayers': config['network']['num_layers'], 'numInputs':num_input, 'numClasses':config["network"]["num_classes"], "loss": []}

        init = tf.global_variables_initializer()

        saver = tf.train.Saver(max_to_keep=3)

        start_time = time.time()

        with tf.Session() as sess:

            tf.set_random_seed(1)
            sess.run(init)
            tf.add_to_collection('train_step', train_op)
            tf.add_to_collection('test_bads', test_bads)
            pat = 0 # Current number of images passed through the model.
            epoch = 0 # Current passage through the dataset.
            stop = False
            validationAccuracy = 0 
            predictions = []
            inputLabels = []

            results['dataPoints'] = len(dataset.train.images)
            results['labelPoints'] = len(dataset.train.labels)
            results['dataPointsImage'] = len(dataset.train.images[0])

            num_images = results["dataPoints"]
            
            while not stop:
                batch_x, batch_y = dataset.train.next_batch(int(config["network"]["batch_size"]))
                pat = pat + int(config["network"]["batch_size"])

                # Run optimization op (backprop)
                train_o, class_numb = sess.run([train_op, just_class_number], feed_dict={x: batch_x, y_: batch_y})
                predictions = np.append(predictions, class_numb)
                inputLabels = np.append(inputLabels, tf.argmax(batch_y, axis=1).eval())
                if pat >= num_images:
                    pat = 0
                    epoch = epoch + 1
                    if epoch >= int(j):
                        stop = True
                    
                    loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x, y_: batch_y})
                    results['training-accuracy'] = results['training-accuracy'] + [[epoch, str(acc)]]
                    print (('step %d, training accuracy %g' % (epoch, acc)))
                    if (0 == (epoch % 1)):
                        results['loss'].append([epoch, str(loss)])
                        expstatus['status'] = 'running'
                        expstatus['progress'] = (epoch / int(j))
                        with open((config["main"]["name"] + '.json'), 'w') as f:
                            f.write(json.dumps(expstatus))
                            f.close()

            print("Optimization Finished!")

            print("Confusion Matrix")
            confMatrix = tf.confusion_matrix(inputLabels, predictions)
            results["confusion-matrix"] = confMatrix.eval().tolist()
            print(confMatrix.eval())


            # Validation
            validationBads = 0
            validationAccuracy = accuracy.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
            validationBads = validation_bads.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
            print (('validation accuracy %g' % validationAccuracy))
            
            # Save the number of epochs done.
            sess.run(tf.assign(git, (git + int(j))))
            
            # # Save the graph.
            saver.save(sess, currentFolder + config["main"]["name"], global_step=epoch, write_meta_graph=True)

            # Write final results.
            results['final-validation-accuracy'] = str(validationAccuracy)
            results['duration'] = math.ceil((time.time() - start_time))
            results['hostname'] = socket.gethostname()
            
            # Memory Usage.
            res = resource.getrusage(resource.RUSAGE_SELF)
            results['maxmem'] = res.ru_maxrss
            # results['validation-bads'] = validationBads.tolist()

            # Calculate accuracy for MNIST test images
            val_acc = sess.run(accuracy, feed_dict={x: dataset.validation.images, y_: dataset.validation.labels})
            results["val_acc"] = str(val_acc)
            print("Validation Accuracy:", str(val_acc))
                

            with open(config['main']['saving_name'] + '/' + str(j) +'.json', 'w') as f:
                f.write(json.dumps(results))
                f.close()

            expstatus['status'] = 'finished'
            with open((config["main"]["name"] + '.json'), 'w') as f:
                f.write(json.dumps(expstatus))
                f.close()


if __name__ == "__main__":

    with open('config.json') as j:
        config = json.loads(j.read())

    print('Config File Loaded')
    expstatus = {'status': 'running'}

    # Writes to file.
    with open((config["main"]["name"] + '.json'), 'w') as f:
        f.write(json.dumps(expstatus))
        f.close()
    list_epochs = [1,3,5]
    if config['main']['is_local'] == 'True':
        dataset = tld.read_data_sets(train_dir=config["main"]["local_folder"], train_images=config["main"]["train_data"], 
                            train_labels=config["main"]["train_label"], test_images=config["main"]["test_data"], 
                            test_labels=config["main"]["test_label"], validation_images=config["main"]["validation_data"], 
                            validation_labels=config["main"]["validation_label"], one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/TF/data/', nbclasses=int(config["network"]["num_classes"]), validation_size=0)
    else:
        dataset = tld.read_data_sets(train_dir=config["main"]["datacenter_dataset_folder"], train_images=config["main"]["train_data"], 
                            train_labels=config["main"]["train_label"], test_images=config["main"]["test_data"], 
                            test_labels=config["main"]["test_label"], validation_images=config["main"]["validation_data"], 
                            validation_labels=config["main"]["validation_label"], one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/TF/data/', nbclasses=int(config["network"]["num_classes"]), validation_size=0)
    
    num_input = dataset.train.images.shape[1]
    sizeOfVal = dataset.validation.labels.shape[0]

    x = tf.placeholder("float", [None, num_input], name="x")
    y_ = tf.placeholder("float", [None, int(config["network"]["num_classes"])], name="y_")
    git = tf.Variable(0, name='git')
    x_image = tf.reshape(x, [-1, 128, 44, 1]) # Reshape of the images into a 4D tensor.

    training(list_epochs, x_image, y_, config)