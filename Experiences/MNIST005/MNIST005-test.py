#
# Experience 003 - Model testing.
#
#

import sys as sys
import tldatasets as tld
import json as json
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import tensorflow as tf

# This sets the python folder on the datacenter. If testing locally, comment this out.
sys.path = (sys.path + [('/home/hpc/tl/' + 'PointCentral/python/')])

# The experience name needs to be the same name of this file, and of the 
# zipped folder containing the files.
expname = 'MNIST003'

# /home/hpc/tl/tmp/mBR/

base_folder = "/home/hpc/tl/tmp/"

currentFolder = base_folder + expname + "/"

print("Current Folder = " + currentFolder)

# /home/hpc/tl/Datasets/MNIST_data
datasetFolder = "/home/hpc/tl/Datasets/MNIST_data"

savingName = expname + ".mBR"

# Number of images that will be evalutated.
numbOfImages = 60000

# Number of Epochs
numbOfEpochs = 3

# Data set creation.
# Change train_dir if testing locally, to a directory of your choosing (/home/hpc/tl/Datasets/MNIST_data)
dataset = tld.read_data_sets(train_dir=datasetFolder, train_images='train-images-idx3-ubyte.gz', 
                            train_labels='train-labels-idx1-ubyte.gz', test_images='t10k-images-idx3-ubyte.gz', 
                            test_labels='t10k-labels-idx1-ubyte.gz', validation_images='t10k-images-idx3-ubyte.gz', 
                            validation_labels='t10k-labels-idx1-ubyte.gz', one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/data/', validation_size=0)


with open(savingName + '.json', 'r') as f:
    results = json.loads(f.read())

with open((expname + '.json'), 'r') as f:
    expstatus = json.loads(f.read())

expstatus['status'] = 'running'
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()

file = open(currentFolder + 'checkpoint', 'r')
line = file.readline()
modelfile = (line.split('"')[1] + '.meta')
file.close()
sess = tf.Session()
new_saver = tf.train.import_meta_graph(currentFolder + modelfile)
new_saver.restore(sess, tf.train.latest_checkpoint(currentFolder))
graph = tf.get_default_graph()
x = graph.get_tensor_by_name('x:0')
y_ = graph.get_tensor_by_name('y_:0')
git = graph.get_tensor_by_name('git:0')
x_image = tf.reshape(x, [-1, 28, 28, 1])
w1 = graph.get_tensor_by_name('W1:0')
b1 = graph.get_tensor_by_name('B1:0')
w2 = graph.get_tensor_by_name('W2:0')
b2 = graph.get_tensor_by_name('B2:0')
w3 = graph.get_tensor_by_name('W3:0')
b3 = graph.get_tensor_by_name('B3:0')
w4 = graph.get_tensor_by_name('W4:0')
b4 = graph.get_tensor_by_name('B4:0')
start_time = time.time()
class_number = graph.get_tensor_by_name('class_number:0')
pat = 0
epoch = 0
classNumbers = class_number.eval(feed_dict={x:dataset.test.images}, session=sess)
results['test-class-numbers'] = classNumbers.tolist()
results['test-filename'] = 't10k-images-idx3-ubyte.gz'
results['duration'] = math.ceil((time.time() - start_time))
results['hostname'] = socket.gethostname()
res = resource.getrusage(resource.RUSAGE_SELF)
results['maxmem'] = res.ru_maxrss
with open(savingName + '.json', 'w') as f:
    f.write(json.dumps(results))
    f.close()

expstatus['status'] = 'finished'
print (w4.eval(session=sess))
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()
