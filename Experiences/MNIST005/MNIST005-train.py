#
# Experience 003 - Model creation and training.
#
#

import sys as sys
import tldatasets as tld
import json as json
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import tensorflow as tf
import numpy as np

# This sets the python folder on the datacenter. If testing locally, comment this out.
sys.path = (sys.path + [('/home/hpc/tl/' + 'PointCentral/python/')])

# The experience name needs to be the same name of this file, and of the 
# zipped folder containing the files.
expname = 'MNIST003'

# Folder, where the expirience will be run.


# /home/hpc/tl/tmp/
base_folder = "/home/hpc/tl/tmp/"

currentFolder = base_folder + expname + "/"

print("Current Folder = " + currentFolder)

# /home/hpc/tl/Datasets/MNIST_data
datasetFolder = "/home/hpc/tl/Datasets/MNIST_data"

savingName = expname + "-results"

# Number of images that will be evalutated.
numbOfImages = 60000

# Number of Epochs
numbOfEpochs = 3

# Data set creation.
# Change train_dir if testing locally, to a directory of your choosing (/home/hpc/tl/Datasets/MNIST_data)
dataset = tld.read_data_sets(train_dir=datasetFolder, train_images='train-images-idx3-ubyte.gz', 
                            train_labels='train-labels-idx1-ubyte.gz', test_images='t10k-images-idx3-ubyte.gz', 
                            test_labels='t10k-labels-idx1-ubyte.gz', validation_images='t10k-images-idx3-ubyte.gz', 
                            validation_labels='t10k-labels-idx1-ubyte.gz', one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/data/', validation_size=0)

# # Tries to read a experience status file, if it exists.
# #  If it does exists, this means that a experience has already been run.
# with open((expname + '.json'), 'r') as f:
#     expstatus = json.loads(f.read())

# Creates a experience status file, and sets the status to running.
expstatus = {'status': 'running'}

# Writes to file.
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()

#
# Global Functions
#

def weight_variable (shape, name, seed):
    """
    Creates a consistent weight variable.

    Args:
        - shape: shape of the output variable.
        - name: name of the output variable. Needed when retraining and testing.
        - seed: seed for the generation of the variable

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.truncated_normal(shape, stddev=0.1, seed=seed)
    return tf.Variable(initial, name=name)

def bias_variable (shape, name):
    """
    Creates a consistent bias variable.

    Args:
        - shape: shape of the bias variable.
        - name: the name of the bias variable. Needed for retraining and testing.

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)

def conv2d (x, W):
    """
    Convolutional operation to a given input, using a given filter.

    Args:
        - x: input, a 4D tensor.
        - W: filter (weight ??), a 4D tensor.
    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


#
# Placeholder creation, and dataset reshape.
#

# First, two placeholders, one for the data and one for the labels are created.
x = tf.placeholder(tf.float32, shape=[None, 784], name='x') # Data
y_ = tf.placeholder(tf.float32, shape=[None, 10], name='y_') # Labels
git = tf.Variable(0, name='git') # ??
x_image = tf.reshape(x, [-1, 28, 28, 1]) # Reshape of the images into a 4D tensor.

# First Layer

# 5x5 convolution. The size of the filter being passed through the image.
# 1 input image
# 32 possible outputs - but why 32?
w1 = weight_variable([5, 5, 1, 32], 'W1', 11)

# The same size of the outputs
b1 = bias_variable([32], 'B1')

y1 = (conv2d(x_image, w1) + b1) # Why the add??
# Output Shape: (_, 28,28,32)


# The ReLU operation will transform the possible outputs to the following:
#   - if x < 0 -> 0
#   - if x => 0 -> x
a1 = tf.nn.relu(y1) #ReLu operation.
# Output Shape: (_, 28, 28, 32)

# Second layer

# MaxPool from the output of the last ReLU.
# The size of the output tensor will chane, given the size of the window of the max pool.
mp2 = tf.nn.max_pool(a1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
# Output Shape: (_, 14, 14, 32)

# The weight variable needs to change, as the number of possible outputs changed.
w2 = weight_variable([5, 5, 32, 16], 'W2', 11)
b2 = bias_variable([16], 'B2')
y2 = (conv2d(mp2, w2) + b2)
# Output Shape: (_, 14,14,16)

a2 = tf.nn.relu(y2)

# Third Layer
mp3 = tf.nn.max_pool(a2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
# Output Shape: (_,7,7,16)

w3 = weight_variable([784, 1024], 'W3', 11)
b3 = bias_variable([1024], 'B3')
in_flat3 = tf.reshape(mp3, [-1, 784])
# Output Shape: (_, 784)

y3 = (tf.matmul(in_flat3, w3) + b3)
# Output Shape: (_, 1024)

a3 = tf.nn.relu(y3)

# 1024 outputs, 10 possible labels. 
w4 = weight_variable([1024, 10], 'W4', 11)
b4 = bias_variable([10], 'B4')
y4 = (tf.matmul(a3, w4) + b4)

# Assign a name to the output graph, to be able to load afterwards.
netOutput = tf.identity(y4, name='NETOUTPUT')

# ArgMax will return the largest number in an axis of a tensor. 
just_class_number = tf.argmax(y4, axis=1, name='class_number')

# This will get the average error of the classification
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=netOutput))

# Then we optimize the graph, given the step (0.001).
# We initialize the optimizer;
optimizer = tf.train.AdamOptimizer(0.001)
# And then train for the minimal value of the graph, given the cost.
train_step = optimizer.minimize(cost)

# Here, we check if the prediction made from the Network was correct.
correct_prediction = tf.equal(just_class_number, tf.argmax(y_, 1))

# And update the accuracy of the model.
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')

# Here we make two tensors containing the bad classifications from the validation and test.
validation_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, 10000)))), [10000, 1]), tf.reshape(tf.cast(tf.argmax(y4, axis=1), tf.int32), [10000, 1])], 1), tf.logical_not(correct_prediction))
test_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, 10000)))), [10000, 1]), tf.reshape(tf.cast(tf.argmax(y4, axis=1), tf.int32), [10000, 1])], 1), tf.logical_not(correct_prediction))

# Initialize the global variables.
init = tf.global_variables_initializer()

# This is a dictionary containing the options made for this experience.
results = {'nb-epochs':numbOfEpochs, 'optimizer':'ADAM-OPTIMIZER', 'cost-function':'SOFTMAX-CROSS-ENTROPY-WITH-LOGITS', 
            'training-accuracy':[], 'final-validation-accuracy':0, "name": expname, ""}

# We are only going to save the last 3 graphs of the model.
# We can later change this to save the best 3 graphs.
saver = tf.train.Saver(max_to_keep=3)

# Start the timer.
start_time = time.time()

with tf.Session() as sess:
    tf.set_random_seed(1)
    sess.run(init)
    tf.add_to_collection('train_step', train_step)
    tf.add_to_collection('test_bads', test_bads)
    pat = 0 # Current number of images passed through the model.
    epoch = 0 # Current passage through the dataset.
    stop = False
    validationAccuracy = 0 
    predictions = []
    inputLabels = []
    while not stop:
        batch = dataset.train.next_batch(50)
        pat = (pat + 50)
        
        # It is here where we run the images through the graph.
        # tf.print("Output: ",just_class_number)
        # We feed the images and the labels through a dict, that will be used in the graph.
        train_s, c, pred = sess.run([train_step, cost, just_class_number], feed_dict={x:batch[0], y_:batch[1]})
        predictions = np.append(predictions, pred)
        inputLabels = np.append(inputLabels, tf.argmax(batch[1], axis=1).eval())
        if (pat >= numbOfImages):
            pat = 0
            epoch = (epoch + 1)
            if (epoch >= numbOfEpochs):
                stop = True
            train_accuracy = accuracy.eval(feed_dict={x:batch[0], y_:batch[1]})

            results['training-accuracy'] = (results['training-accuracy'] + [[epoch, str(train_accuracy)]])
            print (('step %d, training accuracy %g' % (epoch, train_accuracy)))
            if (0 == (epoch % 1)):
                expstatus['status'] = 'running'
                expstatus['progress'] = (epoch / numbOfEpochs)
                with open((expname + '.json'), 'w') as f:
                    f.write(json.dumps(expstatus))
                    f.close()

    # Confusion Matrix
    confMatrix = tf.confusion_matrix(inputLabels, predictions)
    results["confusion-matrix"] = confMatrix.eval().tolist()

    # Validation
    validationBads = 0
    validationAccuracy = accuracy.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
    validationBads = validation_bads.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
    print (('validation accuracy %g' % validationAccuracy))
    # Save the number of epochs done.
    sess.run(tf.assign(git, (git + 3)))
    
    # Save the graph.
    saver.save(sess, currentFolder + expname, global_step=epoch, write_meta_graph=True)

    # Write final results.
    results['final-validation-accuracy'] = str(validationAccuracy)
    results['duration'] = math.ceil((time.time() - start_time))
    results['hostname'] = socket.gethostname()
    
    # Memory Usage.
    res = resource.getrusage(resource.RUSAGE_SELF)
    results['maxmem'] = res.ru_maxrss
    results['validation-bads'] = validationBads.tolist()

    with open(savingName +'.json', 'w') as f:
        f.write(json.dumps(results))
        f.close()

    print (('validation accuracy %g' % validationAccuracy))
    expstatus['status'] = 'finished'
    print (w4.eval())
    with open((expname + '.json'), 'w') as f:
        f.write(json.dumps(expstatus))
        f.close()