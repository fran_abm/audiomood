from __future__ import print_function

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import sys as sys
import tldatasets as tld
import json as json
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import tensorflow as tf
import numpy as np



import tensorflow as tf


# Parameters
expname = "MNIST005"
base_folder = "./" # /home/hpc/tl/tmp/
currentFolder = base_folder + expname + "/"
datasetFolder = "./" # /home/hpc/tl/Datasets/MNIST_data
savingName = expname + "-results"
numbOfEpochs = 3
num_layers = 4
learning_rate = 0.1
num_steps = 500
batch_size = 50
print_step = 100

# Network Parameters
n_hidden_1 = 256 # 1st layer number of neurons
n_hidden_2 = 256 # 2nd layer number of neurons
num_input = 784 # MNIST data input (img shape: 28*28)
imageSize = "28*28"
num_classes = 10 # MNIST total classes (0-9 digits)
trainLabels = 'train-labels-idx1-ubyte.gz'
trainImages = 'train-images-idx3-ubyte.gz'



expstatus = {'status': 'running'}

# Writes to file.
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()


# Dataset
dataset = tld.read_data_sets(train_dir="./", train_images=trainImages, 
                            train_labels=trainLabels, test_images='t10k-images-idx3-ubyte.gz', 
                            test_labels='t10k-labels-idx1-ubyte.gz', validation_images='t10k-images-idx3-ubyte.gz', 
                            validation_labels='t10k-labels-idx1-ubyte.gz', one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/data/', validation_size=0)

# Custom Functions
def weight_variable (shape, name, seed):
    """
    Creates a consistent weight variable.

    Args:
        - shape: shape of the output variable.
        - name: name of the output variable. Needed when retraining and testing.
        - seed: seed for the generation of the variable

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.truncated_normal(shape, stddev=0.1, seed=seed)
    return tf.Variable(initial, name=name)

def bias_variable (shape, name):
    """
    Creates a consistent bias variable.

    Args:
        - shape: shape of the bias variable.
        - name: the name of the bias variable. Needed for retraining and testing.

    Returns:
        - A tensorflow Variable.
    """
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)

def conv2d (x, W):
    """
    Convolutional operation to a given input, using a given filter.

    Args:
        - x: input, a 4D tensor.
        - W: filter (weight ??), a 4D tensor.
    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


# tf Graph input
x = tf.placeholder("float", [None, num_input], name="x")
y_ = tf.placeholder("float", [None, num_classes], name="y_")
git = tf.Variable(0, name='git')
x_image = tf.reshape(x, [-1, 28, 28, 1]) # Reshape of the images into a 4D tensor.

# Store layers weight & bias
weights = {
    'h1': weight_variable([5, 5, 1, 32], 'W1', 11),
    'h2': weight_variable([5, 5, 32, 16], 'W2', 11),
    'h3': weight_variable([784, 1024], 'W3', 11),
    'out': weight_variable([1024, 10], 'W4', 11)
}
biases = {
    'b1': bias_variable([32], 'B1'),
    'b2': bias_variable([16], 'B2'),
    'b3': bias_variable([1024], 'B3'),
    'out': bias_variable([10], 'B4')
}

# Create model
def neural_net(x):
    # Layer 1
    layer_1 = tf.nn.relu(conv2d(x_image, weights['h1']) + biases['b1'])
    # Layer 2
    layer_2 = tf.nn.relu(conv2d(tf.nn.max_pool(layer_1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME'), weights['h2']) + biases['b2'])
    # Layer 3
    layer_3 = tf.nn.relu(tf.matmul(tf.reshape(tf.nn.max_pool(layer_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME'), [-1, 784]), weights['h3']) + biases['b3'])
    # Output fully connected layer with a neuron for each class
    out_layer = (tf.matmul(layer_3, weights['out']) + biases['out'])
    netoutput = tf.identity(out_layer, name="NETOUTPUT")
    return netoutput

# Construct model
logits = neural_net(x)
prediction = tf.nn.softmax(logits)
just_class_number = tf.argmax(prediction, axis=1, name="class_number")

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(
    logits=logits, labels=y_))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(cost)

# Evaluate model
correct_prediction = tf.equal(just_class_number, tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")

validation_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, 10000)))), [10000, 1]), tf.reshape(tf.cast(tf.argmax(logits, axis=1), tf.int32), [10000, 1])], 1), tf.logical_not(correct_prediction))
test_bads = tf.boolean_mask(tf.concat([tf.reshape(tf.constant(list((range(0, 10000)))), [10000, 1]), tf.reshape(tf.cast(tf.argmax(logits, axis=1), tf.int32), [10000, 1])], 1), tf.logical_not(correct_prediction))

# This is a dictionary containing the options made for this experience.
results = {'epochs':numbOfEpochs, 'optimizer':'ADAM-OPTIMIZER', 'costFunction':'SOFTMAX-CROSS-ENTROPY-WITH-LOGITS', 
            'training-accuracy':[], 'final-validation-accuracy':0, "name": expname, "dataName": trainImages, "labelName": trainLabels, 
            'dataPoints': "", 'labelPoints': '', 'dataPointsImage': '', 'imageSize': imageSize, 'learningStep': learning_rate, 
            'batchSize': batch_size, 'numLayers': num_layers, 'numInputs':num_input, 'numClasses':num_classes, "loss": []}

init = tf.global_variables_initializer()

saver = tf.train.Saver(max_to_keep=3)

start_time = time.time()

# Initialize the variables (i.e. assign their default value)

# Start training
with tf.Session() as sess:

    tf.set_random_seed(1)
    sess.run(init)
    tf.add_to_collection('train_step', train_op)
    tf.add_to_collection('test_bads', test_bads)
    pat = 0 # Current number of images passed through the model.
    epoch = 0 # Current passage through the dataset.
    stop = False
    validationAccuracy = 0 
    predictions = []
    inputLabels = []

    results['dataPoints'] = len(dataset.train.images)
    results['labelPoints'] = len(dataset.train.labels)
    results['dataPointsImage'] = len(dataset.train.images[0])

    num_images = results["dataPoints"]
    
    while not stop:
        batch_x, batch_y = dataset.train.next_batch(batch_size)
        pat = pat + batch_size
        # Run optimization op (backprop)
        
        train_o, class_numb = sess.run([train_op, just_class_number], feed_dict={x: batch_x, y_: batch_y})
        predictions = np.append(predictions, class_numb)
        inputLabels = np.append(inputLabels, tf.argmax(batch_y, axis=1).eval())
        if pat >= num_images:
            pat = 0
            epoch = epoch + 1
            if epoch >= numbOfEpochs:
                stop = True
            
            loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x, y_: batch_y})
            results['training-accuracy'] = results['training-accuracy'] + [[epoch, str(acc)]]
            print (('step %d, training accuracy %g' % (epoch, acc)))
            if (0 == (epoch % 1)):
                results['loss'].append([epoch, str(loss)])
                expstatus['status'] = 'running'
                expstatus['progress'] = (epoch / numbOfEpochs)
                with open((expname + '.json'), 'w') as f:
                    f.write(json.dumps(expstatus))
                    f.close()

    print("Optimization Finished!")

    print("Confusion Matrix")
    confMatrix = tf.confusion_matrix(inputLabels, predictions)
    results["confusion-matrix"] = confMatrix.eval().tolist()
    print(confMatrix.eval())


     # Validation
    validationBads = 0
    validationAccuracy = accuracy.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
    validationBads = validation_bads.eval(feed_dict={x:dataset.validation.images, y_:dataset.validation.labels})
    print (('validation accuracy %g' % validationAccuracy))
    
    # Save the number of epochs done.
    sess.run(tf.assign(git, (git + numbOfEpochs)))
    
    # # Save the graph.
    saver.save(sess, currentFolder + expname, global_step=epoch, write_meta_graph=True)

    # Write final results.
    results['final-validation-accuracy'] = str(validationAccuracy)
    results['duration'] = math.ceil((time.time() - start_time))
    results['hostname'] = socket.gethostname()
    
    # Memory Usage.
    res = resource.getrusage(resource.RUSAGE_SELF)
    results['maxmem'] = res.ru_maxrss
    results['validation-bads'] = validationBads.tolist()


    # Calculate accuracy for MNIST test images
    val_acc = sess.run(accuracy, feed_dict={x: dataset.validation.images, y_: dataset.validation.labels})
    results["val_acc"] = str(val_acc)
    print("Validation Accuracy:", str(val_acc))
        

    with open(savingName +'.json', 'w') as f:
        f.write(json.dumps(results))
        f.close()

    expstatus['status'] = 'finished'
    with open((expname + '.json'), 'w') as f:
        f.write(json.dumps(expstatus))
        f.close()