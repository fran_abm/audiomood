#
# Experience 003 - Model retraining.
#
#

import sys as sys
import tldatasets as tld
import json as json
import time as time
import math as math
import socket as socket
import csv as csv
import resource as resource
import tensorflow as tf
import numpy as np

# This sets the python folder on the datacenter. If testing locally, comment this out.
sys.path = (sys.path + [('/home/hpc/tl/' + 'PointCentral/python/')])

# The experience name needs to be the same name of this file, and of the 
# zipped folder containing the files.
expname = 'MNIST003'

# /home/hpc/tl/tmp/mBR/

base_folder = "/home/hpc/tl/tmp/mBR/"

currentFolder = base_folder + expname + "/"

print("Current Folder = " + currentFolder)

# /home/hpc/tl/Datasets/MNIST_data
datasetFolder = "/home/hpc/tl/Datasets/MNIST_data"

savingName = expname + ".mBR"
# Number of images that will be evalutated.
numbOfImages = 1000

# Number of Epochs
numbOfEpochs = 3

# Data set creation.
# Change train_dir if testing locally, to a directory of your choosing (/home/hpc/tl/Datasets/MNIST_data)
dataset = tld.read_data_sets(train_dir=datasetFolder, train_images='train-images-idx3-ubyte.gz', 
                            train_labels='train-labels-idx1-ubyte.gz', test_images='t10k-images-idx3-ubyte.gz', 
                            test_labels='t10k-labels-idx1-ubyte.gz', validation_images='t10k-images-idx3-ubyte.gz', 
                            validation_labels='t10k-labels-idx1-ubyte.gz', one_hot=True, reshape=True, seed=11, 
                            source_url='http://tl.di.fc.ul.pt/s/data/', validation_size=0)


with open('MNIST0032.mBR.json', 'r') as f:
    results = json.loads(f.read())

with open((expname + '.json'), 'r') as f:
    expstatus = json.loads(f.read())

expstatus['status'] = 'running'
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()

#
# Model Loading
#

# Reads checkpoint. This will direct to the correct
#  version of the model.
file = open(currentFolder + 'checkpoint', 'r')
line = file.readline()
modelfile = (line.split('"')[1] + '.meta') # Gets the location of the newest model.
print(modelfile)
file.close()

sess = tf.Session()

new_saver = tf.train.import_meta_graph(currentFolder + modelfile) # Imports the model file.
new_saver.restore(sess, tf.train.latest_checkpoint(currentFolder)) # Restores the model
graph = tf.get_default_graph() # Creates the graph

#
# Reloading of the layers of the graph.
#
x = graph.get_tensor_by_name('x:0')
y_ = graph.get_tensor_by_name('y_:0')
git = graph.get_tensor_by_name('git:0')
accuracy = graph.get_tensor_by_name('accuracy:0')
x_image = tf.reshape(x, [-1, 28, 28, 1])
w1 = graph.get_tensor_by_name('W1:0')
b1 = graph.get_tensor_by_name('B1:0')
w2 = graph.get_tensor_by_name('W2:0')
b2 = graph.get_tensor_by_name('B2:0')
w3 = graph.get_tensor_by_name('W3:0')
b3 = graph.get_tensor_by_name('B3:0')
w4 = graph.get_tensor_by_name('W4:0')
b4 = graph.get_tensor_by_name('B4:0')
netOutput = graph.get_tensor_by_name('NETOUTPUT:0')
class_number = graph.get_tensor_by_name("class_number:0")

#
# Cost and Optimizing functions.
#
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=netOutput))
optimizer = tf.train.AdamOptimizer(0.001)

pat = 0
epoch = git.eval(session=sess)
epoch = epoch.item()
nbEpochs = (numbOfEpochs + epoch)
stop = False
validationAccuracy = 0
start_time = time.time()
train_step = tf.get_collection('train_step')[0]

allPred = []
inputLabels = []

while not stop:
    batch = dataset.train.next_batch(50)
    pat = (pat + 50)
    train_s, c, pred = sess.run([train_step, cost, class_number], feed_dict={x:batch[0], y_:batch[1]})
    if (pat >= numbOfImages):
        pat = 0
        epoch = (epoch + 1)
        if (epoch >= nbEpochs):
            stop = True
        train_accuracy = accuracy.eval(feed_dict={x:batch[0], y_:batch[1]}, session=sess)
        results['training-accuracy'] = (results['training-accuracy'] + [[epoch, str(train_accuracy)]])
        print (('step %d, training accuracy %g' % (epoch, train_accuracy)))
        if (0 == (epoch % 1)):
            expstatus['status'] = 'running'
            expstatus['progress'] = (epoch / nbEpochs)
            with open((expname + '.json'), 'w') as f:
                f.write(json.dumps(expstatus))
                f.close()
    allPred = np.append(allPred, pred)
    inputLabels = np.append(inputLabels, tf.argmax(batch[1], axis=1).eval(session=sess))

class_number = graph.get_tensor_by_name('class_number:0')
test_bads = tf.get_collection('test_bads')[0]
results['final-test-accuracy'] = 0
sess.run(tf.assign(git, nbEpochs))
new_saver.save(sess, currentFolder + savingName, global_step=nbEpochs, write_meta_graph=True)
testAccuracy = accuracy.eval(feed_dict={x:dataset.test.images, y_:dataset.test.labels}, session=sess)
print (('test accuracy %g' % testAccuracy))

#
# Writing Results to file.
#

results['final-test-accuracy'] = str(testAccuracy)

# Confusion Matrix

confMatrix = tf.confusion_matrix(inputLabels, allPred)
results["confusion-matrix"] = confMatrix.eval(session=sess).tolist()
print(confMatrix.eval(session=sess))


# Returns the bad classifications, using the test dataset.
testBads = test_bads.eval(feed_dict={x:dataset.test.images, y_:dataset.test.labels}, session=sess)

# Misclassified labels
classNumbers = class_number.eval(feed_dict={x:dataset.test.images, y_:dataset.test.labels}, session=sess)
results['test-class-numbers'] = classNumbers.tolist()
results['test-bads'] = testBads.tolist()

results['duration'] = math.ceil((time.time() - start_time))
results['hostname'] = socket.gethostname()
res = resource.getrusage(resource.RUSAGE_SELF)
results['maxmem'] = res.ru_maxrss
with open(savingName + '.json', 'w') as f:
    f.write(json.dumps(results))
    f.close()

expstatus['status'] = 'finished'
with open((expname + '.json'), 'w') as f:
    f.write(json.dumps(expstatus))
    f.close()