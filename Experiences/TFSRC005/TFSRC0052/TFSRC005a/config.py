
def convertIntList(l):
    """
    Args:
        - l: is a list composed of comma separated values, from the .ini file.
    """
    return [int(i) for i in list(l.split(","))]

