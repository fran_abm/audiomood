# TFSRC005

This experience is a 12-in-1 experince. All of them will have the same dataset, but different batch sizes and epochs.
The objective is to check the ratio between epochs and batch size, and assess a middle ground for the given dataset.

## TFSRC005b
Batch: 512
Epoch: 10

## TFSRC005c
Batch: 1024
Epoch: 10

## TFSRC005d
Batch: 2048
Epoch: 10

## TFSRC005e
Batch: 4096
Epoch: 10

## TFSRC005f
Batch: 2048
Epoch: 50

## TFSRC005g
Batch: 4096
Epoch: 50

## TFSRC005h
Batch: 80192
Epoch: 50

## TFSRC005i
Batch: 2048
Epoch: 100

## TFSRC005j
Batch: 4096
Epoch: 100

## TFSRC005k
Batch: 80192
Epoch: 100
