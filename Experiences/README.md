# Experiences

In this folder there will be a collections of expericences of CNN's using either the local computer, or the datacenter provided by professor Thibault. Each experience contains all the files needed to execute the program, as well a folder containing the results of the experience. All the experience should be error free.

## ToC
- Experience 1 (MNIST001): Sample program. Creates a CNN to classify the MNIST dataset.
- Experience 2 (MNIST002): Simple variation of the first one. Difference in number of iterations.
- Experience 3 (MNIST003): Creates, trains, and saves the model, using the first file, on the datacenter. The second file will run on the local computer, to test the model.
- Experience 4 (MNIST004): Using the same method and same scripts from the previous experience,
tested a new dataset, derived from MNIST containg only number 0 through 4, being used for training.
- Experience 5 (MNIST005): Reformulation of the results report, in order to test new report module.

## TFSRC
- Experience 1 (TFSRC001): Creation of the first iteration of the model. Testing if the parameters of the architecture are correct. Trying to run locally, with a smaller dataset, before testing in the datacenter.
- Experience 2 (TFSRC002): Created a new datasets, using an improved tool. Testing dataset import tool, to automatize the 
sizes of the different datasets.
- Experience 3 (TFSRC003): Testing MFCC instead of Melspectogram.
- Experience 4 (TFSRC004): Changes to the source code, adding a config file, and self generating model layers, based on the config file. Also, testing with the full dataset.
- Experience 5 (TFSRC005): Testing several architectures with the full dataset. 12 experiences.


## YT
-