#
# Image Library.
#

#
# Resposible for converting images into other file types. 
#

# Used to search for the internal libraries.
import sys, os
sys.path.insert(0, os.path.abspath('..'))


# Internal Libraries.
from display import Display
from Audiomood import Ubyte, Header

# Other Libraries.
import numpy as np
import random
import uuid


class Image:


    # Basic Initializer.
    # Check Audio initializer, for more information.
    def __init__(self, f=None, filepath=None, filetype=None, debug=False):
        self.debug = debug

        # Default filepath. Used in setFilepath.
        self.defaultFilepath = "../images/" 

        if self.debug:
            Display.debugObject("Image",  str(filepath))
        self.file = f
        self.filepath = filepath
        self.filetype = filetype

    
    def setFile(self, f):
        """
        Sets the opened or created image file.

        Args:
            - f: file that will be set.
        """
        if self.debug:
            Display.debugMessage("Image", "setFile", "Setting a new file...")

        if self.file == None:
            if self.debug:
                Display.debugMessage("Image", "setFile", "File is empty... Setting successfully.")
            self.file = f
        else:
            Display.errorMessage("Image", "setFile", "File is already set. Not setting new one.")


    def getFile(self):
        """
        Returns the opened or created file.

        Returns:
            - The opened or created image file.
        """
        if self.debug:
            Display.debugMessage("Image", "getFile", "Returning a image file...")

        if self.file != None:
            if self.debug:
                Display.debugMessage("Image", "getFile", "File is not empty. Returning image file.")
            return self.file
        else:
            Display.errorMessage("Image", "getFile", "No file set. Returning None.")
            return None


    def setFilepath(self, filepath=None, filename=None):
        """
        Sets the filepath of the opened or created file.
        If set to none it will be a preset filepath.

        Args:
            - filepath: filepath that will be set.
            - filename: name of the file to be used.

        Glossary:
            - filepath: the full filepath of the file. This is the 

        TODO:
            - check if the given filename is not already in the file path.
            - check if the given filename doesn't have the termination.
        """
        if self.debug:
            Display.debugMessage("Image", "setFilepath", "Setting a new filepath...")

        if self.filepath == None:
            if self.debug:
                Display.debugMessage("Image", "setFilepath", "No filepath yet set. Setting a new one.")
            
            # First, we will check if a filepath was given.
            if filepath == None:
                if self.debug:
                    Display.debugMessage("Image", "setFilepath", "No filepath given. Using default one: " + self.defaultFilepath)
                
                # If the filepath was not given need to build a new filepath.
                # So we need to check if a filename as given.
                if filename == None:

                    # If the filename wasn't given, we need to create one.
                    # For this we use the uuid library that will create a unique id.
                    ids = str(uuid.uuid4()) + ".png"
                    if self.debug:
                        Display.debugMessage("Image", "setFilepath", "No filename given. Creating a new one: " + ids)
                    self.filepath = self.filepath + ids
                
                else:
                    # If the filename was set, we just concatenate the filepath.
                    filepat = self.defaultFilepath + filename + '.png'
                    if self.debug:
                        Display.debugMessage("Image", "setFilepath", "Filename was given. New filepath: " + filepat)
                    self.filepath = filepat
            
            else:
                
                if filename == None:
                    # Because a filename was not given, we create one
                    ids = str(uuid.uuid4()) + ".png"
                    if self.debug:
                        Display.debugMessage("Image", "setFilepath", "No filename given. Creating a new one: " + ids)
                        Display.debugMessage("Image", "setFilepath", "New filepath:  " + filepath + ids)

                    self.filepath = filepath + ids
                
                else:

                    # If the filename was set, we just concatenate the filepath.
                    filepat = self.defaultFilepath + filename + '.png'
                    if self.debug:
                        Display.debugMessage("Image", "setFilepath", "Filename was given. Creating a new one: " + filepat)
                    self.filepath = filepat
                

    def getFilepath(self):
        """
        Returns the opened or created file.

        Returns:
            - The opened or created image file.
        """
        if self.debug:
            Display.debugMessage("Image", "getFile", "Returning a image file...")

        if self.file != None:
            if self.debug:
                Display.debugMessage("Image", "getFile", "File is not empty. Returning image file.")
            return self.file
        else:
            Display.errorMessage("Image", "getFile", "No file set. Returning None.")
            return None


    def setFiletype(self, f):
        """
        Sets the opened or created images filetype.

        Args:
            - f: filetype that will be set.
        """
        if self.debug:
            Display.debugMessage("Image", "setFile", "Setting a new file...")

        if self.filetype == None:
            if self.debug:
                Display.debugMessage("Image", "setFiletype", "Filetype is empty... Setting successfully.")
            self.filetype = f
        else:
            Display.errorMessage("Image", "setFiletype", "Filetype is already set. Not setting new one.")


    def getFiletype(self):
        """
        Returns the opened or created file.

        Returns:
            - The opened or created image file.
        """
        if self.debug:
            Display.debugMessage("Image", "getFiletype", "Returning images filetype...")

        if self.file != None:
            if self.filetype != None:
                if self.debug:
                    Display.debugMessage("Image", "getFiletype", "Filetype is not empty. Returning images filetype.")
                return self.filetype
            else:
                if self.debug:
                    Display.debugMessage("Image", "getFiletype", "Filetype is empty. Returning nothing.")
                return None
        else:
            if self.file == None:
                Display.errorMessage("Image", "getFiletype", "No file set. Returning None.")
                return None


    def imageToDataset(self):
        """
        Converts a image into a dataset.

        Returns:
            - The image as an dataset.
        """
        if self.debug:
            Display.debugMessage("Image", "imageToDataset", "Starting conversion of image to dataset.")

        # First we need to check if the image is loaded.
        if self.file == None:
            Display.errorMessage("Image", "imagetoDataset", "No Image loaded.")
            return None
        else:
            return np.array(self.file)


    def imageToUbyte(self):
        """
        Converts an image into an Ubyte object.

        Returns:
            - An Ubyte object with an image converted.
        """
        if self.debug:
            Display.debugMessage("Image", "imageToUbyte", "Starting conversion of image to Ubyte.")

        if self.file == None:
            Display.errorMessage("Image", "imagetoUbyte ", "No Image loaded.")
            return None
        else:
            # if the image is loaded
            # we need to check if it is a PNG file. I think it is needed for the function
            #   to convert to a dataset.
            header = Header()
            ubyte = Ubyte()
            image = self.file.convert("L") # Converting to greyscale
                                        # Otherwise it will have a 4 index list with the RBGA. 
            
            # We need the size to get the image lines and columns.
            # The same would be achieved by transforming the image into an array first.
            size = self.file.size()  
            header.setNumOfRows(size[1])
            header.setNumOfCols(size[0])
            header.setNumOfItems(1) # This only contains a single image.
            arr = np.array(self.file)
            ubyte.setHeader(header)
            ubyte.setData(arr)
            return ubyte


    def saveImage(self):
        """
        Saves the image as a png file.
        Uses the atributes of the Image object to save the image.

        FUTURE:
            - Could be useful to have a return, to check if the save was sucessful.
        """
        if self.debug:
            Display.debugMessage("Image", "saveImage", "Saving Image")

        # We need to check if the filename/filepath already has the '.png'.
        # If it doesn't, we need to add it.
        if self.filepath.endswith(".png"):
            self.file.save(self.filepath)
        else:
            # This will double check if the file path already has a '.png'
            # If it doesn't we just add the '.png' and save it.
            # So if the filepath is just the name, it will save it in this same folder.
            if len(self.filepath.split(".")) == 1:
                self.file.save(self.filepath + ".png")
                self.filepath = self.filepath + ".png"
            else:
                # If it has a termination, and it is not a '.png', we need to add the '.png'
                # So we split the filepath by '.' and pick the first item in the list,
                #   and add the termination to it. The first item is the filepath and the second the
                #   original termination
                self.file.save(self.filepath.split(".")[0] + ".png")

                # To keep consistent, we change the objects filepath to the new filepath
                self.filepath = self.filepath.split(".")[0] + ".png"


if __name__=="__main__":
    i = Image(debug=True)

    
    