#
# Functions to convert from a image to an ubyte, wav, or other
#
#
#

# TODO:
#   - png to ubyte converter
#   - png to wav converter


def pngToUbyte(filename):
    """
    Converts a png image to an ubyte.

    Args:
        - filename: the filename or filepath of the image to be converted.

    Returns:
        - ubyte: the image converted into ubyte.
    """
    return ubyte