#
# Functions to convert from ubyte into an png, wav, or other
#
#
#


import struct
from header import Header
from ubyte import Ubyte
import numpy as np
from PIL import Image


def imageToUbyte(image):
    """
    This function transforms a image into a ubyte object.
    This is used for a single image. 
    This might be used in the future.

    ! Be sure that the image is converted into greyscale. 
        Use image.convert("L") !

    Args:
        - image: the image as an image, meaning it is opened as an image.

    Returns:
        - an ubyte object containing the image.

    TODO:
        - Check if it works.
        - Add exceptions.
    """
    header = Header()
    ubyte = Ubyte()
    image = image.convert("L") # Converting to greyscale
                                # Otherwise it will have a 4 index list with the RBGA. 
    
    # We need the size to get the image lines and columns.
    # The same would be achieved by transforming the image into an array first.
    size = image.size()  
    header.setNumOfRows(size[1])
    header.setNumOfCols(size[0])
    header.setNumOfItems(1) # This only contains a single image.
    arr = np.array(image)
    ubyte.setHeader(header)
    ubyte.setData(arr)
    return ubyte


def datasetToUbyte(dataset, numOfItems, numRows=0, numCols=0, t="data"):
    """
    Turns a created dataset into a ubyte file.
    This is for a data set with an image divided into several windows.

    Args:
        - type: The type of the dataset, either is data, or label.
        - dataset: the dataset to be converted.

    Returns:
        - the ubyte file as an ubyte object.
    """
    header = Header()
    header.setNumOfItems(numOfItems)

    # We need to check if the type of the data is "data". 
    # The items in the header are different.
    if t == "data":
        header.setNumOfRows(numRows)
        header.setNumOfCols(numCols)
    ubyte = Ubyte()
    ubyte.setHeader(header)
    ubyte.setData(dataset)
    return ubyte


def ubyteToImage(data, lines, cols):
    """
    Converts an ubyte file to an image.

    Args:
        - data: the data of the window being converted to png.
        - lines: the number of lines in the image.
        - cols: the number of cols in the image.

    Returns:
        - img: the ubyte file converted into imange.
    """

    # First we reshape the image, and set it uint8.
    # This is needed if the data is not in a numpy array, which it is probably not.
    # TODO: check if there might be any errors in this line.
    #           The dataset might already be in the shape needed.
    #           Or might be impossible to convert.
    two_d = (np.reshape(data, (cols, lines))).astype(np.uint8)
    
    # Then we convert it to an image. The argument "L" is the type of color scheme 
    #   of the image. In this case it is greyscale.
    # FUTURE: This might be changed in the future, if we need to add color.
    img = Image.fromarray(two_d, 'L')
    return img
    

def saveImage(image, filename):
    """
    Saves the image as a png file.

    Args:
        - image: the image as a image.
        - filename: the name of the file on to which the image will be stored.

    FUTURE:
        - Could be useful to have a return, to check if the save was sucessful.
    """
    image.save(filename+'.png')


def ubyteToWav(filename):
    """
    Converts a png image to an ubyte.

    Args:
        - filename: the filename or filepath of the ubyte file to be converted.

    Returns:
        - wav: the ubyte converted into wav.
    """
    return wav

