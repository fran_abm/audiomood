#
# Functions to create and use the datasets
# It is used to create a dataset given a MNIST dataset, or an image.
#
# t10k-images-idx3-ubyte
# t10k-labels-idx1-ubyte

import numpy as np
import random

# TODO:
# - Function to create a dataset in the TensorFlow format, given a ubyte file
#   - Should receive the data and the labels and return both
# - Function, that given an image creates a dataset
#    - Should receive an image, a column size, a line size, and a stride
# - Function that retrives a specific "image" from the dataset, or image
# 

def imageToMnist(name):
    """
    Converts a given image to an array.
    Currently only is considering a black and white image.

    Args:
        - name: filename of the image to be converted
    
    Returns:
        - The image as an numpy array.
    """
    try:
        return np.asarray(Image.open('my.png'))
    except:
        return False

def scale(m, low, high):
    """

    TODO: Melhorar. Alguns valores que devolve sao suspeitos
    Talvez adicionar a ideia dos buckets
    """
    min = m.min()
    max = m.max()
    if min == max:
        alpha = 0
    else:
        alpha = (low - high) / (min - max)
    beta = low - alpha * min
    scaled = m * alpha + beta
    return scaled

def test():
    m1 = np.random.rand()*100*np.random.rand(5,3)
    print(m1)
    m2 = scale(m1,0, 255)
    print(m2)
    print(np.round(m2))

def imageToDatset(image, col, line, stride):
    """
    Function to turn an image to a dataset. 

    Args:
        - image: filename of the image to be converted to a dataset
        - col: number of columns of the window 
        - line: number of lines of the window
        - stride: difference in columns and lines between each window
    
    Returns:
        - The given image as a dataset. 

    TODO:
        - Turn image into bytes (values between 0 - 255)
        - Create window
        - Create rolling window function
        - Create Array
        - Return Array
    """
    # Returns the image as an array.
    M = imageToMnist(image)
    M = scaled(np.log(M))
    return M

def cleanUbyte(headlessFile):
    """
    Function to read the last byte in a line of an Ubyte file (technicaly the value)
    
    !Probably never used!
    
    Args:
        - headlessFile: the ubyte file with the header removed.

    Returns:
        - the byte that contains the wanted value.
    """
    return headlessFile.read(4)[-1]

def bytes2int(s):
    """
    Converts bytes into integers.

    Args:
        - s: the string that contains the byte that was read from the file.

    Returns:
        - the integer converted byte 
    """
    return int.from_bytes(s, "big")

def ubyteReader(f, t="data"):
    """
    Ubyte file reader. 

    Args:
        - f: the filename/path of the file that will be read
        - t: the type of data. This might be "data" for the data dataset, 
            and "label" for the label dataset
    
    Returns:
        - two elements, the first the header of the file, and the second one
            the dataset itself.
    """
    header = ""
    dataF = []
    with open(f, "rb") as data:
        n = True
        if t == "data":
            header = data.read(16)
        else:
            header = data.read(8)
        while n:
            try:
                dataF.append(bytes2int(data.read(1)))
            except:
                print("over")
                n = False
    return header, dataF

def ubyteToFullDataset(dataFile, labelFile):
    """
    Receives two files, a file that has the data, and a file that has the labels, reads,
        and turns them to the dataset.
    
    Args:
        - dataFile: Ubyte file that has the data (images)
        - labelFile: Ubyte file that has the labels
    
    Returns:
        - Two tuples with two elements each. The first contains the data, and 
            the second one the labels. The first element in each tuple has the 
            headers of the files, and the second the rest of the data.
    """
    dataHeader = []
    dataF = []
    dataHeader, dataF = ubyteReader(dataFile, "data")
    labelHeader, labelF = ubyteReader(labelFile, "label")
    print "Length of data array: ", len(dataF)
    print "Number of labels: ", len(labelF)
    return (dataHeader, dataF), (labelHeader, labelF)


def imageDataset(datafile, col, line, numOfElements):
    """
    Receives the datafile as a dataset (meaning, as a list of numbers), a returns 
        a list of lists with the image spliced into windows, with a given stride
    
    !To be used for MNIST!

    Args:
        - datafile: list of integers, that represent the image as a 256 bit grey image
        - col: number of columns in the window
        - line: number of lines in the window
        - stride: difference in columns between windows.
    
    Returns:
        - The dataset, converted into a list of lists. Each list has an image,
            that is not treated (i.e - divided by columns).
    TODO:
        - verify that the elem does not pass  the dataFileSize
    """
    imgDataset = []
    dataFileSize = len(datafile)
    for i in range(numOfElements):
        o = []
        elem = 0
        for j in range(line):
            p = []
            for k in range(col):
                # if datafile[i] != 0:
                #     print(datafile[i])
                p.append(datafile[elem])
                elem += 1
            o.append(p)
        imgDataset.append(o)
    return imgDataset

def singularWindow(image, numCol, numLine, winCol, winLine, startCol, startLine):
    """
    Retrives a window from the image.

    Args:
        - image: the image as a list of numbers.
        - numCol: number of columns in the image.
        - numLine: number of lines in the image.
        - winCol: the number of columns in the window.
        - winLine: number of lines in the window.
        - startCol: the top left corner of the window, in the columns
        - startLine: the top left corner of the window, in the lines
    
    Returns:
        a list of lists, containing the window
    """
    l = []
    if startLine + winLine <= numLine and startCol + winCol <= numCol:
        for i in range(winLine):
            l.append(image[startLine+winLine][startCol:startCol+winCol])
    return l

def numOfWindows(numCol, numLine, col, line, stride):
    """
    Calculates the number of windows of an imagem given the number of columns and lines that
        the image has, the number of columns and lines of the desired window, and the stride.
    
    Args:
        - numCol: number of columns in the image.
        - numLine: number of lines in the image.
        - col: the number of columns in the window.
        - line: number of lines in the window.
        - stride: difference in columns between images.
    
    Returns:
        - the number of windows that can be cut from the image.

    TODO:
        - verify if the numCol and numLine is larger than col and line respectably
        - minimize the code 
    """
    c = 0
    l = 0
    ret = 0
    if line != numLine:
        l = numLine - line
    if col != numCol:
        c = numCol - col
    if c == 0:
        ret = l/stride
    elif l == 0:
        ret = c/stride
    elif c == 0 and l == 0:
        ret = 1
    else:
        ret = (l*c)/stride
    return ret



def window(image, numCol, numLine, col, line, stride):
    """
    Recieves an image, and retrieves windows from that image, given a 
        window column size and line size, and a given stride.
    
    !To used with an image.!
    
    Args:
        - image: the image as a list of numbers.
        - numCol: number of columns in the image.
        - numLine: number of lines in the image.
        - col: the number of columns in the window.
        - line: number of lines in the window.
        - stride: difference in columns between images.
    
    Returns:
        l: list of lists containing the image spliced into windows.
    
    TODO:
        - Check if the passes are accurate
        - Clean up
    """
    l = []
    leftUp = [0,0]
    numOfWindow = numOfWindows(numCol, numLine, col, line, stride)
    for i in range(numOfWindow):
        print("Canto Sup Esquerdo: ", leftUp)
        l.append(singularWindow(image, numCol, numLine, col, line, leftUp[0], leftUp[1]))
        if leftUp[0] < numCol and leftUp[0] + stride + col <= numCol :
            leftUp[0] = leftUp[0] + stride
        elif leftUp[0] + stride + col > numCol and leftUp[1] + stride + line <= numCol:
            leftUp[0] = 0
            leftUp[1] = leftUp[1] + stride
    return l



def randomImage(line, col):
    """
    Creates a random image, given the number of lines and the number of columns.
        This is strictly for testing. 
        
    Args:
        - line: the number of lines that the generated image has.
        - col: the number of columns that the generated image has.
    
    Returns:
        - a list of lists containing the generated image.
    """
    l = []
    for i in range(line):
        j = []
        for k in range(col):
            j.append(random.randint(0,255))
        l.append(j)
    return l



# a,b = ubyteToFullDataset("t10k-images-idx3-ubyte", "t10k-labels-idx1-ubyte")
# test = imageDataset(a[1], 28, 28, 10000)

rand = randomImage(56,56)
#win = singularWindow(rand, 56, 56, 28, 28, 0, 0)
win = window(rand, 56, 56, 28, 28, 1)
print "Number of windows: ", len(win)
print "Number of lines in first window: ", len(win[0])

# print "Number of columns in first window: ", len(win[0][0])

